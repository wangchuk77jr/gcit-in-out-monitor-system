@props(['url'])
<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://gitlab.com/wangchuk77jr/gcit-in-out-monitor-system/-/raw/main/In-Out-Monitoring-System/public/images/logo.png?ref_type=heads" class="logo" alt="gcit in-out-logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
