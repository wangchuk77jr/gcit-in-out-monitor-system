@extends('layouts.generalbase')

@section('title')
    HR HEAD | @yield('title-addition') 
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/hrhead-css/HRHeadlayout.css') }}">
    @yield('hrheadcss')
@endsection

@section('content')
    <div class="container-fluid m-0">
        <div class="row">
            {{-- Side Bar section --}}
            <div class="col-md-3 vh-100 side-bar position-sticky top-0">
                <ul class="nav flex-column gap-2">
                    <li class="nav-item" id="logo">
                        <span class="nav-link py-3">
                            <img class="mb-2" style="filter: saturate(0) brightness(100);" width="200"  src="{{ asset('images/sidebarlogo.png') }}" alt="">
                            <svg width="192" height="1" viewBox="0 0 192 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <line x1="0.994751" y1="0.5" x2="191.005" y2="0.5" stroke="#D9D9D9" stroke-opacity="0.37"/>
                            </svg>
                        </span>     
                    </li>
                    <li class="nav-item mt-3">
                        <a class=" dashboard-link nav-link nav-link-item {{ Request::is('hrhead/dashboard') ? 'active-nav' : '' }} d-flex align-items-center gap-2" href="{{ route('HRHeaddashboard') }}">
                            <img width="20" src="{{ asset('images/dashboard.png') }}" alt="">
                            <span class="ps-2">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="dashboard-link position-relative nav-link nav-link-item  {{ Request::is('hrhead/visitors-record') ? 'active-nav' : '' }} d-flex align-items-center gap-2" href="{{ route('hrhead-visitor-record') }}">
                            <img width="20" src="{{ asset('images/users.png') }}" alt="">
                            <span class="ps-2">Visitor Approval</span>
                            @if($totalVisitorRequestCount > 0)
                            <span class="notification">{{$totalVisitorRequestCount}}</span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="dashboard-link nav-link nav-link-item {{ Request::is('hrhead/profile-setting') ? 'active-nav' : '' }} d-flex align-items-center gap-2" href="{{ route('hrhead-setting') }}">
                            <img width="20" src="{{ asset('images/settings.png') }}" alt="">
                            <span class="ps-2">Profile Setting</span>
                        </a>
                    </li>
                </ul>
            </div>
            
            {{-- Header section & Dashboard Content --}}
            <div class="col-md-9" id="page-content">
                <div class="row">
                    {{-- Header section --}}
                    <div class="col-md-12 header d-flex gap-3 align-items-center position-sticky top-0">
                        <h3 class="me-auto dashboard-heading d-flex align-items-center">
                            {{-- Samll screen menu bar --}}
                             <div class="d-none menu-bar me-3">
                                <button type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="side-menu-button">
                                    <i style="font-size:30px;" class="fa-sharp fa-solid fa-bars"></i>
                                </button>
                            </div>
                            {{-- Dashboard heading --}}
                            @yield('dashboard-heading')
                        </h3>
                        <div class="profile d-flex gap-3">
                        {{-- Header inheritance --}}
                         @yield('header-content')
                           <div>
                             <p class="username pt-1 text-uppercase">{{ Auth::user()->name }}({{ Auth::user()->role }})</p>                          
                            </div>
                           <div>
                            @if (Auth::user()->profile_image)
                                <img class="profile-img" src="{{ asset('profile_images/' . Auth::user()->profile_image) }}" alt="User Profile Image">
                            @else
                                <img class="profile-img" src="{{ asset('images/defualtProfileImage.png') }}" alt="Default Profile Image">
                            @endif                     
                                <button type="button" class="logout-button-drop-down" data-bs-toggle="dropdown" aria-expanded="false">
                                   <img src="{{ asset('images/dropdown.png')}}" alt="dropdwonicon">
                               </button>
                               <div class="dropdown-menu dropdown-menu-end logout-dropdown-body mt-3 text-center">
                                    <!-- logout -->
                                    <form method="POST" action="{{ route('logout') }}" class="w-100 mt-3" >
                                        @csrf
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();" class="logout-button">Logout</a>
                                    </form>
                               </div>
                           </div>
                       </div>  
                     </div>
                     {{-- dashboard contente --}}
                     <div class="dashboard-content-section">
                        @yield('dashboard-content')
                    </div>
                </div>
            </div>
        </div>    
    </div>
                <!-- side bar items in small screen -->
            <div style="background:#F56E2C" class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-fullscreen">
                <div class="modal-content" style="background:#F56E2C">
                    <div class="modal-header border-bottom-0">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">
                        <img class="mb-2" style="filter: saturate(0) brightness(100);" width="200"  src="{{ asset('images/sidebarlogo.png') }}" alt="">
                    </h1>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <hr class="mx-3" style="color: #eee">
                    <div class="modal-body">
                        <div>
                            <ul class="nav flex-column gap-2">
                                <li class="nav-item mt-3">
                                    <a class=" dashboard-link nav-link nav-link-item {{ Request::is('hrhead/dashboard') ? 'active-nav' : '' }} d-flex align-items-center gap-2" href="{{ route('HRHeaddashboard') }}">
                                        <img width="20" src="{{ asset('images/dashboard.png') }}" alt="">
                                        <span class="ps-2">Dashboard</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="dashboard-link position-relative  nav-link nav-link-item  {{ Request::is('hrhead/visitors-record') ? 'active-nav' : '' }} d-flex align-items-center gap-2" href="{{ route('hrhead-visitor-record') }}">
                                        <img width="20" src="{{ asset('images/users.png') }}" alt="">
                                        <span class="ps-2">Visitor Approval</span>
                                    
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="dashboard-link nav-link nav-link-item {{ Request::is('hrhead/profile-setting') ? 'active-nav' : '' }} d-flex align-items-center gap-2" href="{{ route('hrhead-setting') }}">
                                        <img width="20" src="{{ asset('images/settings.png') }}" alt="">
                                        <span class="ps-2">Profile Setting</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    <script>
        // JavaScript code to add the 'active-nav' class to the current page's link
        const currentPath = window.location.pathname;
        const links = document.querySelectorAll('.nav-link');

        links.forEach(link => {
            const href = link.getAttribute('href');
            if (currentPath === href) {
                link.classList.add('active-nav');
            }
        });
        document.querySelectorAll('.dashboard-link').forEach(function(link) {
                link.addEventListener('click', function(event) {
                    event.preventDefault();
                    var targetRoute = this.getAttribute('href');
                    document.getElementById('page-content').classList.remove('fade-in');
                    setTimeout(function() {
                        window.location.href = targetRoute;
                    }, 400); // Adjust the delay to match your transition duration
                });
            });
    </script>
@endsection


