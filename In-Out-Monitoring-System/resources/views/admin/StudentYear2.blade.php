        {{-- 2 year students record --}}
        <div class="col-md-12 section-container mt-5 ">
            <div class="student-sorting px-4 d-flex align-items-center">
                <div class="student-heading-number me-auto">
                    <h6 class="student-number pt-2">YEAR 2 STUDENTS: <span class="s-total-number">{{ $yearCounts['year2'] }}</span></h6>
                </div>
                <div class="student-search d-flex gap-5">
                    <form id="searchYear2Form" class="searchContainer bg-danger p-0 m-0">
                        <input name="searchYear2" class="student-search-input px-3" type="search" placeholder="Search student by name or enrollment number">
                        <button  type="button" id="searchIcon" onclick="searchYear2()" >
                            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/></svg>
                        </button>
                    </form>
                  <div class="btn-group">
                    <button class="action-button" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                            <rect width="8" height="6" rx="2" fill="#FF7131"/>
                            <rect x="12" width="8" height="6" rx="2" fill="#FF7131"/>
                            <rect y="7" width="8" height="6" rx="2" fill="#FF7131"/>
                            <rect x="12" y="7" width="8" height="6" rx="2" fill="#FF7131"/>
                            <rect y="14" width="8" height="6" rx="2" fill="#FF7131"/>
                            <rect x="12" y="14" width="8" height="6" rx="2" fill="#FF7131"/>
                          </svg>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end px-4 py-4" style="box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px;">
                        <li>
                          <button class="dropdown-item" type="button" data-bs-toggle="modal" data-bs-target=".studentImageUploadAtOnce">
                              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="8" viewBox="0 0 15 8" fill="none">
                                  <path d="M14.3536 4.35355C14.5488 4.15829 14.5488 3.84171 14.3536 3.64645L11.1716 0.464466C10.9763 0.269204 10.6597 0.269204 10.4645 0.464466C10.2692 0.659728 10.2692 0.976311 10.4645 1.17157L13.2929 4L10.4645 6.82843C10.2692 7.02369 10.2692 7.34027 10.4645 7.53553C10.6597 7.7308 10.9763 7.7308 11.1716 7.53553L14.3536 4.35355ZM0 4.5H14V3.5H0V4.5Z" fill="black" fill-opacity="0.8"/>
                              </svg>
                              Batch Student Profile Image Upload/Update
                          </button>
                      </li>
                      <li class="mt-2">
                          <button class="dropdown-item" type="button" data-bs-toggle="modal" data-bs-target="#updateStudentYear2">
                              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="8" viewBox="0 0 15 8" fill="none">
                                  <path d="M14.3536 4.35355C14.5488 4.15829 14.5488 3.84171 14.3536 3.64645L11.1716 0.464466C10.9763 0.269204 10.6597 0.269204 10.4645 0.464466C10.2692 0.659728 10.2692 0.976311 10.4645 1.17157L13.2929 4L10.4645 6.82843C10.2692 7.02369 10.2692 7.34027 10.4645 7.53553C10.6597 7.7308 10.9763 7.7308 11.1716 7.53553L14.3536 4.35355ZM0 4.5H14V3.5H0V4.5Z" fill="black" fill-opacity="0.8"/>
                              </svg>
                               Update Student Year
                          </button>
                      </li>
                      <li class="mt-2">
                          <button class="dropdown-item" type="button" data-bs-toggle="modal" data-bs-target="#deleteAllStudentyear2">
                              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="8" viewBox="0 0 15 8" fill="none">
                                  <path d="M14.3536 4.35355C14.5488 4.15829 14.5488 3.84171 14.3536 3.64645L11.1716 0.464466C10.9763 0.269204 10.6597 0.269204 10.4645 0.464466C10.2692 0.659728 10.2692 0.976311 10.4645 1.17157L13.2929 4L10.4645 6.82843C10.2692 7.02369 10.2692 7.34027 10.4645 7.53553C10.6597 7.7308 10.9763 7.7308 11.1716 7.53553L14.3536 4.35355ZM0 4.5H14V3.5H0V4.5Z" fill="black" fill-opacity="0.8"/>
                              </svg>
                              Delete all student at once
                          </button>
                      </li>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
        {{-- 2 year student table --}}
        <div class="col-md-12 section-container mt-3">
            <table id="year2StudentTableBodyOuter" class="table table-responsive table-bordered custom-table">
                <thead>
                    <tr class="table-title py-3">
                        <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                        <th scope="col">SID</th>
                        <th scope="col">NAME</th>
                        <th scope="col">PHONE NUMBER</th>
                        <th scope="col">COURSE</th>
                        <th scope="col">BLOCK</th>
                        <th scope="col">GENDER</th>
                        <th scope="col">PROFILE IMG</th>
                        <th scope="col" class="rounded-top-right text-center">ACTION</th>
                    </tr>
                </thead>
                <tbody id="year2StudentTableBody">
                    @foreach($year2Students as $student2)
                    <tr>
                        <td class="ps-3">{{ $loop->index + 1 }}</td>
                        <td>{{ $student2->studentID }}</td>
                        <td>{{ $student2->studentName }}</td>
                        <td>{{ $student2->phoneNumber }}</td>
                        <td>{{ $student2->studentCourse }}</td>
                        <td>{{ $student2->hotelBlock }}</td>
                        <td>{{ $student2->gender }}</td>
                        @if ($student2->profileImage !== null)
                        <td class="text-center">
                            <img style="border-radius: 5px;background:rgb(120, 117, 117);object-fit:fill;object-position:center" width="50" height="30" src="{{ asset('studentProfile/' . $student2->profileImage) }}" alt="{{ $student2->profileImage }}">
                         </td>
                         @else
                         <td class="text-center">
                            <span class="d-flex ms-auto me-auto justify-content-center align-items-center" style="border-radius: 5px;background:#fff;width:50px;height:30px;color:#000;border:1px solid #F56E2C;">{{ strtoupper(substr(current(explode(' ', $student2->studentName)), 0, 1)) }} {{ strtoupper(substr(last(explode(' ', $student2->studentName)), 0, 1)) }}</span> 
                         </td>
                         @endif
                        <td class="actionbutton d-flex gap-3 text-center justify-content-center align-items-center">
                            <button class="editButton" data-bs-toggle="modal" data-bs-target=".editStudent{{$student2->studentID}}">
                                <i class="fa-solid fa-pen" style="color: #454444; font-size: 16px;"></i>
                            </button>
                            <button class="deleteButton" data-bs-toggle="modal" data-bs-target="#deleteStudent{{  $student2->studentID}}">
                                <i class="fa-solid fa-trash-can" style="color: #454444; font-size: 16px;"></i>
                            </button>
                        </td>
                    </tr>
        
                    <!-- Delete Modal -->
                    <div class="modal fade" id="deleteStudent{{ $student2->studentID }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteStudentLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content px-4 py-3 deleteModal">
                                <div class="modal-header">
                                    <h1 class="modal-title deleteModalTitle fs-5" id="deleteStudentLabel{{ $student2->id }}">Delete Student</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p class="confirm-text">Are you sure you want to delete this student, <span style="color: red">{{ $student2->studentName}} ({{ $student2->studentID }})</span>?</p>
                                </div>
                                <div class="modal-footer d-flex flex-row justify-content-start my-2">
                                    <form action="{{ route('deleteStudent') }}" method="POST">
                                        @csrf
                                        <input hidden type="text" name="studentID" value="{{$student2->studentID}}">
                                        <button type="submit" class="btnDeleteYes">Yes</button>
                                    </form>
                                    <button type="button" class="btnCancel" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Student Edit Modal -->
                    <div class="modal fade editStudent{{$student2->studentID}}" data-bs-backdrop="static" tabindex="-1" aria-labelledby="editStudentLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content px-5 py-3 addStudentModal">
                                <div class="modal-header border-0">
                                <h1 class="modal-title fs-5 addstudent-heading mb-0" id="addNewStudentModalLabel">Edit Student ID : <span style="color: green">{{$student2->studentID}}</span></h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    {{-- Edit --}}
                                    <form action="{{ route('editStudent') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input hidden value="{{$student2->studentID}}" name="studentID" type="text" class="w-100 mt-0 px-3 addstudentInput" placeholder="Enter student number">
                                        <input value="{{$student2->studentName}}" name="studentName" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Student name">
                                        <select name="studentYear" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                            <option value="{{$student2->studentYear}}" selected>{{$student2->studentYear}}</option>
                                            <option class="selectOptions" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                        <select name="studentCourse" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                            <option value="{{$student2->studentCourse}}" selected>{{$student2->studentCourse}}</option>
                                            <option value="IT">IT</option>
                                            <option value="CS">CS</option>
                                            <option value="Block Chain">Bloack Chain</option>
                                            <option value="AI">AI</option>
                                            <option value="Fullstack">Fullstack</option>
                                        </select>
                                        <select name="hotelBlock" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                            <option value="{{$student2->hotelBlock}}" selected>{{$student2->hotelBlock}}</option>
                                            <option value="k">Block K</option>
                                            <option value="m">Block M</option>
                                            <option value="l">Block L</option>
                                            <option value="n">Block N</option>
                                        </select>
                                        <select name="gender" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                            <option value="{{$student2->gender}}" selected>{{$student2->gender}}</option>
                                            <option value="male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>                                   
                                        <input  value="{{$student2->phoneNumber}}" name="phoneNumber" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Phone number">
                                        <input style="border: 1px solid #000;" class="w-100 mt-2 pt-1 px-3 addstudentInput" name="profileImage" class="w-100 mt-0 px-3 addstudentInput" type="file">
                                
                                        <button type="submit" class="w-100 text-uppercase mt-4 mb-3 addStudentFileButton">Update</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>   
      
                    @endforeach
                </tbody>
            </table>
            
         </div>


        {{-- Update year --}}
        <div class="modal fade" id="updateStudentYear2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="updateStudentYear1Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content  px-4 py-3 deleteModal">
                <div class="modal-header">
                <h1 class="modal-title deleteModalTitle fs-5" id="updateStudentYear2Label">Update students year 2 to 3</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/UpdateStudentYear') }}" method="POST">
                        @csrf
                        <p>Are you sure you want to update all students' year 2 to 3? <br>
                            <span id="confirm-text2" style="color: rgb(2, 145, 52);user-select: none;">Confirm Code: deleteX$89year03_+=</span>
                        </p>
                        <input class="w-100 px-3 addstudentInput" type="text" id="code-input2" placeholder="Enter the above code to confirm">
                        <input hidden type="text" name="studentYear" id="studentYear" value="2">
                        <div class="mt-3 prerequisite-message">
                            <p style="color: red">*** Ensure the year 3 table is empty and properly prepared before initiating the update from year 2 to year 3.</p>
                        </div>
                        <button style="height: 43px;" class="w-100 text-uppercase mt-4 mb-3" type="submit" id="confirm-button2" disabled>Confirm</button>
                    </form>
                    <script>
                        // Add an event listener to the code input field
                        document.getElementById("code-input2").addEventListener("input", function () {
                            var codeInput = this.value;
                            var confirmationCode = "deleteX$89year03_+=";
                    
                            if (codeInput === confirmationCode) {
                                // Enable the "Confirm" button and add a class
                                document.getElementById("confirm-button2").removeAttribute("disabled");
                                document.getElementById("confirm-button2").classList.add('addStudentFileButton');
                            } else {
                                // Disable the "Confirm" button if the codes don't match
                                document.getElementById("confirm-button2").setAttribute("disabled", "disabled");
                                // Optionally, remove the class if it was previously added
                                document.getElementById("confirm-button2").classList.remove('addStudentFileButton');
                            }
                        });
                    </script>
                </div>
            </div>
            </div>
        </div>


        {{-- Delete Student a once --}}
        <div class="modal fade" id="deleteAllStudentyear2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteAllStudentyear1Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content  deleteModal  px-4 py-3">
                <div class="modal-header">
                <h1 class="modal-title deleteModalTitle fs-5" id="deleteAllStudentyear1Label">Delete all year 2 students</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/deleteStudentsByYear') }}" method="POST">
                        @csrf
                        <p>Are you sure you want to delete all students' of year 2? <br>
                            <span id="confirm-deletetext" style="color: rgb(220, 23, 23);user-select: none;">Confirm Code: deleteX$89year02_+=</span>
                        </p>
                        <input class="w-100 px-3 addstudentInput" type="text" id="code-deleteinput2" placeholder="Enter the above code to confirm">
                        <input hidden type="text" name="studentYear" id="studentYear3" value="2">
                        <button style="height: 43px;" class="w-100 text-uppercase mt-4 mb-3" type="submit" id="deleteconfirm-button2" disabled>Confirm</button>
                    </form>
                    <script>
                        // Add an event listener to the code input field
                        document.getElementById("code-deleteinput2").addEventListener("input", function () {
                            var codeInput = this.value;
                            var confirmationCode = "deleteX$89year02_+=";
                    
                            if (codeInput === confirmationCode) {
                                // Enable the "Confirm" button and add a class
                                document.getElementById("deleteconfirm-button2").removeAttribute("disabled");
                                document.getElementById("deleteconfirm-button2").classList.add('btnDeleteYes');
                            } else {
                                // Disable the "Confirm" button if the codes don't match
                                document.getElementById("deleteconfirm-button2").setAttribute("disabled", "disabled");
                                // Optionally, remove the class if it was previously added
                                document.getElementById("deleteconfirm-button2").classList.remove('btnDeleteYes');
                            }
                        });
                    </script>
                </div>
            </div>
            </div>
        </div>

        <!-- Script for Year 2 -->
        <script>
                document.addEventListener('DOMContentLoaded', function () {
                    const searchFormYear2 = document.getElementById('searchYear2Form');
                    const tableRowsYear2 = document.querySelectorAll('#year2StudentTableBody tr');

                    const handleSearchYear2 = function () {
                        const searchInputYear2 = searchFormYear2.querySelector('input[name="searchYear2"]').value.toLowerCase();

                        tableRowsYear2.forEach((row, index) => {
                            // Skip the first row (index 0) which is the heading row
                            if (index === 0) {
                                return;
                            }

                            const studentNameElement = row.querySelector('td:nth-child(3)');
                            const enrollmentNumberElement = row.querySelector('td:nth-child(2)');

                            // Check if the elements are found before accessing properties
                            const studentNameYear2 = studentNameElement ? studentNameElement.textContent.toLowerCase() : '';
                            const enrollmentNumberYear2 = enrollmentNumberElement ? enrollmentNumberElement.textContent.toLowerCase() : '';

                            if (studentNameYear2.includes(searchInputYear2) || enrollmentNumberYear2.includes(searchInputYear2)) {
                                row.style.display = ''; // show the row
                            } else {
                                row.style.display = 'none'; // hide the row
                            }
                        });
                    };

                    searchFormYear2.addEventListener('input', handleSearchYear2);

                    // Optional: If you want to reset the search when the form is cleared
                    searchFormYear2.addEventListener('reset', function () {
                        tableRowsYear2.forEach(row => {
                            row.style.display = ''; // show all rows
                        });
                    });
                });
            </script>
        <!-- Pagination Container with Bootstrap 5 Pagination -->
        <div id="paginationContainer" class="mt-3 d-flex justify-content-center justify-content-lg-end align-items-center">
            <nav aria-label="Page navigation">
                <ul id="paginationList" class="pagination">
                    <!-- Pagination links will be dynamically added here -->
                </ul>
            </nav>
        </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const tableRowsYear2 = document.querySelectorAll('#year2StudentTableBody tr');
            const rowsPerPage = 7;
            let currentPage = 1;

            // Function to update the table based on the current page
            const updateTableRows = function () {
                const startIndex = (currentPage - 1) * rowsPerPage;
                const endIndex = startIndex + rowsPerPage;

                tableRowsYear2.forEach((row, index) => {
                    if (index >= startIndex && index < endIndex) {
                        row.style.display = ''; // show the row
                    } else {
                        row.style.display = 'none'; // hide the row
                    }
                });
            };

            // Function to generate pagination links
            const generatePaginationLinks = function () {
                const totalRows = tableRowsYear2.length - 1; // Exclude the header row
                const totalPages = Math.ceil(totalRows / rowsPerPage);

                const paginationContainer = document.getElementById('paginationContainer');
                const paginationList = document.getElementById('paginationList');

                // Clear existing links
                paginationList.innerHTML = '';

                // Previous Arrow
                const previousArrow = document.createElement('li');
                previousArrow.classList.add('page-item');
                if (currentPage === 1) {
                    previousArrow.classList.add('disabled');
                }
                const previousLink = document.createElement('a');
                previousLink.classList.add('page-link');
                previousLink.href = '#';
                previousLink.innerHTML = '&laquo;';
                previousLink.addEventListener('click', function (event) {
                    event.preventDefault();
                    if (currentPage > 1) {
                        currentPage--;
                        updateTableRows();
                        generatePaginationLinks();
                    }
                });
                previousArrow.appendChild(previousLink);
                paginationList.appendChild(previousArrow);

                // Pagination links
                for (let i = 1; i <= totalPages; i++) {
                    const pageItem = document.createElement('li');
                    pageItem.classList.add('page-item');
                    if (i === currentPage) {
                        pageItem.classList.add('active');
                    }
                    const pageLink = document.createElement('a');
                    pageLink.classList.add('page-link');
                    pageLink.href = '#';
                    pageLink.textContent = i;
                    pageLink.addEventListener('click', function (event) {
                        event.preventDefault();
                        currentPage = i;
                        updateTableRows();
                        generatePaginationLinks();
                    });
                    pageItem.appendChild(pageLink);
                    paginationList.appendChild(pageItem);
                }

                // Next Arrow
                const nextArrow = document.createElement('li');
                nextArrow.classList.add('page-item');
                if (currentPage === totalPages) {
                    nextArrow.classList.add('disabled');
                }
                const nextLink = document.createElement('a');
                nextLink.classList.add('page-link');
                nextLink.href = '#';
                nextLink.innerHTML = '&raquo;';
                nextLink.addEventListener('click', function (event) {
                    event.preventDefault();
                    if (currentPage < totalPages) {
                        currentPage++;
                        updateTableRows();
                        generatePaginationLinks();
                    }
                });
                nextArrow.appendChild(nextLink);
                paginationList.appendChild(nextArrow);
            };

            // Initial setup
            updateTableRows();
            generatePaginationLinks();
        });
    </script>







        
        
        
