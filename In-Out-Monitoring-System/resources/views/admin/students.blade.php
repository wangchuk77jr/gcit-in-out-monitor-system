@extends('layouts.adminlayout')

@section('admincss')
    <link rel="stylesheet" href="{{ asset('css/admin/students.css') }}">
@endsection

@section('title-addition')
    Stundents
@endsection

@section('dashboard-heading')
    Students Record
@endsection

@section('header-content')
    <button class="button-add-student d-flex small-screen-displayNone align-items-center px-5" data-bs-toggle="modal" data-bs-target="#addNewStudentModal">
        Add new students
    </button>
@endsection
 

@section('dashboard-content')
<!-- File Upload Modal -->
<div class="modal fade" id="addNewStudentModal" data-bs-backdrop="static" tabindex="-1" aria-labelledby="addNewStudentModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content px-5 py-3 addStudentModal">
                <div class="modal-header border-0">
                <h1 class="modal-title fs-5 addstudent-heading" id="addNewStudentModalLabel">Add New Student</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p class="addnewStudent-sub-heading mb-3">Add list of students at once **</p>
                    <form  action="{{ route('importStudentExcel') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <label for="excelFile" class="custom-file-upload">
                            <input name="excel_file" required id="excelFile" class="w-100 excelFileInput px-3"  type="file" accept=".xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" onchange="updateFileName()">
                            <span id="file-name-label">Choose a file (accepted file format .xlsx)</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="16" viewBox="0 0 30 16" fill="none">
                                <rect width="30" height="16" fill="#D9D9D9"/>
                                <path d="M15.3536 4.64645C15.1583 4.45118 14.8417 4.45118 14.6464 4.64645L11.4645 7.82843C11.2692 8.02369 11.2692 8.34027 11.4645 8.53553C11.6597 8.7308 11.9763 8.7308 12.1716 8.53553L15 5.70711L17.8284 8.53553C18.0237 8.7308 18.3403 8.7308 18.5355 8.53553C18.7308 8.34027 18.7308 8.02369 18.5355 7.82843L15.3536 4.64645ZM15.5 12V5H14.5V12H15.5Z" fill="black" fill-opacity="0.6"/>
                            </svg>
                        </label>
                        <script>
                            function updateFileName() {
                                const fileInput = document.getElementById("excelFile");
                                const fileNameLabel = document.getElementById("file-name-label");
                    
                                if (fileInput.files.length > 0) {
                                    const fileName = fileInput.files[0].name;
                                    // You can customize the number of characters to display here.
                                    const maxLength = 30; // Change this to your preferred length.
                    
                                    if (fileName.length > maxLength) {
                                        fileNameLabel.textContent = fileName.substr(0, maxLength) + " ...";
                                    } else {
                                        fileNameLabel.textContent = fileName;
                                    }
                                } else {
                                    fileNameLabel.textContent = "Choose a file (accepted file format .xlsx)";
                                }
                            }
                        </script>
                        <div class="mt-3 prerequisite-message">
                                <p>The following required columns are necessary to add a student:</p>
                                <ul>
                                    <li>- student_id</li>
                                    <li>- student_name</li>
                                    <li>- year</li>
                                    <li>- course_name</li>
                                    <li>- hostel_block</li>
                                    <li>- gender</li>
                                    <li>- phone_number</li>
                                </ul>
                                <p>*** All column names must match the ones listed above.</p>
                            </div>                        
                       <button class="w-100 text-uppercase addStudentFileButton" type="submit">Upload</button>
                    </form>
                    <p class="text-center mt-4">OR</p>
             <button class="w-100 addMuallyButton mb-4" data-bs-toggle="modal" data-bs-dismiss="modal"  data-bs-target="#addManuallyModal">ADD MANUALLY</button>
        </div>
    </div>
</div>
</div>

<!-- Modal for manual entry -->
<div class="modal fade" id="addManuallyModal" data-bs-backdrop="static" tabindex="-1" aria-labelledby="addManuallyModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content px-5 py-3 addStudentModal">
                        <div class="modal-header border-0">
                        <h1 class="modal-title fs-5 addstudent-heading" id="addNewStudentModalLabel">Add New Student</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            {{-- ADD STUDENT MANUALLY --}}
                            <form action="{{ url('admin/studentRecord') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input required name="studentID" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Enter student number">
                                <input required name="studentName" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Student name">
                                <select name="studentYear" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                    <option disabled selected>Select Year</option>
                                    <option class="selectOptions" value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                <select name="studentCourse" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                    <option disabled selected>Select course</option>
                                    <option value="IT">IT</option>
                                    <option value="CS">CS</option>
                                    <option value="Block Chain">Bloack Chain</option>
                                    <option value="AI">AI</option>
                                    <option value="Fullstack">Fullstack</option>
                                </select>
                                <select name="hotelBlock" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                    <option disabled selected>Select Hostel Block</option>
                                    <option value="k">Block K</option>
                                    <option value="m">Block M</option>
                                    <option value="l">Block L</option>
                                    <option value="n">Block N</option>
                                </select>
                                <select name="gender" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                    <option disabled selected>Choose Gender</option>
                                    <option value="male">Male</option>
                                    <option value="Female">Female</option>
                                </select>                                   
                                <input required name="phoneNumber" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Phone number">
                                <label for="profile-image" class="custom-file-upload mt-2">
                                    <input required name="profileImage" id="profile-image" class="w-100 profile-update-input px-3" type="file">
                                    Choose Profile Image
                                    <img width="50" height="20" class="choose-profile-image img-fluid" src="{{ asset('images/profile-icon.png')}}" alt="profile-image">
                                </label>
                                <button type="submit" class="w-100 text-uppercase mt-4 mb-3 addStudentFileButton">Add student</button>
                            </form>                               
                </div>
         </div>
  </div>
</div>
    @include('admin.StudentYear4')
    @include('admin.StudentYear3')
    @include('admin.StudentYear2')
    @include('admin.StudentYear1')

  
   

@endsection