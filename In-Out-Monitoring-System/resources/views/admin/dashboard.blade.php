@extends('layouts.adminlayout')

@section('admincss')
    <link rel="stylesheet" href="{{ asset('css/admin/dashboard.css') }}">
@endsection

@section('title-addition')
    Dashboard
@endsection

@section('dashboard-heading')
    Dashboard
@endsection

@section('dashboard-content')

<div class="col-12 mt-3 dashboard-button-container">
    <div class="row">
        <div class="col-md-6 mb-md-0 mb-3">
            <button type="button" class="button d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#addusers">
                <p class="me-auto button-text pt-3">Add new user</p>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="30" viewBox="0 0 46 30" fill="none">
                        <rect x="0.5" y="0.5" width="45" height="28" fill="white"/>
                        <rect x="0.5" y="0.5" width="45" height="28" stroke="white" stroke-opacity="0.76"/>
                        <rect x="0.5" y="0.5" width="45" height="28" stroke="#239F49"/>
                        <line x1="23.5" y1="0.964355" x2="23.5" y2="29.0357" stroke="white" stroke-opacity="0.76"/>
                        <line x1="23.5" y1="0.964355" x2="23.5" y2="29.0357" stroke="#239F49"/>
                        <line x1="7" y1="14.5" x2="40" y2="14.5" stroke="white" stroke-opacity="0.76"/>
                        <line x1="7" y1="14.5" x2="40" y2="14.5" stroke="#239F49"/>
                      </svg>
                </div>
            </button>

            <!-- Modal -->
            <div class="modal fade" id="addusers" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="adduserlabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                    <div class="modal-header border-0">
                    <h5 class="modal-title text-uppercase" id="adduserlabel">Add new user</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        {{-- Add users --}}
                        <form action="{{url('admin/addUsers')}}" method="POST">
                            @csrf
                            <select name="userRole" required class=" custom-select w-100 px-3 userInput pe-5">
                                <option disabled selected>Select user type</option>
                                <option value="security-guard">Security Guard</option>
                                <option value="sso">SSO</option>
                                <option value="councilor">Councilor</option>
                                <option value="HR_Head">HR Head</option>
                                <option value="HR">HR</option>
                              </select>
                              <select name="userGender" required class=" custom-select mt-3 w-100 px-3 userInput pe-5">
                                <option disabled selected>Select user gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                              </select>
                            <input name="email" required type="email" class="w-100 px-3 mt-3 userInput" placeholder="Enter email address">
                            <input name="userName" required  class="w-100 px-3 mt-3 userInput"  type="text" placeholder="Enter user name">
                            <button class="w-100 px-3 mt-5 mb-4 userbutton text-uppercase"  type="submit">Add user</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <button class="button d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#addNewStudentModal">
                <p class="me-auto button-text pt-3">Add new students</p>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="30" viewBox="0 0 46 30" fill="none">
                        <rect x="0.5" y="0.5" width="45" height="28" fill="white"/>
                        <rect x="0.5" y="0.5" width="45" height="28" stroke="white" stroke-opacity="0.76"/>
                        <rect x="0.5" y="0.5" width="45" height="28" stroke="#239F49"/>
                        <line x1="23.5" y1="0.964355" x2="23.5" y2="29.0357" stroke="white" stroke-opacity="0.76"/>
                        <line x1="23.5" y1="0.964355" x2="23.5" y2="29.0357" stroke="#239F49"/>
                        <line x1="7" y1="14.5" x2="40" y2="14.5" stroke="white" stroke-opacity="0.76"/>
                        <line x1="7" y1="14.5" x2="40" y2="14.5" stroke="#239F49"/>
                      </svg>
                </div>
            </button>

            <!-- File Upload Modal -->
            <div class="modal fade" id="addNewStudentModal" data-bs-backdrop="static" tabindex="-1" aria-labelledby="addNewStudentModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content px-5 py-3 addStudentModal">
                    <div class="modal-header border-0">
                    <h1 class="modal-title fs-5 addstudent-heading" id="addNewStudentModalLabel">Add New Student</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p class="addnewStudent-sub-heading mb-3">Add list of students at once **</p>
                        <form  action="{{ route('importStudentExcel') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <label for="excelFile" class="custom-file-upload">
                                <input name="excel_file" required id="excelFile" class="w-100 excelFileInput px-3"  type="file" accept=".xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" onchange="updateFileName()">
                                <span id="file-name-label">Choose a file (accepted file format .xlsx)</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="16" viewBox="0 0 30 16" fill="none">
                                    <rect width="30" height="16" fill="#D9D9D9"/>
                                    <path d="M15.3536 4.64645C15.1583 4.45118 14.8417 4.45118 14.6464 4.64645L11.4645 7.82843C11.2692 8.02369 11.2692 8.34027 11.4645 8.53553C11.6597 8.7308 11.9763 8.7308 12.1716 8.53553L15 5.70711L17.8284 8.53553C18.0237 8.7308 18.3403 8.7308 18.5355 8.53553C18.7308 8.34027 18.7308 8.02369 18.5355 7.82843L15.3536 4.64645ZM15.5 12V5H14.5V12H15.5Z" fill="black" fill-opacity="0.6"/>
                                </svg>
                            </label>
                            <div class="mt-3 prerequisite-message">
                                <p>To add a student, the following required columns must be filled out with valid values:</p>
                                <ul>
                                    <li>- student_id</li>
                                    <li>- student_name</li>
                                    <li>- year</li>
                                    <li>- course_name</li>
                                    <li>- hostel_block</li>
                                    <li>- gender</li>
                                    <li>- phone_number</li>
                                </ul>
                                <p style="color: red">*** Check and confirm each column's data for accuracy, ensuring it follows specified names and is complete to maintain data integrity.</p>
                            </div>
                            
                            <script>
                                function updateFileName() {
                                    const fileInput = document.getElementById("excelFile");
                                    const fileNameLabel = document.getElementById("file-name-label");
                        
                                    if (fileInput.files.length > 0) {
                                        const fileName = fileInput.files[0].name;
                                        // You can customize the number of characters to display here.
                                        const maxLength = 30; // Change this to your preferred length.
                        
                                        if (fileName.length > maxLength) {
                                            fileNameLabel.textContent = fileName.substr(0, maxLength) + " ...";
                                        } else {
                                            fileNameLabel.textContent = fileName;
                                        }
                                    } else {
                                        fileNameLabel.textContent = "Choose a file (accepted file format .xlsx)";
                                    }
                                }
                            </script>                          
                           <button class="w-100 text-uppercase addStudentFileButton" type="submit">Upload</button>
                        </form>
                        <p class="text-center mt-4">OR</p>
                      <button class="w-100 addMuallyButton mb-4" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#addManuallyModal">ADD MANUALLY</button>
                    </div>
                </div>
                </div>

            </div>
            <!-- Modal for manual entry -->
            <div class="modal fade" id="addManuallyModal" data-bs-backdrop="static" tabindex="-1" aria-labelledby="addManuallyModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content px-5 py-3 addStudentModal">
                            <div class="modal-header border-0">
                            <h1 class="modal-title fs-5 addstudent-heading" id="addNewStudentModalLabel">Add New Student</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                {{-- ADD STUDENT MANUALLY --}}
                                <form action="{{ url('admin/studentRecord') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input required name="studentID" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Enter student number">
                                    <input required name="studentName" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Student name">
                                    <select name="studentYear" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                        <option disabled selected>Select Year</option>
                                        <option class="selectOptions" value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                      </select>
                                      <select name="studentCourse" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                        <option disabled selected>Select course</option>
                                        <option value="IT">IT</option>
                                        <option value="CS">CS</option>
                                        <option value="Block Chain">Bloack Chain</option>
                                        <option value="AI">AI</option>
                                        <option value="Fullstack">Fullstack</option>
                                      </select>
                                      <select name="hotelBlock" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                        <option disabled selected>Select Hostel Block</option>
                                        <option value="k">Block K</option>
                                        <option value="m">Block M</option>
                                        <option value="l">Block L</option>
                                        <option value="n">Block N</option>
                                      </select>
                                      <select name="gender" required class="mt-2 custom-select w-100 px-3 addstudentInput optionInput pe-5">
                                        <option disabled selected>Choose Gender</option>
                                        <option value="male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>                                   
                                    <input required name="phoneNumber" type="text" class="w-100 mt-2 px-3 addstudentInput" placeholder="Phone number">
                                    <label for="profile-image" class="custom-file-upload mt-2">
                                        <input required name="profileImage" id="profile-image" class="w-100 profile-update-input px-3" type="file">
                                        Choose Profile Image
                                        <img width="50" height="20" class="choose-profile-image img-fluid" src="{{ asset('images/profile-icon.png')}}" alt="profile-image">
                                    </label>
                                    <button type="submit" class="w-100 text-uppercase mt-4 mb-3 addStudentFileButton">Add student</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="col-md-12 mt-3" id="chartUser"></div>

        <script>
            // Function to capitalize the first letter of a string
            function capitalizeFirstLetter(string) {
                return string.toUpperCase();
            }

            // Get the roles and capitalize them
            var roles = {!! json_encode(array_column($chartData, 'role')) !!}.map(function(role) {
                return capitalizeFirstLetter(role);
            });
            var grandTotal = Array.from({ length: {!! count($chartData) !!}}, (_, i) =>
                    {!! json_encode(array_column($chartData, 'role_total')) !!}.slice(0, i + 1).reduce((a, b) => a + b, 0)
                );
            var lastIndex = grandTotal.length - 1;
            var options = {
                series: [
                         {
                            name: 'Female',
                            data: {!! json_encode(array_column($chartData, 'female_total')) !!}
                        },
                        {
                            name: 'Male',
                            data: {!! json_encode(array_column($chartData, 'male_total')) !!},
                        },
                        {
                            name: 'Total',
                            data: {!! json_encode(array_column($chartData, 'role_total')) !!}
                        },
                        ],
                annotations: {
                position: 'front',
                yaxis: [
                        {
                            y: grandTotal,
                            label: {
                                style: {
                                    fontSize: '15px',
                                    color: '#fff',
                                    background: '#F56E2C',
                                    padding: {
                                        top: 7, 
                                        right: 7, 
                                        bottom: 7, 
                                        left: 7 
                                    }
                                },
                                text: 'Total User: ' + grandTotal[lastIndex],
                            }
                        }
                    ]
                },
                chart: {
                type: 'bar',
                height: 250
                },
                plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
                },
                dataLabels: {
                enabled: false
                },
                stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
                },
                xaxis: {
                categories: roles,
                },
                yaxis: {
                title: {
                    text: 'No. of users'
                }
                },
                colors: ['#F56E2C','#239F49','#ab1f23'],

                fill: {
                opacity: 1
                },
                tooltip: {
                y: {
                    formatter: function (val) {
                    return val
                    }
                }
                }
                };
          var chart = new ApexCharts(document.querySelector("#chartUser"), options);
          chart.render();
        </script>
        

        <div class="col-md-12 mt-3" id="chartStudent">
        </div>
        <script>
        var options = {
                series: [{
                name: 'Female',
                data: [{{$totalgirlnumber}}]
                },
                {
                name: 'Male',
                data: [{{$totalboynumber}}]
                },
            ],
                chart: {
                type: 'bar',
                height: 200,
                stacked: true,
                },
                plotOptions: {
                bar: {
                    horizontal: true,
                    dataLabels: {
                    total: {
                        enabled: true,
                        offsetX: 0,
                        style: {
                        fontSize: '11px',
                        fontWeight: 900
                        }
                    }
                    }
                },
                },
                stroke: {
                width: 1,
                colors: ['#fff']
                },
                title: {
                text: 'Student Gender Distribution'
                },
                colors: ['#F56E2C','#239F49',],
                xaxis: {
                categories: ['Student Gender'],
                labels: {
                    formatter: function (val) {
                    return val
                    }
                }
                },
                yaxis: {
                title: {
                    text: undefined
                },
                },
                tooltip: {
                y: {
                    formatter: function (val) {
                    return val
                    }
                }
                },
                fill: {
                opacity: 1
                },
                legend: {
                position: 'top',
                horizontalAlign: 'left',
                offsetX: 40
                }
                };
            var chart = new ApexCharts(document.querySelector("#chartStudent"), options);
            chart.render();

        </script>



   @endsection



