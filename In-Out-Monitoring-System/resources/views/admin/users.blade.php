@extends('layouts.adminlayout')

@section('admincss')
    <link rel="stylesheet" href="{{ asset('css/admin/users.css') }}">
@endsection

@section('title-addition')
    Users
@endsection

@section('dashboard-heading')
    Users Record
@endsection

@section('header-content')
<div class="small-screen-displayNone">
    <button class="addNewuser text-uppercase" data-bs-toggle="modal" data-bs-target="#addusers">add new user</button>
</div>
<div class="small-screen-displayNone">
    <form class="searchForm"  action="{{ route('adminUsers') }}" method="get">
        <input class="px-3 me-4 h-100 w-100" type="search" name="search" id="search" placeholder="Search users by Name or Role" value="{{ request('search') }}">
        <button type="submit" id="searchIcon">
            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/></svg>
        </button>
    </form>
</div>
    
@endsection

@section('dashboard-content')
    <!-- Add User Modal -->
    <div class="modal fade" id="addusers" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="adduserlabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content py-3 px-5" style="border-radius: 20px;">
            <div class="modal-header border-0">
            <h5 class="modal-title text-uppercase" id="adduserlabel">Add new user</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                {{-- Add new users --}}
                <form action="{{url('admin/addUsers')}}" method="POST">
                    @csrf
                    <select name="userRole" required class=" custom-select w-100 px-3 userInput pe-5">
                        <option disabled selected>Select user type</option>
                        <option value="security-guard">Security Guard</option>
                        <option value="sso">SSO</option>
                        <option value="councilor">Councilor</option>
                        <option value="HR_Head">HR Head</option>
                        <option value="HR">HR</option>
                      </select>
                      <select name="userGender" required class=" custom-select mt-3 w-100 px-3 userInput pe-5">
                        <option disabled selected>Select user gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                      </select>
                    <input name="email" required type="email" class="w-100 px-3 mt-3 userInput" placeholder="Enter email address">
                    <input name="userName" required  class="w-100 px-3 mt-3 userInput"  type="text" placeholder="Enter user name">
                    <button class="w-100 px-3 mt-5 mb-4 userbutton text-uppercase"  type="submit">Add user</button>
                </form>
            </div>
        </div>
        </div>
    </div>

    {{-- user count section --}}
    <div class="col-md-12 mt-3" id="chartUser"></div>

    <script>
      // Assuming $roleCounts is passed from your Laravel controller
      var roleCounts = {!! json_encode($roleCounts) !!};
      // Function to capitalize the first letter of a string
      function capitalizeFirstLetter(string) {
                return string.toUpperCase();
            }
    
      var seriesData = roleCounts.map(roleCount => ({
        name: capitalizeFirstLetter(roleCount.role),
        data: [roleCount.count],
      }));
    
      var options = {
        series: seriesData,
        chart: {
          type: 'bar',
          height: 150,
          stacked: true,
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              total: {
                enabled: true,
                offsetX: 0,
                style: {
                  fontSize: '11px',
                  fontWeight: 900
                }
              }
            }
          },
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        title: {
          text: 'User Role Distribution'
        },
        xaxis: {
          categories: [''],
          labels: {
            formatter: function (val) {
              return val;
            }
          }
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val;
            }
          }
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: 'top',
          horizontalAlign: 'left',
          offsetX: 40
        }
      };
    
      var chart = new ApexCharts(document.querySelector("#chartUser"), options);
      chart.render();
    </script>
    
    <div class="col-md-12 mt-0 section-container d-flex flex-wrap">
        <button class="sortStudentButtonBlock buttonAll buttonActive text-uppercase px-4" data-role="All">All</button>
        <button data-role="security-guard" class="sortStudentButtonBlock text-uppercase px-4">security-guard</button>
        <button data-role="sso" class="sortStudentButtonBlock text-uppercase px-4">sso</button>
        <button data-role="councilor" class="sortStudentButtonBlock text-uppercase px-4">councilor</button>
        <button data-role="HR" class="sortStudentButtonBlock text-uppercase px-4">HR</button>
        <button data-role="HR_Head" class="sortStudentButtonBlock text-uppercase px-4">Hr_Head</button>
    
        
    </div>
    {{-- user rocord table section --}}
    <div class=" col-md-12  table-responsive userCountSection mt-3">
        @if(count($users) > 0)
        <table id="userTable" class="table table-bordered custom-table">
            <thead>
                <tr class="table-title py-3">
                    <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                    <th scope="col">USER NAME</th>
                    <th scope="col">EMAIL ADDRESS</th>
                    <th scope="col">ROLE</th>
                    <th scope="col">GENDER</th>
                    <th scope="col">PROFILE</th>
                    <th scope="col" class="rounded-top-right text-center">ACTION</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $count = 0;
                @endphp
                @foreach ($users as $user)
                <tr>
                    <td class="ps-3">{{$count+1 }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role }}</td>
                    <td>{{ $user->gender }}</td>
                    @if ($user->profile_image !== null)
                    <td class="text-center">
                        <img style="border-radius: 5px;background:rgb(120, 117, 117);object-fit:fill;object-position:center" width="50" height="30" src="{{ asset('profile_images/' . $user->profile_image ) }}" alt="{{ $user->profile_image  }}">
                     </td>
                     @else
                     <td class="text-center">
                        <span class="d-flex ms-auto me-auto justify-content-center align-items-center" style="border-radius: 5px;background:#fff;width:50px;height:30px;color:#000;border:1px solid #F56E2C;">{{ strtoupper(substr(current(explode(' ', $user->name)), 0, 1)) }} {{ strtoupper(substr(last(explode(' ', $user->name)), 0, 1)) }}</span> 
                     </td>
                     @endif
                    <td class="actionbutton d-flex gap-3 text-center justify-content-center align-items-center">
                        <button class="editButton" data-bs-toggle="modal" data-bs-target="#EditUsers{{$user->id}}">
                            <i class="fa-solid fa-pen" style="color: #454444; font-size: 16px;"></i>
                        </button>
                        <button class="deleteButton" data-bs-toggle="modal" data-bs-target="#deleteUser{{$user->id}}">
                            <i class="fa-solid fa-trash-can" style="color: #454444; font-size: 16px;"></i>
                        </button>
                    </td>
                </tr>
                 <!-- Edit users -->
                <div class="modal fade" id="EditUsers{{$user->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="EditUsers" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                        <div class="modal-header border-0">
                        <h5 class="modal-title text-uppercase" id="adduserlabel">Edit {{ $user->role }}, <span style="color: green">{{ $user->name }}</span></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form  action="{{ route('user.update', ['id' => $user->id]) }}" method="POST">
                                @csrf
                                @method('PUT') {{-- Use PUT method for updating --}}
                                <select name="userRole" required class="custom-select w-100 px-3 userInput pe-5">
                                    @foreach(['security-guard', 'sso', 'councilor', 'HR_Head', 'HR'] as $role)
                                        <option value="{{ $role }}" @if($role === $user->role) selected @endif>{{ ucfirst(str_replace('_', ' ', $role)) }}</option>
                                    @endforeach
                                </select>
                                <input name="email" required type="email" class="w-100 px-3 mt-3 userInput" placeholder="Enter email address" value="{{ $user->email }}">
                                <input name="userName" required  class="w-100 px-3 mt-3 userInput"  type="text" placeholder="Enter user name" value="{{ $user->name }}">
                                <button class="w-100 px-3 mt-5 mb-4 userbutton text-uppercase"  type="submit">Update</button>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- Delete user Modal -->
                    <div class="modal fade" id="deleteUser{{$user->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteUserLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content px-4 py-3 deleteModal">
                                <div class="modal-header">
                                    <h1 class="modal-title deleteModalTitle fs-5" id="deleteUserLabel">Delete User</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p class="confirm-text">Are you sure you want to delete this user, <span style="color:red"> {{ $user->name }}({{ $user->role }}) </span>?</p>
                                </div>
                                <div class="modal-footer d-flex flex-row justify-content-start my-2">
                                    <form action="{{url('/delete-user')}}" method="POST">
                                        @csrf
                                        <input hidden  type="email" name="email" value="{{$user->email}}">
                                        <button type="submit" class="btnDeleteYes">Yes</button>
                                    </form>
                                    <button type="button" class="btnCancel" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $count++;
                    @endphp
                @endforeach
                <tr id="noDataMatchedMessage" class=" alert-danger text-center" style="display: none;">
                    <td colspan="8">Oops, sorry! No data for the selected block</td>
                </tr>
            </tbody>
        </table> 
        @else
        <h5 class="text-center" style="color:red">Opps ! 🥺 <br> No Matched record found!</h5>
        @endif
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script>
          $(document).ready(function () {
            $(".sortStudentButtonBlock").on("click", function () {
              var selectedBlock = $(this).data("role");
        
              // Update button states
              $(".sortStudentButtonBlock").removeClass("buttonActive");
              $(this).addClass("buttonActive");
        
              // Show/hide rows based on the selected block
              if (selectedBlock === "All") {
                $("#userTable tbody tr").show();
                $("#noDataMatchedMessage").hide();
              } else {
                var matchingRows = $("#userTable tbody tr").filter(function () {
                  return $(this).find("td:eq(3)").text() === selectedBlock;
                });
        
                if (matchingRows.length > 0) {
                  $("#userTable tbody tr").hide();
                  matchingRows.show();
                  $("#noDataMatchedMessage").hide();
                } else {
                  // Display a message if no data matched
                  $("#userTable tbody tr").hide();
                  $("#noDataMatchedMessage").show();
                }
              }
            });
          });
        </script>
        
    </div>
@endsection