<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if(isset($_POST["submit"]))
{
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["excelFile"]["name"]);
    $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    
    if($fileType == "xlsx")
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    }
    else
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
    }

    $spreadsheet = $reader->load($_FILES["excelFile"]["tmp_name"]);
    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
    
    foreach($sheetData as $row)
    {
        $studentName = mysqli_real_escape_string($conn, $row['