@extends('layouts.adminlayout')

@section('admincss')
    <link rel="stylesheet" href="{{asset('css/profile-setting.css')}}">
@endsection

@section('title-addition')
    Setting
@endsection

@section('dashboard-heading')
    Profile Setting
@endsection

@section('dashboard-content')
    @include('partials.Profile-setting')
@endsection




