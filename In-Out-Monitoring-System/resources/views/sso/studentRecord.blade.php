@extends('layouts.ssoLayout')
@section('title-addition')
    Student Record
@endsection

@section('SSOcss')
    <link rel="stylesheet" href="{{asset('css/sso/StudentRecord.css')}}">
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'> 
    <style>
        .page-item{
            padding: 5px !important;
        }
        .page-link{
            border-radius: 20px !important;
            color: #F56E2C !important;
            font-weight: 500;
            border: 1px solid #F56E2C !important;
        }
        .page-link:hover{
            background: #F56E2C !important;
            color:#fff !important ;

        }
        .page-item.active .page-link{
            background-color: #F56E2C !important;
            border: 1px solid #f25405 !important;
            color: #fff !important;
            font-weight: 600;
       }
       .page-item.disabled .page-link{
        border: 1px solid #00000051 !important;
        color: rgba(82, 82, 82, 0.646) !important;
       }

    /* Customize the datepicker style */
    .ui-datepicker-header {
      background: #4CAF50 !important;
      color: white;
    }
    .ui-datepicker{
        max-width: 16rem;
    }
    .ui-datepicker-calendar {
      background-color: #ffff; 
    }
    .ui-datepicker-calendar tbody a:hover {
      background: #4CAF50 ; 
      color: white;
    }
    /* Style for the date icon */
    .date-icon {
      position: absolute;
      left:10px;
      top: 50%;
      transform: translateY(-50%);
      cursor: pointer;
    }
    #my_date_picker{
        border-radius: 10px;
        border: 1.4px solid #F56E2C;
    }
    #my_date_picker:active{
        border: 1.4px solid #F56E2C!important;
    }
    #my_date_picker{
    caret-color: transparent;
    }

    #my_date_picker{
    caret-color: transparent;
    }
    #showAllButton,#showMoreBtn,#showLessBtn{
        border: none;
        border-radius: 10px;
        background: #F56E2C;
        color: #fff;
    }
    #studentTable tr.hide {
      display: none;
    }
    </style>
@endsection
@section('dashboard-heading')
    Student Record
@endsection
@section('dashboard-content')
 
<h5 class="mt-3" style="font-family: Montserrat;font-size: 16px;font-weight: 700;color:red;">Students who are out of campus -No Reason</h5>
<div class="col-md-12 mt-3 mb-0 section-container d-flex flex-wrap">
    <button class="sortStudentButtonBlock buttonAll buttonActive px-4" data-block="All">All</button>
    @foreach ($distinctHotelBlocks as $b)
      <button data-block="{{ $b->hotelBlock }}" class="sortStudentButtonBlock px-4">Block {{ $b->hotelBlock }}</button>
    @endforeach
</div>
<div class="col-md-12">
    <div class=" col-md-12 section-container mt-3 table-responsive">
        <table id="studentCheckOutTable" class="table table-bordered custom-table">
            <thead>
                <tr class="table-title py-3">
                    <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                    <th scope="col">ID</th>
                    <th scope="col">NAME</th>
                    <th scope="col">YEAR</th>
                    <th scope="col">COURSE</th>
                    <th scope="col">BLOCK</th>
                    <th scope="col" class="rounded-top-right">CHECK-OUT-DATE-TIME</th>
                </tr>
            </thead>
            <tbody>
                @php
                  $slno = 1;  
                @endphp
         @if(count($studentCheckoutRecord) > 0)
             @foreach($studentCheckoutRecord as $log)
         
             <tr>
                 <td class="ps-3">{{ $slno }}</td>
                 <td>{{ $log->studentID }}</td>
                 <td>{{ $log->studentName }}</td>
                 <td>{{ $log->studentYear }}</td>
                 <td>{{ $log->studentCourse }}</td>
                 <td>{{ $log->hotelBlock }}</td>
                 <td>{{ $log->checkOutDateTime }}</td>
             </tr>
         
             @php
                 $slno++;
             @endphp
         @endforeach
         @else
         <tr  class="alert-warning text-center">
             <td colspan="7">Oops, sorry! No data available...</td>
         </tr>
         @endif
         <tr id="noDataMatchedMessage" class=" alert-danger text-center" style="display: none;">
            <td colspan="9">Oops, sorry! No data for the selected block</td>
        </tr>
         
    
            </tbody>
        </table>
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script>
          $(document).ready(function () {
            $(".sortStudentButtonBlock").on("click", function () {
              var selectedBlock = $(this).data("block");
        
              // Update button states
              $(".sortStudentButtonBlock").removeClass("buttonActive");
              $(this).addClass("buttonActive");
        
              // Show/hide rows based on the selected block
              if (selectedBlock === "All") {
                $("#studentCheckOutTable tbody tr").show();
                $("#noDataMatchedMessage").hide();
              } else {
                var matchingRows = $("#studentCheckOutTable tbody tr").filter(function () {
                  return $(this).find("td:eq(5)").text() === selectedBlock;
                });
        
                if (matchingRows.length > 0) {
                  $("#studentCheckOutTable tbody tr").hide();
                  matchingRows.show();
                  $("#noDataMatchedMessage").hide();
                } else {
                  // Display a message if no data matched
                  $("#studentCheckOutTable tbody tr").hide();
                  $("#noDataMatchedMessage").show();
                }
              }
            });
          });
        </script>
    </div>
</div>


{{-- On Leave out of campus --}}
<h5 class="mt-3" style="font-family: Montserrat;font-size: 16px;font-weight: 700;color:rgb(24, 121, 5);">Students who are out of campus -On Leave</h5>
<div class="col-md-12 mt-3 mb-0 section-container d-flex flex-wrap">
    <button class="sortStudentButtonBlockLeave buttonAll buttonActive px-4" data-leaveblock="All">All</button>
    @foreach ($distinctHotelBlocks as $b)
      <button data-leaveblock="{{ $b->hotelBlock }}" class="sortStudentButtonBlockLeave px-4">Block {{ $b->hotelBlock }}</button>
    @endforeach
    <script>
        $(document).ready(function () {
          $(".sortStudentButtonBlockLeave").on("click", function () {
            var selectedBlock = $(this).data("leaveblock");
      
            // Update button states
            $(".sortStudentButtonBlockLeave").removeClass("buttonActive");
            $(this).addClass("buttonActive");
      
            // Show/hide rows based on the selected block
            if (selectedBlock === "All") {
              $("#studentCheckOutTableLeave tbody tr").show();
              $("#noDataMatchedMessageLeave").hide();
            } else {
              var matchingRows = $("#studentCheckOutTableLeave tbody tr").filter(function () {
                return $(this).find("td:eq(5)").text() === selectedBlock;
              });
      
              if (matchingRows.length > 0) {
                $("#studentCheckOutTableLeave tbody tr").hide();
                matchingRows.show();
                $("#noDataMatchedMessageLeave").hide();
              } else {
                // Display a message if no data matched
                $("#studentCheckOutTableLeave tbody tr").hide();
                $("#noDataMatchedMessageLeave").show();
              }
            }
          });
        });
      </script>
</div>
<div class="col-md-12">
    <div class=" col-md-12 section-container mt-3 table-responsive">
        <table id="studentCheckOutTableLeave" class="table table-bordered custom-table">
            <thead>
                <tr class="table-title py-3">
                    <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                    <th scope="col">ID</th>
                    <th scope="col">NAME</th>
                    <th scope="col">YEAR</th>
                    <th scope="col">COURSE</th>
                    <th scope="col">BLOCK</th>
                    <th scope="col" class="rounded-top-right">CHECK-OUT-DATE-TIME</th>
                </tr>
            </thead>
            <tbody>
                @php
                  $slno = 1;  
                @endphp
            @if(count($studentCheckoutRecordOnleave) > 0)
                @foreach($studentCheckoutRecordOnleave as $log)
                    <tr>
                        <td class="ps-3">{{ $loop->iteration }}</td>
                        <td>{{ $log->studentID }}</td>
                        <td>{{ $log->studentName }}</td>
                        <td>{{ $log->studentYear }}</td>
                        <td>{{ $log->studentCourse }}</td>
                        <td>{{ $log->hotelBlock }}</td>
                        <td>{{ $log->checkOutDateTime }}</td>
                    </tr>
                @endforeach
                @else
                <tr  class="alert-warning text-center">
                    <td colspan="7">Oops, sorry! No data available...</td>
                </tr>
                @endif
    
                <tr id="noDataMatchedMessageLeave" class="alert-danger text-center" style="display: none;">
                    <td colspan="7">Oops, sorry! No data for the selected block</td>
                </tr>
        
            </tbody>
      
       
        </table>
    </div>
</div>


<div id="chartCheckInCheckOut" class="mt-3"></div>
<script>
    var checkInOutData = @json($checkInOutCounts);

    // Convert counts to integers and format the date
    checkInOutData.forEach(item => {
        item.checkin_count = parseInt(item.checkin_count);
        item.checkout_count = parseInt(item.checkout_count);
        item.checkDateFormatted = new Date(item.checkDate).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: '2-digit' });

    });

    var options = {
        series: [{
            name: 'Check In',
            data: checkInOutData.map(item => item.checkin_count),
            color: '#F56E2C'
        }, {
            name: 'Check Out',
            data: checkInOutData.map(item => item.checkout_count),
            color: '#239F49'
        }],
        chart: {
            height: 370,
            type: 'area',

        },
        title: {
            text: 'Check In & Check out Analysis',
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            type: 'category',
            categories: checkInOutData.map(item => item.checkDateFormatted)
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var chart = new ApexCharts(document.querySelector("#chartCheckInCheckOut"), options);
    chart.render();
</script>

<div class="col-md-12 section-container mt-4 d-flex justify-content-between align-items-center">
    <h4 class="ssoheading">Check-in & Check-out History</h4>
    <div class="d-flex gap-3">
        <button class="px-2" id="showAllButton" style="display:none;">Show All</button>
        <div style="position: relative;">
            <input class="me-2 p-2" type="text" placeholder="Sort by date" id="my_date_picker">
            <span class="date-icon"></span>
          </div>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <script>
            $(document).ready(function () {
                // Your existing datepicker initialization
                $("#my_date_picker").datepicker({
                    showOn: "both",
                    buttonImage: "{{ asset('images/calendar.png') }}",
                    buttonImageOnly: true,
                    beforeShow: function (input, inst) {
                        inst.dpDiv.addClass('my-datepicker');
                    }
                });

                $("#my_date_picker").on("keydown", function (e) {
                    e.preventDefault();
                });

                // Function to filter and show rows for the selected date
                function filterTableByDate(selectedDate) {
                    var foundRows = false;

                    // Loop through each table row
                    $('#studentTable tbody tr').each(function () {
                        var checkOutDate = $(this).find('td:eq(6)').text().split(' ')[0]; // Check-Out Date-Time
                        var checkInDate = $(this).find('td:eq(7)').text().split(' ')[0]; // Check-In Date-Time

                        // Check if either check-out or check-in date matches the selected date
                        if (checkOutDate === selectedDate || checkInDate === selectedDate) {
                            $(this).show(); // Show the row
                            foundRows = true;
                        } else {
                            $(this).hide(); // Hide the row if not a match
                        }
                    });

                    // Show/hide the message and button based on whether rows were found or not
                    if (foundRows) {
                        $('#noDateMessage').hide();
                        $('#showAllButton').show();
                    } else {
                        $('#noDateMessage').show();
                        $('#showAllButton').show();
                    }
                }

                // Event handler for datepicker change
                $("#my_date_picker").on("change", function () {
                    var selectedDate = $.datepicker.formatDate("yy-mm-dd", $("#my_date_picker").datepicker("getDate"));
                    filterTableByDate(selectedDate);
                });

                // Event handler for "Show All" button
                $('#showAllButton').on('click', function () {
                    $('#studentTable tbody tr').show();
                    $('#noDateMessage').hide();
                    $(this).hide();

                    // Reset the date input
                    $("#my_date_picker").datepicker("setDate", null);
                });
            });
        </script>
    </div>
</div>
<div class=" col-md-12 section-container mt-3 table-responsive">
    <table class="table table-bordered custom-table" id="studentTable">
        <thead>
            <tr class="table-title py-3">
                <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                <th scope="col">ID</th>
                <th scope="col">NAME</th>
                <th scope="col">YEAR</th>
                <th scope="col">COURSE</th>
                <th scope="col">BLOCK</th>
                <th scope="col">CHECK-OUT-DATE-TIME</th>
                <th scope="col" class="rounded-top-right">CHECK-IN-DATE-TIME</th>
            </tr>
        </thead>
        <tbody>
            @php
              $slno = 1;  
            @endphp
            @foreach($studentExitEntryRecord as $log)
            <tr class="data-row{{ $slno > 10 ? ' hide' : '' }}">
                <td class="ps-3">{{$slno}}</td>
                <td>{{ $log->student->studentID }}</td>
                <td>{{ $log->student->studentName }}</td>
                <td>{{ $log->student->studentYear }}</td>
                <td>{{ $log->student->studentCourse }}</td>
                <td>{{ $log->student->hotelBlock }}</td>
                <td>{{ $log->checkOutDateTime }}</td>  
                <td>{{ $log->checkInDateTime }}</td>                  
                
            </tr>
                @php
                $slno++;
                @endphp
                
            @endforeach
        </tbody>
    </table>
    <div id="noDateMessage" style="display: none;" class="text-center mt-2 mb-4">No matching entries for the selected date.</div>
    <div class="col-md-12 d-flex justify-content-end mb-4">
        <div class="ms-auto d-flex gap-3">
            <button id="showMoreBtn" class="btn px-4" style="{{ $slno > 10 ? '' : 'display:none' }}">Show More</button>
            <button id="showLessBtn" class="btn px-4" style="display:none">Show Less</button>
        </div>
    </div>

  
    <script>
      $(document).ready(function(){
        var pageSize = 10;
        var $tableRows = $('#studentTable tbody tr');
  
        if ($tableRows.length > pageSize) {
          $('#showMoreBtn').show();
        }
  
        $('#showMoreBtn').on('click', function(){
          $tableRows.filter(':hidden').slice(0, pageSize).slideDown();
          if ($tableRows.filter(':hidden').length === 0) {
            $('#showMoreBtn').hide();
          }
          $('#showLessBtn').show();
        });
  
        $('#showLessBtn').on('click', function(){
          $tableRows.slice(pageSize).slideUp();
          $('#showMoreBtn').show();
          $('#showLessBtn').hide();
        });
      });
    </script>
  
</div>

    {{ $studentExitEntryRecord->links('pagination::bootstrap-5') }}

@endsection