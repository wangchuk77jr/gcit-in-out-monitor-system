@extends('layouts.ssoLayout')
@section('title-addition')
    Student Leave
@endsection

@section('SSOcss')
    <link rel="stylesheet" href="{{asset('css/sso/StudentLeave.css')}}">
@endsection
@section('dashboard-heading')
    Student Leave
@endsection
@section('dashboard-content')
<div class="mt-2" id="chart"></div>

<script>
    // Use Blade syntax to inject PHP variables into JavaScript
    var pendingLeave = {{ $pendingLeave }};
    var totalLeave = {{ $totalLeave }};
    var startWeek = new Date("{{ $startWeek }}").toLocaleDateString('en-US', { month: 'short', day: 'numeric' });
    var endWeek = new Date("{{ $endWeek }}").toLocaleDateString('en-US', { month: 'short', day: 'numeric' });

    var options = {
        series: [{
            name: 'Pending Leave',
            data: [pendingLeave],
            color: '#239F49', // Color for the first bar
        }, {
            name: 'Total Leave',
            data: [totalLeave],
            color: '#F56E2C', // Color for the second bar
        }],
        chart: {
            type: 'bar',
            height: 150,
            stacked: true,
        },
        plotOptions: {
            bar: {
                horizontal: true,
                dataLabels: {
                    total: {
                        enabled: true,
                        offsetX: 50,
                        style: {
                            fontSize: '13px',
                            fontWeight: 700,
                        },
                        formatter: function (w) {
                            return pendingLeave.toString(); // Display the difference as the data label
                        }
                    }
                }
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title: {
            text: 'Pending Leave for this week ' + startWeek + ' to ' + endWeek,
        },
        xaxis: {
            categories: ['Leave Summery'],
            labels: {
                formatter: function (val) {
                    return val
                }
            }
        },
        yaxis: {
            title: {
                text: undefined
            },
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            position: 'top',
            horizontalAlign: 'left',
            offsetX: 40
        },
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
</script>

    <div class="col-md-12">
        <div class="row ${1| ,row-cols-2,row-cols-3, auto,justify-content-md-center,|}">
            <div class="col-md-6">
                <div class="numberStudentleave d-flex align-items-center pt-2 ps-4 gap-3">
                    <h4 style="color: #000;font-family: Montserrat;font-size: 30px;font-weight: 700;">{{$totalNumberLeave}}</h4>
                    <p class="mt-2" style="color: #000;font-family: Montserrat;font-size: 14px;font-weight: 400;">Numbers of students on leave</p>
                </div>
            </div>

            
            <div class="col-md-6 mt-md-0 mt-3">
                <div class="px-4 position-relative sortStudent d-flex justify-content-center align-items-center" >
                    <button class="btn dropdown_button ps-4 text-start position-relative w-100 dropdown"  style="width: 90%;border-radius: 20px;color:#000;" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Sort Student By ...
                        <span class="dropdown-arrow"><svg xmlns="http://www.w3.org/2000/svg" height="16" width="10" viewBox="0 0 320 512"><!--!Font Awesome Free 6.5.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path fill="#000000" d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z"/></svg></span>
                    </button>
                    <ul style="min-width: 90%;" class="dropdown-menu w-50 dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                      <li><a href="#" class="dropdown-item dropdown-item-sort-all">Show All</a></li>
                      <li class="dropstart">
                        <a href="#" class="dropdown-item dropdown-toggle" role="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" data-bs-offset="-9,2" aria-expanded="false">Sort by hostel block</a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                            @foreach ($distinctHotelBlocks as $block)
                                <li><a href="#" data-block="{{$block->hotelBlock}}" class="dropdown-item dropdown-item-sort-block">Block {{$block->hotelBlock}}</a></li>
                            @endforeach
                        </ul>
                      </li>
                     
                      <li class="dropstart">
                        <a href="#" class="dropdown-item dropdown-toggle" role="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" data-bs-offset="-9,2" aria-expanded="false">Sort by year</a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                            @foreach ($distinctYear as $year)
                                <li><a href="#" data-year="{{$year->studentYear}}" class="dropdown-item dropdown-item-sort-year">Year {{$year->studentYear}}</a></li>
                            @endforeach
                        </ul>
                      </li>
                      <li class="dropstart">
                        <a href="#" class="dropdown-item dropdown-toggle" role="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" data-bs-offset="-9,2" aria-expanded="false">Sort by course</a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                            @foreach ($distinctCourse as $course)
                                <li><a href="#" data-course="{{$course->studentCourse}}" class="dropdown-item dropdown-item-sort-course">{{$course->studentCourse}}</a></li>
                            @endforeach
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
                  <script>
                    document.querySelectorAll('.dropdown-menu .dropdown-toggle[href="#"]')
                        .forEach(function (element) {
                            element.addEventListener("click", function (event) {
                            event.stopPropagation();
                            });
                    });

                  </script>
                {{-- Sort student --}}
                <script>
                    $(document).ready(function () {
                        $('.dropdown-item-sort-block').on('click', function () {
                            var selectedBlock = $(this).data('block');
                            // Hide all rows initially
                            $('tbody').find('tr').hide();
                            // If "Show All" is selected, show all rows
                            if (selectedBlock === 'show all') {
                                $('tbody').find('tr').show();
                            } else {
                                // Show only rows with the selected hotel block
                                $('tbody').find('tr').each(function () {
                                    var blockColumnValue = $(this).find('td:eq(6)').text(); // Assuming the block column is at index 6
                                    if (blockColumnValue === selectedBlock) {
                                        $(this).show();
                                    }
                                });
                            }
                        });
                        $('.dropdown-item-sort-all').on('click', function () {
                            var sortBy = $(this).text().toLowerCase();
                
                            // If "Show All" is selected, show all rows
                            if (sortBy === 'show all') {
                                $('tbody').find('tr').show();
                            }
                        });
                
                        $('.dropdown-item-sort-year').on('click', function () {
                            var selectedYear = $(this).data('year');
                
                            // Hide all rows
                            $('tbody').find('tr').hide();
                
                            // Show only rows with matching year in the "YEAR" column
                            $('tbody').find('td:nth-child(6):contains(' + selectedYear + ')').closest('tr').show();
                        });

                        $('.dropdown-item-sort-course').on('click', function () {
                            var selectedCourse = $(this).data('course');

                            // Hide all rows
                            $('tbody').find('tr').hide();

                            // Show only rows with matching course in the "COURSE" column
                            $('tbody').find('td:nth-child(5):contains(' + selectedCourse + ')').closest('tr').show();
                        });
                    });
                
                    // Custom jQuery contains case-insensitive
                    jQuery.expr[':'].contains = function (a, i, m) {
                        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
                    };
                </script>
            </div>
        </div>
    </div>


    {{-- student leave record --}}
    <div class="col-md-12">
        <div class="col-md-12 section-container table-responsive mt-3">
            <table class="table studentLeaveTable  table-bordered custom-table">
                <thead>
                    <tr class="table-title py-3">
                        <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                        <th scope="col">SID</th>
                        <th scope="col">NAME</th>
                        <th scope="col">PHONE NUMBER</th>
                        <th scope="col">COURSE</th>
                        <th scope="col">YEAR</th>
                        <th scope="col">BLOCK</th>
                        <th scope="col">START DATE</th>
                        <th scope="col">END DATE</th>
                        <th scope="col" class="rounded-top-right text-center">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($studentLeaveRecord as $student)
                    <tr>
                        <td class="ps-3">{{ $loop->index + 1 }}</td>
                        <td>{{ $student->student->studentID }}</td>
                        <td>{{ $student->student->studentName }}</td>
                        <td>{{ $student->student->phoneNumber }}</td>
                        <td>{{ $student->student->studentCourse }}</td>
                        <td>{{ $student->student->studentYear }}</td>
                        <td>{{ $student->student->hotelBlock }}</td>
                        <td>{{ $student->StartDate}}</td>
                        <td>{{$student->EndDate}}</td>
                        @if ($student->LeaveApproval === null)
                        <td class="actionbutton d-flex gap-2 text-center justify-content-center align-items-center">
                                <button class="acceptButton px-3" data-bs-toggle="modal" data-bs-target="#approveModal{{ $student->leaveID }}">
                                    Accept
                                </button>
                                <button class="declineButton px-3" data-bs-toggle="modal" data-bs-target="#dclineStudent{{ $student->leaveID }}">
                                    Decline
                                </button>
                            
                        </td> 
                        @else  
                        <td>
                            <p class="text-capitalize text-center" style="color: {{ $student->LeaveApproval === 'accepted' ? 'green' : 'red' }}; font-weight: 900">
                                {{ $student->LeaveApproval }}
                            </p>                            
                        </td>
                        @endif
                    </tr>

                      <!-- Modal Approve-->
                    <div class="modal fade" id="approveModal{{$student->leaveID}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="approveModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content px-3 py-2">
                            <div class="modal-header border-0">
                            <h5 class="modal-title" id="approveModalLabel">Student Leave</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-wrap justify-content-between w-100 ">
                                    <p class="visitor-detail">{{ $student->student->studentID }}</p>
                                    <p class="visitor-detail">{{ $student->student->studentName }} </p>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between w-100 ">
                                    <p class="visitor-detail">{{ $student->student->studentYear }}</p>
                                    <p class="visitor-detail">{{ $student->student->studentCourse }}</p>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between w-100  ">
                                    <p class="visitor-detail">Start Date: {{ $student->StartDate}}</p>
                                    <p class="visitor-detail">End Date: {{ $student->EndDate}} </p>
                                </div>
                                <div class="w-100 ">
                                    <p class="visitor-detail p-3 w-100 h-auto">
                                        {{ $student->Reason}}
                                    </p>
                                </div>
                            </div>
                            <div class="border-top d-flex gap-4 py-4 justify-content-center">
                            <form action="{{url('sso/leaveApproval')}}" method="post">
                                @csrf
                                <input hidden type="text" value="{{$student->leaveID}}" name="leaveID">
                                <input hidden type="text" name="leaveApproval" value="accepted">
                                <input hidden type="text" name="studentID" value="{{$student->student->studentID}}">
                                <input hidden type="text" name="message" value="Your leave request, which is from {{ $student->StartDate}} to{{ $student->EndDate}}, has been approved.">
                                <button type="submit" class="approve-button text-uppercase px-5 w-50">Accept</button>
                            </form>
                            <button type="button" class="decline-button w-50 text-uppercase px-5" data-bs-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                         </div>
                    </div>
                    <!-- Modal Decline -->
                    <div class="modal fade" id="dclineStudent{{ $student->leaveID }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="declineModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content px-3 py-2">
                            <div class="modal-header">
                            <h5 class="modal-title" id="declineModalLabel">Student Leave Rejection</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                            <p>
                                Are you sure you want reject <span style="color: red">{{ $student->student->studentName }}’s</span>  leave application?                            </p>
                            </div>
                            <div class="modal-footer justify-content-start">
                            <form action="{{url('sso/leaveApproval')}}" method="POST">
                                @csrf
                                <input hidden type="text" value="{{$student->leaveID}}" name="leaveID">
                                <input hidden type="text" name="leaveApproval" value="declined">
                                <input hidden type="text" name="studentID" value="{{$student->student->studentID}}">
                                <textarea required name="message"  class="w-100 p-4" cols="100" rows="3" placeholder="Please enter the reason for rejecting the student's leave approval"></textarea>
                                <div class="mt-2">
                                    <button type="submit" class="px-5 decline-button-form">Decline</button>
                                    <button type="button" class="px-5 ms-md-3 decline-button" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12" id="studentLeaveChart"></div>
    <script>
        var currentYear = new Date().getFullYear(); // Get the current year

        var options = {
            series: [{
                name: 'Applied Leave',
                data: {!! json_encode($appliedLeaveData) !!},
                color: '#F28C28' 
    
            }, {
                name: 'Accepted Leave',
                data: {!! json_encode($acceptedLeaveData) !!},
                color: '#239F49' 
            }, {
                name: 'Declined Leave',
                data: {!! json_encode($declinedLeaveData) !!},
                color: '#FF5733' 
            }],
            chart: {
                type: 'bar',
                height: 300,
                title: 'Leave Statistics by Month' // Add your desired title here
            },
            title: {
            text: 'Leave Statistics by Month for ' + currentYear,
        },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: {!! json_encode($months) !!}.map(function(month, index) {
                    if (month === 1) {
                        return 'Jan';
                    } else if (month === 2) {
                        return 'Feb';
                    } else if (month === 3) {
                        return 'Mar';
                    } else if (month === 4) {
                        return 'Apr';
                    } else if (month === 5) {
                        return 'May';
                    } else if (month === 6) {
                        return 'Jun';
                    } else if (month === 7) {
                        return 'Jul';
                    } else if (month === 8) {
                        return 'Aug';
                    } else if (month === 9) {
                        return 'Sep';
                    } else if (month === 10) {
                        return 'Oct';
                    } else if (month === 11) {
                        return 'Nov';
                    } else if (month === 12) {
                        return 'Dec';
                    }
    
                    return month;
                }),
                labels: {
                    rotate: 0,
                    style: {
                        fontSize: '12px'
                    }
                }
            },
            yaxis: {
                title: {
                    text: 'No of leave'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            }
        };
    
        var chart = new ApexCharts(document.querySelector("#studentLeaveChart"), options);
        chart.render();
    </script>
    
    
    
@endsection