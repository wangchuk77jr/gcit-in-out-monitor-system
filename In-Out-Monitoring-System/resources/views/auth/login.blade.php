@extends('layouts.generalbase')

@section('title')
In & Out | Sign In 
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6 vh-100 left-section">
      <img src="{{ asset('images/loginbg.png')}}" alt="">
    </div>
    <div class="col-md-6 vh-100 right-section pt-5">
      <!-- Login Form -->
          <form method="POST" action="{{ route('login') }}" class="px-md-5 mt-5" autocomplete="off">
              @csrf
                <h1 class="signin-heading">Sign In <br> <span class='sign-title d-none text-uppercase'>GCIT In & Out Monitoring System</span></h1>
                      <div class="d-flex flex-column gap-3 mt-5">
                      @error('email')
                        <span class="alert-danger p-2">{{ $message }}</span>
                      @enderror
                      <!-- Session Status -->
                      @if (session('status'))
                        <div class="p-2 alert-success">
                            {{ session('status') }}
                        </div>
                      @endif
                        <!-- Email address -->
                          <input class="sign-in-input ps-3" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="username" placeholder="Email address">
                            <div class="input-group position-relative">

                              <!-- Password -->
                                <input class=" ps-3 sign-in-input w-100" id="password" type="password" name="password" required autocomplete="current-password" placeholder="Password">

                                <button class="eye-icon" type="button" id="togglePassword">
                                    <i class="fa-solid fa-eye-slash" style="color: #fff;"></i>                                
                                </button>
                            </div>
                            @error('password')
                                <span class="alert-danger P-2">{{ $message }}</span>
                            @enderror
  
                                {{-- <!-- Remember Me -->
                                <div class="mb-4">
                                  <label for="remember" class="inline-flex items-center">
                                      <input id="remember" type="checkbox" name="remember" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out">
                                      <span class="ml-2 text-sm text-white">{{ __('Remember me') }}</span>
                                </label>
                                </div>  --}}
                                 @if (Route::has('password.request'))
                                      <a class="forgot-password align-self-end pt-2" href="{{ route('password.request') }}">
                                          {{ __('Forgot your password?') }}
                                      </a>
                                @endif
                            <button type="submit" class="sign-button mt-5">Sign In</button>
                        </div>
                    </form>
                </div>
          </div>
    </div>
    <script>
          document.getElementById("togglePassword").addEventListener("click", function() {
                const passwordInput = document.getElementById("password");
                const passwordIcon = this.querySelector("i");
                              
                if (passwordInput.type === "password") {
                        passwordInput.type = "text";
                        passwordIcon.classList.remove("fa-eye-slash");
                        passwordIcon.classList.add("fa-eye");
                    } 
                else {
                      passwordInput.type = "password";
                      passwordIcon.classList.remove("fa-eye");
                      passwordIcon.classList.add("fa-eye-slash");
                    }
                });
   </script> 
@endsection