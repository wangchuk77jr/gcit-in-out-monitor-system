@extends('layouts.counsilorLayout')
@section('title-addition')
    Setting
@endsection

@section('Counsilorcss')
    <link rel="stylesheet" href="{{asset('css/profile-setting.css')}}">
@endsection
@section('dashboard-heading')
    Profile Setting
@endsection
@section('dashboard-content')
    @include('partials.Profile-setting')
@endsection