@extends('layouts.counsilorLayout')

@section('Counsilorcss')
    <link rel="stylesheet" href="{{ asset('css/sso/dashboard.css') }}">
@endsection

@section('title-addition')
    Dashboard
@endsection

@section('dashboard-heading')
    Dashboard
@endsection

@section('dashboard-content')
{{-- Studentleave and outofcampus--}}
<div class="col-md-12 Studentleave-and-outofcampus mt-3">
    <div class="leftside col-md-6">
        <h5 class="studentcountHeading p-3">Total number student out of campus : {{$dataCount['total']}}</h5>
        <div class="mt-3 " id="StudentOutofCampuschart"></div>
    </div>
    <div class="rightside col-md-6">
        <h5 class="studentcountHeading p-3">Total number student on Leave : {{$dataCount['totalAcceptedLeaves']}}</h5>
        <div class="mt-3" id="StudentLeavechart"></div>
    </div>
</div>
<script>
    
    var options = {
        series: [{{$dataCount['totalMale']}},{{$dataCount['totalFemale']}}], // male female
        chart: {
            type: 'donut',
            height:180,
            width: 400,
        },
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val.toFixed(0); // Display whole numbers
            }
        },
        legend: {
            position: 'right',
            offsetY: 0,
        },
        labels: ['Male out of campus', 'Female Out of campus'], // Add labels for the legend
        colors: [  '#F56E2C','#239F49'], 
    };

    var chart = new ApexCharts(document.querySelector("#StudentOutofCampuschart"), options);
    chart.render();

</script>
<script>
    var options = {
        series: [{{$dataCount['totalMaleLeaves']}}, {{$dataCount['totalFemaleLeaves']}}], // male female
        chart: {
            type: 'donut',
            height: 180,
            width: 400,
        },
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val.toFixed(0); // Display whole numbers
            }
        },
        legend: {
            position: 'right',
            offsetY: 0,
        },
        labels: ['Male on Leave', 'Female on Leave'], // Add labels for the legend
        colors: [  '#F56E2C','#239F49'], // Set colors for Male and Female sections
    };

    var chart = new ApexCharts(document.querySelector("#StudentLeavechart"), options);
    chart.render();
</script>

{{-- leave chart and outof campus --}}
<div class="row">
    <div class="col-md-6 mt-3" id="chartStudentOutofCampus">
    </div>
    <div class="col-md-6 mt-3" id="chartStudentLeave">
    </div>
</div>


{{-- student countsection --}}
<div class="col-md-12 mt-3">
    <div class="row">
        <div class="col-md-4">
            <div class="studentCountConatiner d-flex flex-column justify-content-center p-4">
                 <h2 class="countNumber">{{$totalstudent}}</h2>
                 <h6 class="countText">Total number of students</h6>
            </div>
        </div>
        <div class="col-md-4 mt-md-0 mt-3">
            <div class="studentCountConatiner d-flex flex-column justify-content-center p-4">
                 <h2 class="countNumber">{{$totalboynumber}}</h2>
                 <h6 class="countText">Total number of boys</h6>
            </div>
        </div>
        <div class="col-md-4 mt-md-0 mt-3">
            <div class="studentCountConatiner d-flex flex-column justify-content-center p-4">
                 <h2 class="countNumber">{{$totalgirlnumber}}</h2>
                 <h6 class="countText">Total number of girls</h6>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 mt-2">
    <div id="chartStudent">
    </div>
</div>



<script>
    var studentLeaveData = [
        @foreach ($studentOutofCampusbyBlock as $s)
            {
                x: 'Block {{ $s->hotelBlock }}',
                y: {{ $s->studentCount }},
            },
        @endforeach
    ];

    var options = {
        series: [
            {
                data: studentLeaveData
            }
        ],
        legend: {
            show: false
        },
        chart: {
            height: 170,
            type: 'treemap'
        },
        title: {
            text: 'Total number of students out-of-campus by block'
        },
        colors: ['#239F49', '#F56E2C', '#ab551f', '#1fc2bd', '#ab1f23', '#9747FF'],
        dataLabels: {
            enabled: true,
            style: {
                fontSize: '16px',
            },
            formatter: function (text, op) {
                return [text, op.value]
            },
            offsetY: -4
        },
        plotOptions: {
            treemap: {
                distributed: true,
                enableShades: false
            }
        },
    };

    var chart = new ApexCharts(document.querySelector("#chartStudentOutofCampus"), options);
    chart.render();
</script>


<script>
    var studentLeaveData = [
        @foreach ($studentLeavebyBlock as $leave)
            {
                x: 'Block {{ $leave->student_block }}',
                y: {{ $leave->total_leaves }},
            },
        @endforeach
    ];
    var options = {
          series: [
          {
            data: studentLeaveData
          }
        ],
          legend: {
          show: false
        },
        chart: {
          height: 170,
          type: 'treemap'
        },
        title: {
          text: 'Total number of students on leave by block'
        },
        colors: ['#239F49','#F56E2C','#ab551f','#1fc2bd','#ab1f23','#9747FF'],
   
        dataLabels: {
          enabled: true,
          style: {
            fontSize: '16px',
          },
          formatter: function(text, op) {
            return [text, op.value]
          },
          offsetY: -4
        },
        plotOptions: {
          treemap: {
            distributed: true,
            enableShades: false
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#chartStudentLeave"), options);
        chart.render();
</script>



<script>
    var options = {
      series: [{
        data: [{{$totalstudent}}, {{$totalboynumber}}, {{$totalgirlnumber}}],
        name:'',
      }],
      chart: {
        type: 'bar',
        height: 200
      },
      plotOptions: {
        bar: {
          borderRadius: 4,
          horizontal: true,
        }
      },
      dataLabels: {
        enabled: false
      },
      xaxis: {
        categories: ['Total Number Of Students', 'Total Number of Boys', 'Total Number of Girls'],
      },
      colors: ['#239F49']
    };
  
    var chart = new ApexCharts(document.querySelector("#chartStudent"), options);
    chart.render();
  </script>
  {{-- By block --}}
  <div id="studentByBlockChart"></div>
  <script>
    var options = {
        series: [
            @foreach($studentByBlock as $row)
                {
                    name: 'Block {{ $row->hotelBlock }}',
                    data: [{{ $row->total_students }}]
                },
            @endforeach
        ],
        chart: {
            type: 'bar',
            height: 200,
            stacked: true,
        },
        plotOptions: {
            bar: {
                horizontal: true,
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title: {
            text: 'Student Counts by Block'
        },
        xaxis: {
            categories: [''],
        },
        colors: ['#239F49','#F56E2C','#ab551f','#1fc2bd','#ab1f23','#9747FF'],
        tooltip: {
            y: {
                formatter: function (val) {
                    return val
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            position: 'top',
            horizontalAlign: 'left',
            offsetX: 40
        }
    };

    var chart = new ApexCharts(document.querySelector("#studentByBlockChart"), options);
    chart.render();
</script>
@endsection




