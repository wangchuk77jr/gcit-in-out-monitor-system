@extends('layouts.counsilorLayout')
@section('title-addition')
    Student Record
@endsection

@section('Counsilorcss')
    <link rel="stylesheet" href="{{asset('css/counsilor/StudentRecord.css')}}">
@endsection
@section('dashboard-heading')
    Student Record out-of-campus
@endsection
@section('dashboard-content')

<!-- Include ApexCharts library -->
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<!-- Chart container -->
<div class="mt-2" id="chart"></div>

<script>
    // Use Blade syntax to inject PHP variables into JavaScript
    var blockCounts = @json($blockCounts);
    

    // Format the data
    var formattedData = blockCounts.map(function (block) {
        return {
            name: 'Block ' + block.hotelBlock,
            data: [block.studentCount]
        };
    });
    console.log(formattedData);


    // ApexCharts configuration
    var options = {
        series: formattedData,
        chart: {
            type: 'bar',
            height: 200,
            stacked: true,
        },
        plotOptions: {
            bar: {
                horizontal: true,
            }
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title: {
            text: 'Number of students outside campus each block'
        },
        xaxis: {
            categories: [''],
        },
        colors: ['#239F49', '#F56E2C', '#ab551f', '#1fc2bd', '#ab1f23', '#9747FF'],
        tooltip: {
            y: {
                formatter: function (val) {
                    return val;
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            show: true, // Ensure legend is set to show
            position: 'top',
            horizontalAlign: 'left',
            offsetX: 40
        }
    };

    // Render the chart
    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
</script>


    
    <h5 style="font-family: Montserrat;font-size: 16px;font-weight: 700;">Students who are out of campus</h5>
    <div class="col-md-12 mt-3 mb-0 section-container d-flex flex-wrap">
        <button class="sortStudentButtonBlock buttonAll buttonActive px-4" data-block="All">All</button>
        @foreach ($distinctHotelBlocks as $b)
          <button data-block="{{ $b->hotelBlock }}" class="sortStudentButtonBlock px-4">Block {{ $b->hotelBlock }}</button>
        @endforeach
    </div>
    <div class="col-md-12">
        <div class=" col-md-12 section-container mt-3 table-responsive">
            <table id="studentCheckOutTable" class="table table-bordered custom-table">
                <thead>
                    <tr class="table-title py-3">
                        <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                        <th scope="col">ID</th>
                        <th scope="col">NAME</th>
                        <th scope="col">YEAR</th>
                        <th scope="col">COURSE</th>
                        <th scope="col">BLOCK</th>
                        <th scope="col" class="rounded-top-right">CHECK-OUT-DATE-TIME</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                      $slno = 1;  
                    @endphp
                 @foreach($studentCheckoutRecord as $log)
             
                 <tr>
                     <td class="ps-3">{{ $slno }}</td>
                     <td>{{ $log->studentID }}</td>
                     <td>{{ $log->studentName }}</td>
                     <td>{{ $log->studentYear }}</td>
                     <td>{{ $log->studentCourse }}</td>
                     <td>{{ $log->hotelBlock }}</td>
                     <td>{{ $log->checkOutDateTime }}</td>
                 </tr>
             
                 @php
                     $slno++;
                 @endphp
             @endforeach
             <tr id="noDataMatchedMessage" class=" alert-danger text-center" style="display: none;">
                <td colspan="9">Oops, sorry! No data for the selected block</td>
            </tr>
             
        
                </tbody>
            </table>
            <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
            <script>
              $(document).ready(function () {
                $(".sortStudentButtonBlock").on("click", function () {
                  var selectedBlock = $(this).data("block");
            
                  // Update button states
                  $(".sortStudentButtonBlock").removeClass("buttonActive");
                  $(this).addClass("buttonActive");
            
                  // Show/hide rows based on the selected block
                  if (selectedBlock === "All") {
                    $("#studentCheckOutTable tbody tr").show();
                    $("#noDataMatchedMessage").hide();
                  } else {
                    var matchingRows = $("#studentCheckOutTable tbody tr").filter(function () {
                      return $(this).find("td:eq(5)").text() === selectedBlock;
                    });
            
                    if (matchingRows.length > 0) {
                      $("#studentCheckOutTable tbody tr").hide();
                      matchingRows.show();
                      $("#noDataMatchedMessage").hide();
                    } else {
                      // Display a message if no data matched
                      $("#studentCheckOutTable tbody tr").hide();
                      $("#noDataMatchedMessage").show();
                    }
                  }
                });
              });
            </script>
            
        </div>
    </div>
@endsection