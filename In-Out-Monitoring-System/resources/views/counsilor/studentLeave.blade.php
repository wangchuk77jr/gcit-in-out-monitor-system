@extends('layouts.counsilorLayout')
@section('title-addition')
    Student Leave
@endsection

@section('Counsilorcss')
    <link rel="stylesheet" href="{{asset('css/counsilor/StudentLeave.css')}}">
@endsection
@section('dashboard-heading')
    Student Leave
@endsection
@section('dashboard-content')

<div class="mt-2" id="studentByBlockChart"></div>
<script>
    var options = {
        series: [
            @foreach($studentLeaveCountByBlock as $row)
                {
                    name: 'Block {{ $row->student_block }}',
                    data: [{{ $row->total_leaves }}]
                },
            @endforeach
        ],
        chart: {
            type: 'bar',
            height: 200,
            stacked: true,
        },
        plotOptions: {
            bar: {
                horizontal: true,
            }
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title: {
            text: 'Student Leave Counts by Block'
        },
        xaxis: {
            categories: [''],
        },
        colors: ['#239F49','#F56E2C','#ab551f','#1fc2bd','#ab1f23','#9747FF'],
        tooltip: {
            y: {
                formatter: function (val) {
                    return val
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            position: 'top',
            horizontalAlign: 'left',
            offsetX: 40
        }
    };

    var chart = new ApexCharts(document.querySelector("#studentByBlockChart"), options);
    chart.render();
</script>

<div class="col-md-12 mt-0 section-container d-flex flex-wrap">
    <button class="sortStudentButtonBlock buttonAll buttonActive px-4" data-block="All">All</button>
    @foreach ($distinctHotelBlocks as $b)
      <button data-block="{{ $b->hotelBlock }}" class="sortStudentButtonBlock px-4">Block {{ $b->hotelBlock }}</button>
    @endforeach
    
</div>

{{-- student leave record --}}
<div class="col-md-12">
    <div id="studentTableContainer" class="col-md-12 section-container table-responsive mt-3">
        <table id="studentLeaveTable" class="table  table-bordered custom-table">
            <thead>
                <tr  class="table-title py-3 student-row">
                    <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                    <th scope="col">SID</th>
                    <th scope="col">NAME</th>
                    <th scope="col">PHONE NUMBER</th>
                    <th scope="col">COURSE</th>
                    <th scope="col">YEAR</th>
                    <th scope="col">BLOCK</th>
                    <th scope="col">START DATE</th>
                    <th scope="col">END DATE</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($studentLeaveRecord as $student)
                <tr>
                    <td class="ps-3">{{ $loop->index + 1 }}</td>
                    <td>{{ $student->student->studentID }}</td>
                    <td>{{ $student->student->studentName }}</td>
                    <td>{{ $student->student->phoneNumber }}</td>
                    <td>{{ $student->student->studentCourse }}</td>
                    <td>{{ $student->student->studentYear }}</td>
                    <td>{{ $student->student->hotelBlock }}</td>
                    <td>{{ $student->StartDate}}</td>
                    <td>{{$student->EndDate}}</td>
                </tr>
           
                @endforeach
                <tr id="noDataMatchedMessage" class=" alert-danger text-center" style="display: none;">
                    <td colspan="9">Oops, sorry! No data for the selected block</td>
                </tr>
            </tbody>
        </table>
   

        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script>
          $(document).ready(function () {
            $(".sortStudentButtonBlock").on("click", function () {
              var selectedBlock = $(this).data("block");
        
              // Update button states
              $(".sortStudentButtonBlock").removeClass("buttonActive");
              $(this).addClass("buttonActive");
        
              // Show/hide rows based on the selected block
              if (selectedBlock === "All") {
                $("#studentLeaveTable tbody tr").show();
                $("#noDataMatchedMessage").hide();
              } else {
                var matchingRows = $("#studentLeaveTable tbody tr").filter(function () {
                  return $(this).find("td:eq(6)").text() === selectedBlock;
                });
        
                if (matchingRows.length > 0) {
                  $("#studentLeaveTable tbody tr").hide();
                  matchingRows.show();
                  $("#noDataMatchedMessage").hide();
                } else {
                  // Display a message if no data matched
                  $("#studentLeaveTable tbody tr").hide();
                  $("#noDataMatchedMessage").show();
                }
              }
            });
          });
        </script>
        

    </div>
</div>
@endsection