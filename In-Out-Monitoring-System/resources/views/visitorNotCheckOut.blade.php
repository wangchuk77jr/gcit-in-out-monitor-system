<!DOCTYPE html>
<html>
<head>
    <title>Visitor not checkout</title>

</head>
<body>
        <ol>
            <h2>Visitors who have not checked out</h2>
            @foreach ($visitorInTheCampus as $v)
            <li style="padding-top:10px">
                Visitor CID: {{ $v->visitorCID }}({{$v->visitor_type}}) Phone Number: {{$v->v_phoneNumber}} 
                <h4 class="vistornamesHeading">Visitors Name:</h4>
                <ol>
                    @foreach (json_decode($v->visitor_names) as $visitorName)
                        <li class="visitorsName">{{ $visitorName }}</li>
                    @endforeach
                </ol>
            @endforeach
        </ol>
            <br> <br>
            <a  style="text-decoration: none;background: orange;color: #fff;padding:20px;" href="http://127.0.0.1:8000/HR/visitors-record">View In Detail</a>
          
</body>
</html>
