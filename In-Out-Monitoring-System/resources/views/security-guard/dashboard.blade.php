@extends('layouts.securityLayout')

@section('securityguardcss')
    <link rel="stylesheet" href="{{ asset('css/securityguard-css/dashboard.css') }}">

@endsection

@section('title-addition')
    Dashboard
@endsection

@section('dashboard-heading')
    Dashboard
@endsection


@section('dashboard-content')

{{-- Slider Section --}}
    <div class="col-md-12 slider mt-3">
        <div class="slider-container">
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img src="{{ asset('images/security-guard/slider01.png')}}" class="d-block w-100 carousel-images" alt="...">
                    <div class="slider-caption">
                        <h5 class="heading-text-slider text-uppercase">TOTAL NUMBER OF STUDENTS OUTSIDE THE CAMPUS</h5>
                        <h1 class="number-text">{{$totalNumberOfUnchecked}}</h1>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <img src="{{ asset('images/security-guard/slider02.png')}}" class="d-block w-100 carousel-images" alt="...">
                    <div class="slider-caption">
                        <h5 class="heading-text-slider text-uppercase">TOTAL NUMBER OF VISITOR IN THE CAMPUS</h5>
                        <h1 class="number-text">{{$totalNumberOfCheckOutVisitor}}</h1>
                    </div>
                    </div>
                </div>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <svg xmlns="http://www.w3.org/2000/svg" width="37" height="24" viewBox="0 0 37 24" fill="none">
                        <path d="M36.0607 13.0607C36.6464 12.4749 36.6464 11.5251 36.0607 10.9393L26.5147 1.3934C25.9289 0.807611 24.9792 0.807611 24.3934 1.3934C23.8076 1.97919 23.8076 2.92893 24.3934 3.51472L32.8787 12L24.3934 20.4853C23.8076 21.0711 23.8076 22.0208 24.3934 22.6066C24.9792 23.1924 25.9289 23.1924 26.5147 22.6066L36.0607 13.0607ZM0 13.5H35V10.5H0V13.5Z" fill="#239F49"/>
                        </svg>
                </button>
                </div> 
        </div>
    </div>


{{-- Student entry & exit button --}}
<div class="col-12 mt-5 dashboard-button-container">
    <div class="row">
        <div class="col-md-6">
            @include('partials.studentCheckOutButton')
        </div>
        <div class="col-md-6">
            @include('partials.stundenCheckInButton')
        </div>
    </div>
</div>

{{-- visitor entry & exit button --}}
<div class="col-12 mt-3 dashboard-button-container mb-1">
    <div class="row">
        <div class="col-md-6">
            <button type="button" class="visitor-button-check-out-in d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#VisitorCheckOut">
                <p class="me-auto visitor-button-check-out-in-text text-uppercase pt-3">Visitor Check-Out</p>
                <div>
                    <svg class="svg" xmlns="http://www.w3.org/2000/svg" width="40" height="20" viewBox="0 0 46 29" fill="none">
                        <rect width="46" height="29" fill="#239F49" stroke="white"/>
                        <path d="M23.7071 7.29273C23.3166 6.90221 22.6834 6.90221 22.2929 7.29273L15.9289 13.6567C15.5384 14.0472 15.5384 14.6804 15.9289 15.0709C16.3195 15.4614 16.9526 15.4614 17.3431 15.0709L23 9.41405L28.6569 15.0709C29.0474 15.4614 29.6805 15.4614 30.0711 15.0709C30.4616 14.6804 30.4616 14.0472 30.0711 13.6567L23.7071 7.29273ZM24 24.167L24 7.99984H22L22 24.167H24Z" fill="white"/>
                      </svg>
                </div>
            </button>
        </div>
        <!-- Visitor Check-Out Modal -->
        <div class="modal fade" id="VisitorCheckOut" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="VisitorCheckOutLabel" aria-hidden="true">
            <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content py-3 px-4 visitorModalContent">
                <div class="modal-header border-0">
                <h1 class="modal-title fs-5 visitor-check-modal-heading" id="VisitorCheckOutLabel">Visitor Check-Out</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <form action="{{url('securityGuard/visitorCheckOutLog')}}" method="post">
                    @csrf
                        <input name="visitorPassID" required class="w-100 visitorModalInput ps-3 mt-1" type="number" placeholder="Enter Visitor Pass ID">
                        <input hidden type="text" name="visitorCheckOutDateTime" id="visitorCheckOutDateTime">
                        <script>
                            function updateDateTime() {
                                const now = new Date();
                                const year = now.getFullYear();
                                const month = String(now.getMonth() + 1).padStart(2, '0');
                                const day = String(now.getDate()).padStart(2, '0');
                                const hours = now.getHours();
                                const minutes = String(now.getMinutes()).padStart(2, '0');
                                const ampm = hours >= 12 ? 'pm' : 'am';
            
                                let formattedHours = hours % 12;
                                formattedHours = formattedHours ? formattedHours : 12;
            
                                const currentDate = `${year}-${month}-${day}`;
                                const currentTime = `${formattedHours}:${minutes} ${ampm}`;
                                const currentDateTime = `${currentDate} ${currentTime}`;
            
                                // Set the value of the hidden input field
                                document.getElementById('visitorCheckOutDateTime').value = currentDateTime;
                            }
            
                            // Update the time on page load
                            updateDateTime();
            
                            // Update the time every second (1000 milliseconds)
                            setInterval(updateDateTime, 1000);
                        </script>
                        <button type="submit" class="w-100 mt-4 visitorCheckmodalButton mb-3">CHECK-OUT</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        <div class="col-md-6">
            <button type="button" class="visitor-button-check-out-in  d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#VisitorCheckIn">
                <p class="me-auto visitor-button-check-out-in-text pt-3 text-uppercase">Visitor Check-in</p>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="20" viewBox="0 0 46 29" fill="none">
                        <rect x="0.5" y="0.5" width="45" height="28" fill="#239F49" stroke="white"/>
                        <path d="M21.5429 22.8391C21.9334 23.2296 22.5666 23.2296 22.9571 22.8391L29.3211 16.4751C29.7116 16.0846 29.7116 15.4515 29.3211 15.0609C28.9305 14.6704 28.2974 14.6704 27.9069 15.0609L22.25 20.7178L16.5931 15.0609C16.2026 14.6704 15.5695 14.6704 15.1789 15.0609C14.7884 15.4515 14.7884 16.0846 15.1789 16.4751L21.5429 22.8391ZM21.25 5.96484L21.25 22.132H23.25L23.25 5.96484H21.25Z" fill="white"/>
                    </svg>
                </div>
            </button>
        </div>
    </div>
</div>

<!-- Visitor Check-in Modal -->
  <div class="modal fade" id="VisitorCheckIn" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="VisitorCheckInLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content py-3 px-4 visitorModalContent">
        <div class="modal-header border-0">
          <h1 class="modal-title fs-5 visitor-check-modal-heading" id="VisitorCheckInLabel">Visitor Check-In</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>Select Visitor Type *</p>
          <div class="w-100 d-flex gap-4 mb-4">
            <button class="VisitorType" data-type="long">Long Term Visitor</button>
            <button class="VisitorType" data-type="short">Short Term Visitor</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!--  Modals for Long term visitor type -->
  <div class="modal fade" id="LongTermVisitorModal" tabindex="-1" aria-labelledby="LongTermVisitorModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content py-3 px-4 visitorModalContent">
          <div class="modal-header border-0">
            <h1 class="modal-title fs-5 visitor-check-modal-heading" id="VisitorCheckInLabel">Visitor Check-In</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <p class="w-100" style="color: #239F49;font-size:12px;font-weight:900;">** Visitor Type: Long Term **</p>
             <form action="{{url('securityGuard/visitorCheckIn')}}" method="post">
                @csrf
                  <input name="visitorCID" required class="w-100 visitorModalInput ps-3 mt-1" type="number" placeholder="Visitor CID Number">
                  <input name="visitorPassID" required class="w-100 visitorModalInput ps-3 mt-2" type="number" maxlength="8" minlength="8" placeholder="Visitor Pass ID">
                  <input id="longTermcheckInDateTime" hidden type="text" name="checkInDateTime">
                  <script>
                    function updateDateTime() {
                        const now = new Date();
                        const year = now.getFullYear();
                        const month = String(now.getMonth() + 1).padStart(2, '0');
                        const day = String(now.getDate()).padStart(2, '0');
                        const hours = now.getHours();
                        const minutes = String(now.getMinutes()).padStart(2, '0');
                        const ampm = hours >= 12 ? 'pm' : 'am';
    
                        let formattedHours = hours % 12;
                        formattedHours = formattedHours ? formattedHours : 12;
    
                        const currentDate = `${year}-${month}-${day}`;
                        const currentTime = `${formattedHours}:${minutes} ${ampm}`;
                        const currentDateTime = `${currentDate} ${currentTime}`;
    
                        // Set the value of the hidden input field
                        document.getElementById('longTermcheckInDateTime').value = currentDateTime;
                    }
    
                    // Update the time on page load
                    updateDateTime();
    
                    // Update the time every second (1000 milliseconds)
                    setInterval(updateDateTime, 1000);
                </script>
                  <input hidden type="text" name="visitorType" value="longTerm">
                  <button type="submit" class="w-100 mt-4 visitorCheckmodalButton mb-3">CHECK-IN</button>
             </form>
          </div>
        </div>
      </div>
  </div>
  
  <!--  Modals for short term visitor type -->
  <div class="modal fade" id="ShortTermVisitorModal" tabindex="-1" aria-labelledby="ShortTermVisitorModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content py-3 px-4 visitorModalContent">
          <div class="modal-header border-0">
            <h1 class="modal-title fs-5 visitor-check-modal-heading" id="VisitorCheckInLabel">Visitor Check-In</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <p class="w-100" style="color: #239F49;font-size:12px;font-weight:900;">** Visitor Type: Short Term **</p>
             <form id="vshortTermVisitorCheckInForm" action="{{url('securityGuard/visitorCheckIn')}}" method="post">
                @csrf 
                  <input required class="w-100 visitorModalInput ps-3 mt-1" type="number" name="visitorCID" placeholder="Visitor CID Number">
                  <input required class="w-100 visitorModalInput ps-3 mt-3" type="number" name="phoneNumber" maxlength="8" minlength="8" placeholder="Phone Number">
                  <div>
                      <input required class="w-100 visitorModalInput ps-3 mt-3" type="number" placeholder="Number Of Visitor" name="numberOfVisitor" id="numOfVisitors">
                      <span class="error-message" id="numOfVisitorsError"></span>
                  </div>
                  <div id="visitorNamesContainer"></div>
                  <input required class="w-100 visitorModalInput ps-3 mt-3" type="text" name="dependentName" placeholder="Dependent Name">
                  <textarea required  rows="5" class="visitorModalInputTextArea w-100  p-3 mt-3" placeholder="Purpose of Visit" name="reason" ></textarea>
                  <input name="visitorPassID" required class="w-100 visitorModalInput ps-3 mt-2" type="number" maxlength="8" minlength="8" placeholder="Visitor Pass ID">
                  <!-- Set the default time zone to Bhutan -->
                  <?php date_default_timezone_set('Asia/Thimphu'); ?>
                  <!-- Hidden input field for dynamic current date time in Bhutan time -->
                  <input required hidden type="text" name="checkInDateTime" id="shortTermcheckInDateTime" readonly>

    
                  <input hidden type="text" name="visitorType" value="shortTerm">
                  <div class="VisitorErrorMessage mt-3 alert-danger" style="display:none;">
                    <ul class="pt-2">
                        <li>error message</li>
                    </ul>
                 </div>
                  <button type="submit" class="w-100 mt-3 visitorCheckmodalButton mb-3">CHECK-IN</button>
             </form>

            <!-- JavaScript to update the time dynamically -->
            <script>
                function updateDateTime() {
                    const now = new Date();
                    const year = now.getFullYear();
                    const month = String(now.getMonth() + 1).padStart(2, '0');
                    const day = String(now.getDate()).padStart(2, '0');
                    const hours = now.getHours();
                    const minutes = String(now.getMinutes()).padStart(2, '0');
                    const ampm = hours >= 12 ? 'pm' : 'am';

                    let formattedHours = hours % 12;
                    formattedHours = formattedHours ? formattedHours : 12;

                    const currentDate = `${year}-${month}-${day}`;
                    const currentTime = `${formattedHours}:${minutes} ${ampm}`;
                    const currentDateTime = `${currentDate} ${currentTime}`;

                    // Set the value of the hidden input field
                    document.getElementById('shortTermcheckInDateTime').value = currentDateTime;
                }

                // Update the time on page load
                updateDateTime();

                // Update the time every second (1000 milliseconds)
                setInterval(updateDateTime, 1000);
            </script>
             <script>
                document.addEventListener("DOMContentLoaded", function () {
                    // Get the form element
                    var form = document.querySelector("#vshortTermVisitorCheckInForm");
            
                    // Add a submit event listener to the form
                    form.addEventListener("submit", function (event) {
                        // Prevent the default form submission
                        event.preventDefault();
            
                        // Reset previous error messages and remove error-input class from all input fields
                        document.querySelector(".VisitorErrorMessage").style.display = "none";
                        document.querySelectorAll(".error-input").forEach(function (input) {
                            input.classList.remove("error-input");
                        });
            
                        // Get form values
                        var visitorCID = form.querySelector("[name='visitorCID']");
                        var v_phoneNumber = form.querySelector("[name='phoneNumber']");
                        var staffName = form.querySelector("[name='dependentName']");
                        var reason = form.querySelector("[name='reason']");
                        var visitorNames = form.querySelector(".visitorNames");

                        
                        
                        // Convert numeric values to strings for validation
                        var visitorCIDValue = visitorCID.value.toString();
                        var vPhoneNumberValue = v_phoneNumber.value.toString();
            
                        // Array to store error messages
                        var errorMessages = [];
            
                        // Validate staff name
                        var staffNameRegex = /^[a-zA-Z\s]{3,20}$/;
                        if (!staffNameRegex.test(staffName.value) || !staffNameRegex.test(visitorNames.value)) {
                            errorMessages.push("Dependent Name or visitor name must contain only alphabets and have a length between 3 to 20 characters.");
                            staffName.classList.add("error-input");
                            visitorNames.classList.add("error-input");
                        }
            
                        // Validate reason
                        var reasonRegex = /^[a-zA-Z\s]+$/;
                        if (!reasonRegex.test(reason.value)) {
                            errorMessages.push("Reason can only contain alphabets.");
                            reason.classList.add("error-input");
                        }
            
                        var wordCount = reason.value.split(/\s+/).filter(function (word) {
                            return word.length > 0;
                        }).length;
            
                        if (wordCount < 3 || wordCount > 100) {
                            errorMessages.push("Reason must be between 3 and 100 words.");
                            reason.classList.add("error-input");
                        }
            
                        // Validate Visitor CID
                        var visitorCIDRegex = /^\d{11}$/;
                        if (!visitorCIDRegex.test(visitorCIDValue)) {
                            errorMessages.push("Visitor CID must be exactly 11 digits.");
                            visitorCID.classList.add("error-input");
                        }
            
                        // Validate Visitor Phone Number
                        var phoneNumberRegex = /^(77|17)\d{6}$/;
                        if (!phoneNumberRegex.test(vPhoneNumberValue)) {
                            errorMessages.push("Visitor phone number must be a valid Bhutanese number and be exactly 8 digits.");
                            v_phoneNumber.classList.add("error-input");
                        }
        
            
                        // Display all error messages at once
                        if (errorMessages.length > 0) {
                            var errorMessageList = errorMessages.map(function (message) {
                                return "<li>" + message + "</li>";
                            }).join("");
                            document.querySelector(".VisitorErrorMessage ul").innerHTML = errorMessageList;
                            document.querySelector(".VisitorErrorMessage").style.display = "block";
                        } else {
                            // If all validations pass, submit the form
                            form.submit();
                        }
                    });
                });
            </script>
          </div>
        </div>
      </div>
  </div>
  
<!-- Include jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
{{-- modal show based on visitor type --}}
  <script>
    $(document).ready(function() {
        // Add click event listener to buttons with class 'VisitorType'
        $('.VisitorType').on('click', function() {
            // Get the 'data-type' attribute value
            var visitorType = $(this).data('type');

            // Close any open modals
            $('.modal').modal('hide');

            // Show the corresponding modal based on visitorType
            if (visitorType === 'long') {
            $('#LongTermVisitorModal').modal('show');
            } else if (visitorType === 'short') {
            $('#ShortTermVisitorModal').modal('show');
            }
        });
    });
    // Function to dynamically add visitor name input fields and handle errors
    function handleNumOfVisitorsInput() {
        const numOfVisitorsInput = document.getElementById("numOfVisitors");
        const numOfVisitors = parseInt(numOfVisitorsInput.value);
        const numOfVisitorsError = document.getElementById("numOfVisitorsError");

        // Clear any existing input fields and error message
        const visitorNamesContainer = document.getElementById("visitorNamesContainer");
        visitorNamesContainer.innerHTML = "";
        numOfVisitorsError.textContent = "";

        // Check for valid input (not negative and not a decimal)
        if (isNaN(numOfVisitors) || numOfVisitors < 1 || numOfVisitors % 1 !== 0) {
            numOfVisitorsInput.classList.add("error-border");
            numOfVisitorsError.textContent = "Please enter a valid positive integer.";
        } else {
            numOfVisitorsInput.classList.remove("error-border");

            // Add input fields for visitor names based on the number of visitors
            for (let i = 1; i <= numOfVisitors; i++) {
                const input = document.createElement("input");
                input.required = true;
                input.classList.add("w-100", "visitorModalInput", "ps-3", "mt-3","visitorNames");
                input.type = "text";
                input.placeholder = "Visitor Name " + i;
                input.name = "vistor_Name[]";
                visitorNamesContainer.appendChild(input);
            }
        }
    }

    // Add event listeners to handle changes in the "Number Of Visitor" input field
    const numOfVisitorsInput = document.getElementById("numOfVisitors");
    numOfVisitorsInput.addEventListener("input", handleNumOfVisitorsInput);
</script>


@endsection
