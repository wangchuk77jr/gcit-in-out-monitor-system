@extends('layouts.securityLayout')
@section('title-addition')
    Setting
@endsection

@section('securityguardcss')
    <link rel="stylesheet" href="{{asset('css/profile-setting.css')}}">
@endsection
@section('dashboard-heading')
    Profile Setting
@endsection
@section('dashboard-content')
    @include('partials.Profile-setting')
@endsection
