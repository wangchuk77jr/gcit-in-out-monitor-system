@extends('layouts.securityLayout')
@section('title-addition')
    Student record
@endsection

@section('securityguardcss')
    <link rel="stylesheet" href="{{ asset('css/securityguard-css/student-record.css') }}">
@endsection

@section('dashboard-heading')
    Student Record
@endsection

@section('dashboard-content')
<div class="col-md-12 mt-3" id="chartStudentRecord"></div>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
  var options = {
    series: [
        {
            name:'Total students checked out today',
            data:[{{ $totalNumberOfCheckOut  }}],
        },
        {
            name:'Total students checked in today.',
            data:[{{ $totalNumberOfCheckedIn }}],
        },
        {
            name:'Total student out of campus',
            data:[{{ $totalNumberOfUnchecked }}],
        }
 
    ],
    chart: {
      type: 'bar',
      height: 200,
      stacked: true,
    },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          total: {
            enabled: true,
            offsetX: 0,
            style: {
              fontSize: '11px',
              fontWeight: 900
            }
          }
        }
      },
    },
    stroke: {
      width: 1,
      colors: ['#fff']
    },
    colors: ['#239F49','#F56E2C','#ab1f23',],
    xaxis: {
      categories: ['Student Record'],
      labels: {
        formatter: function (val) {
          return val;
        }
      }
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return val;
        }
      }
    },
    fill: {
      opacity: 1
    },
    legend: {
      position: 'top',
      horizontalAlign: 'left',
      offsetX: 40
    }
  };

  var chart = new ApexCharts(document.querySelector("#chartStudentRecord"), options);
  chart.render();
</script>
{{-- Student entry & exit button --}}
<div class="col-12 dashboard-button-container">
    <div class="row">
        <div class="col-md-6">
            @include('partials.studentCheckOutButton')
        </div>
        <div class="col-md-6 mt-md-0 mt-3">
            @include('partials.stundenCheckInButton')
        </div>
    </div>
</div>

<div class=" col-md-12 section-container mt-4 table-responsive">
    <table class="table table-bordered custom-table">
        <thead>
            <tr  class="table-title py-3">
                <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                <th scope="col">STUDENT ID</th>
                <th scope="col">STUDENT NAME</th>
                <th scope="col">CHECK-OUT-DATE-TIME</th>
                <th scope="col">CHECK-IN-DATE-TIME</th>
                <th scope="col" class="rounded-top-right text-center">ACTION</th>
            </tr>
        </thead>
        <tbody>
            @php
              $slno = 1;  
              $currentDate = now()->format('Y-m-d');
            @endphp
            @foreach($entryExitLogs as $log)
            <tr style="background: {{ (
                ($log->checkInDateTime === null || $log->checkOutDateTime === null) &&
                (
                    (
                        ($log->checkInDateTime instanceof DateTime && strtotime($log->checkInDateTime->format('Y-m-d')) < strtotime($currentDate)) ||
                        (is_string($log->checkInDateTime) && strtotime(date('Y-m-d', strtotime($log->checkInDateTime))) < strtotime($currentDate))
                    )
                ) ||
                (
                    (
                        ($log->checkOutDateTime instanceof DateTime && strtotime($log->checkOutDateTime->format('Y-m-d')) < strtotime($currentDate)) ||
                        (is_string($log->checkOutDateTime) && strtotime(date('Y-m-d', strtotime($log->checkOutDateTime))) < strtotime($currentDate))
                    )
                )
            ) ? '#FDFFD0' : '#fff' }}">
                <td class="ps-3">{{$slno}}</td>
                <td>{{ $log->student->studentID }}</td>
                <td>{{ $log->student->studentName }}</td>
                <td>

                   {{$log->checkOutDateTime}}
            

                </td>
                <td>
                    @if($log->checkInDateTime)
                    {{ $log->checkInDateTime }}
                    @else
                        pending...
                    @endif
                </td>
                
                <td class="actionbutton d-flex gap-3 text-center justify-content-center align-items-center">
                    @if($log->checkInDateTime)
                     <span class="ps-3" style="color: green;font-weight:800;">completed.</span>   
                    @else
                        <button class="checkIn" data-bs-toggle="modal" data-bs-target="#StudentcheckInModal{{ $log->student->studentID }}">
                            Check-In
                        </button>
                    @endif
                </td>                
            </tr>
         
   
            <div class="modal fade" id="StudentcheckInModal{{ $log->student->studentID }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                        <div class="modal-header border-0">
                            <h5 class="modal-title scanHeading">Confirm Student Check-In</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p class="scan-sub-heading">Enter Student Number To Confirm ** <span style="color: red">{{ $log->student->studentID }}</span></p>
                            <form action="{{ url('securityGuard/updateCheckInTimeDate') }}" method="post">
                                @csrf
                                <input id="studentID{{ $log->student->studentID }}" name="studentID" required class="w-100 StudentModalInput ps-3 mt-1" type="number" placeholder="Student ID">
                                <button style="height: 42px;" id="confirmbutton{{$log->student->studentID}}"  disabled type="submit" class="w-100 my-4 text-uppercase">Confirm</button>
                                <script>
                                        document.getElementById('studentID{{ $log->student->studentID }}').addEventListener('input', function() {
                                            var enteredPassID = this.value;
                                            var actualPassID = {{ $log->student->studentID}};
                                            var confirmButton = document.getElementById('confirmbutton{{$log->student->studentID}}');
                                            
                                            if (enteredPassID == actualPassID) {
                                                confirmButton.disabled = false;
                                                confirmButton.classList.remove('checkinout-confirm-button-inactive');
                                                confirmButton.classList.add('checkinout-confirm-button-active');
                                            } else {
                                                confirmButton.disabled = true;
                                                confirmButton.classList.remove('checkinout-confirm-button-active');
                                                confirmButton.classList.add('checkinout-confirm-button-inactive ');
                                            }
                                        });
                                </script>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                @php
                $slno++;
                @endphp
            @endforeach
        </tbody>
    </table>
    
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

@endsection
