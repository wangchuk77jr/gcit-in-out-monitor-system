@extends('layouts.generalbase')

@section('title')
    GCIT In & OUT Monitor System
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/home.css')}}">  
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
@endsection
@section('content')
    <section class="homecontainer">
        <div class="container">
            <div class="row pt-3">
                <div class="col-6">
                    <a class="img-fluid" href="#"><img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="logo"></a>
                </div>                
                    @if (Route::has('login'))
                    <div id="dashboard" class="col-6 ms-auto d-flex justify-content-end align-items-center ">
                        @auth
                            @if (Auth::user()->role === 'admin')
                                    <!-- Show the Admin Dashboard link for admin users -->
                                    <a href="{{ route('Admindashboard') }}">Admin Dashboard</a>
                                @elseif (Auth::user()->role === 'sso')
                                    <!-- Show the SSO Dashboard link for SSO users -->
                                    <a href="{{ route('SSOdashboard') }}">SSO Dashboard</a>
                                @elseif (Auth::user()->role === 'security-guard')
                            
                                    <a href="{{ route('securityGuardDashboard') }}">Security Guard Dashboard</a>
                                @elseif (Auth::user()->role === 'councilor')

                                    <a href="{{ route('Counsilordashboard') }}">Counsilor Dashboard</a>
                                @elseif (Auth::user()->role === 'HR')
                                 
                                    <a href="{{ route('HRdashboard') }}">HR Dashboard</a>
                                @elseif (Auth::user()->role === 'HR_Head')
                                  
                                    <a href="{{ route('HRHeaddashboard') }}">HR Head Dashboard</a>
                                @endif                    
                            @else
                            <a href="{{ route('login') }}" class="login-button text-decoration-none text-center d-flex justify-content-center align-items-center">Log in</a>
                        @endauth
                    </div>
                @endif
            </div>
            <div class="row mt-3">
                <div class="col-12 home-content d-flex flex-column justify-content-center align-items-center">
                    <h1 class="home-heading mt-3">
                        EFFORTLESS ENTRY AND EXIT TRACKING: INTRODUCING GCIT IN & OUT MONITORING SYSTEM
                        FOR ENHANCED SECURITY
                    </h1>
                    <div class="mt-3">
                        <button class="visitor-button mt-5" data-toggle="modal" data-target="#Visitorentryform">VISITOR ENTRY</button>
                        {{-- visitor modal form  --}}
                        <div class="modal fade" id="Visitorentryform" tabindex="-1" role="dialog" aria-labelledby="VisitorentryformLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content  p-3" style="border-radius: 18px">
                                    <div class="modal-header">
                                    <h5 class="modal-title text-center" id="VisitorentryformLabel">VISITOR ENTRY FORM</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 26 25" fill="none">
                                                <rect x="1" y="21.9204" width="31" height="4" transform="rotate(-45 1 21.9204)" fill="rgba(0, 0, 0, 0.72)"/>
                                                <rect x="2.82861" width="31" height="4" transform="rotate(45 2.82861 0)" fill="rgba(0, 0, 0, 0.72)"/>
                                              </svg>
                                        </span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        {{-- Visitor Preregistration --}}
                                        <form id="visitorform" action="{{ route('home') }}" method="POST"  class="d-flex flex-column gap-3">
                                             @csrf
                                            <input type="hidden" name="Visitor_type" value="Long Term">
                                            <input type="email" required name="staffEmail" class="student-leave-input px-3" placeholder="Staff email address">
                                            <input type="text" required name="dependentName" class="student-leave-input px-3" placeholder="Staff name">
                                            <input type="number" name="employeId" class="student-leave-input px-3 " required placeholder="Employee ID">
                                            <input type="number" name="visitorCID" class="student-leave-input px-3 " required placeholder="Visitor CID number">
                                            <input type="number" name="v_phoneNumber" class="student-leave-input px-3 " required placeholder="+975 Visitor phone number">
                                            <div>
                                                <input name="no_visitor" class="w-100 visitorModalInput student-leave-input px-3" type="number" placeholder="Number Of Visitor" id="numOfVisitors">
                                                <span class="error-message" id="numOfVisitorsError"></span>
                                            </div>
                                            <div id="visitorNamesContainer"></div>
                                            <div class="input-group">
                                                <label for="start-date" class="input-group-text">Pick the entry date:</label>
                                                <input name="start_date" type="date" id="start-date" class="form-control student-leave-input date" required>
                                            </div>
                                            <div class="input-group">
                                                <label for="start-date" class="input-group-text">Pick your end date:</label>
                                                <input name="end_date" type="date" id="start-date" class="form-control student-leave-input date" required>
                                            </div>
                                            <textarea name="reason" class="reason-input p-3" name="reason-of-leave" id="" cols="30" rows="3" required placeholder="Reason for entering the campus"></textarea>
                                            <div class="VisitorErrorMessage mt-3 alert-danger" style="display:none;">
                                                <ul class="pt-2">
                                                    <li>error message</li>
                                                </ul>
                                             </div>
                                            <button type="submit" class="btn student-leave-button-submit"><span> Submit</span></button>
                                        </form>
                                        <script>
                                            document.addEventListener("DOMContentLoaded", function () {
                                                // Get the form element
                                                var form = document.querySelector("#visitorform");
                                        
                                                // Add a submit event listener to the form
                                                form.addEventListener("submit", function (event) {
                                                    // Prevent the default form submission
                                                    event.preventDefault();
                                        
                                                    // Reset previous error messages and remove error-input class from all input fields
                                                    document.querySelector(".VisitorErrorMessage").style.display = "none";
                                                    document.querySelectorAll(".error-input").forEach(function (input) {
                                                        input.classList.remove("error-input");
                                                    });
                                        
                                                    // Get form values
                                                    var staffEmail = form.querySelector("[name='staffEmail']");
                                                    var staffName = form.querySelector("[name='dependentName']");
                                                    var startDate = form.querySelector("[name='start_date']");
                                                    var endDate = form.querySelector("[name='end_date']");
                                                    var reason = form.querySelector("[name='reason']");
                                                    var visitorNames = form.querySelector(".visitorNames");
                                                    var visitorCID = form.querySelector("[name='visitorCID']");
                                                    var v_phoneNumber = form.querySelector("[name='v_phoneNumber']");
                                                    
                                                    // Convert numeric values to strings for validation
                                                    var visitorCIDValue = visitorCID.value.toString();
                                                    var vPhoneNumberValue = v_phoneNumber.value.toString();
                                        
                                                    // Array to store error messages
                                                    var errorMessages = [];
                                        
                                                    // Validate staff name
                                                    var staffNameRegex = /^[a-zA-Z\s]{3,20}$/;
                                                    if (!staffNameRegex.test(staffName.value) || !staffNameRegex.test(visitorNames.value)) {
                                                        errorMessages.push("Staff Name or visitor name must contain only alphabets and have a length between 3 to 20 characters.");
                                                        staffName.classList.add("error-input");
                                                        visitorNames.classList.add("error-input");
                                                    }
                                        
                                                    // Validate that start date is less than the current date
                                                    var currentDate = new Date();
                                                    if (new Date(startDate.value) <= currentDate) {
                                                        errorMessages.push("Start date must be greater than or equal to the current date.");
                                                        startDate.classList.add("error-input");
                                                    }
                                        
                                                    // Validate that end date is greater than or equal to start date
                                                    if (new Date(endDate.value) <= new Date(startDate.value)) {
                                                        errorMessages.push("End date must be greater than the start date.");
                                                        endDate.classList.add("error-input");
                                                    }
                                        
                                                    // Validate reason
                                                    var reasonRegex = /^[a-zA-Z\s]+$/;
                                                    if (!reasonRegex.test(reason.value)) {
                                                        errorMessages.push("Reason can only contain alphabets.");
                                                        reason.classList.add("error-input");
                                                    }
                                        
                                                    var wordCount = reason.value.split(/\s+/).filter(function (word) {
                                                        return word.length > 0;
                                                    }).length;
                                        
                                                    if (wordCount < 3 || wordCount > 100) {
                                                        errorMessages.push("Reason must be between 3 and 100 words.");
                                                        reason.classList.add("error-input");
                                                    }
                                        
                                                    // Validate Visitor CID
                                                    var visitorCIDRegex = /^\d{11}$/;
                                                    if (!visitorCIDRegex.test(visitorCIDValue)) {
                                                        errorMessages.push("Visitor CID must be exactly 11 digits.");
                                                        visitorCID.classList.add("error-input");
                                                    }
                                        
                                                    // Validate Visitor Phone Number
                                                    var phoneNumberRegex = /^(77|17)\d{6}$/;
                                                    if (!phoneNumberRegex.test(vPhoneNumberValue)) {
                                                        errorMessages.push("Visitor phone number must be a valid Bhutanese number and be exactly 8 digits.");
                                                        v_phoneNumber.classList.add("error-input");
                                                    }
                                        
                                                    // Validate staff email with "gcit" subdomain
                                                    var emailRegex = /^[a-zA-Z0-9._-]+\.gcit@rub\.edu\.bt$/;
                                                    if (!emailRegex.test(staffEmail.value)) {
                                                        errorMessages.push("Invalid email address format. The email should have the gcit domain name.");
                                                        staffEmail.classList.add("error-input");
                                                    }
                                        
                                                    // Display all error messages at once
                                                    if (errorMessages.length > 0) {
                                                        var errorMessageList = errorMessages.map(function (message) {
                                                            return "<li>" + message + "</li>";
                                                        }).join("");
                                                        document.querySelector(".VisitorErrorMessage ul").innerHTML = errorMessageList;
                                                        document.querySelector(".VisitorErrorMessage").style.display = "block";
                                                    } else {
                                                        // If all validations pass, submit the form
                                                        form.submit();
                                                    }
                                                });
                                            });
                                        </script>
                                        
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                        <script>
                            // Function to dynamically add visitor name input fields and handle errors
                            function handleNumOfVisitorsInput() {
                                const numOfVisitorsInput = document.getElementById("numOfVisitors");
                                const numOfVisitors = parseInt(numOfVisitorsInput.value);
                                const numOfVisitorsError = document.getElementById("numOfVisitorsError");

                                // Clear any existing input fields and error message
                                const visitorNamesContainer = document.getElementById("visitorNamesContainer");
                                visitorNamesContainer.innerHTML = "";
                                numOfVisitorsError.textContent = "";

                                // Check for valid input (not negative and not a decimal)
                                if (isNaN(numOfVisitors) || numOfVisitors < 1 || numOfVisitors % 1 !== 0) {
                                    numOfVisitorsInput.classList.add("error-border");
                                    numOfVisitorsError.textContent = "Please enter a valid positive integer.";
                                } else {
                                    numOfVisitorsInput.classList.remove("error-border");

                                    // Add input fields for visitor names based on the number of visitors
                                    for (let i = 1; i <= numOfVisitors; i++) {
                                        const input = document.createElement("input");
                                        input.required = true;
                                        input.classList.add("w-100", "student-leave-input", "px-3",'mt-3','visitorNames');
                                        input.type = "text";
                                        input.placeholder = "Visitor Name " + i;
                                        input.name = "vistor_Name[]";
                                        visitorNamesContainer.appendChild(input);
                                    }
                                }
                            }

                            // Add event listeners to handle changes in the "Number Of Visitor" input field
                            const numOfVisitorsInput = document.getElementById("numOfVisitors");
                            numOfVisitorsInput.addEventListener("input", handleNumOfVisitorsInput);
                        </script>
                        <button class="student-leave-button mt-5 ms-md-2" data-toggle="modal" data-target="#studentleaveform">STUDENT LEAVE</button>
                
                        {{-- student modal form --}}
                        <div class="modal fade" id="studentleaveform" tabindex="-1" role="dialog" aria-labelledby="studentleaveformLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                
                            {{-- modal content --}}
                                <div class="modal-content formContainer p-3" style="border-radius: 18px">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="studentleaveformLabel">STUDENT LEAVE FORM</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 26 25" fill="none">
                                            <rect x="1" y="21.9204" width="31" height="4" transform="rotate(-45 1 21.9204)" fill="rgba(0, 0, 0, 0.72)"/>
                                            <rect x="2.82861" width="31" height="4" transform="rotate(45 2.82861 0)" fill="rgba(0, 0, 0, 0.72)"/>
                                          </svg>
                                        </span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        {{-- student leave --}}
                                        <form id="leaveform" action="{{ url('studentLeaveForm') }}" method="POST" class="d-flex flex-column">
                                            @csrf
                                            <input type="text" required name="studentID" class="student-leave-input px-3" placeholder="Enter your student ID">
                                            <div class="input-group mt-3">
                                                <label for="LeaveStartDate" class="input-group-text">Pick the leave start date:</label>
                                                <input id="LeaveStartDate" name="leaveStartDate" type="date" class="form-control student-leave-input date" required>
                                            </div>
                                            <span id="startDateError" class="ValidationErrorMessage" style="display:none;color:red;"></span>

                                            <div class="input-group mt-3">
                                                <label for="LeaveEndDate" class="input-group-text">Pick your return date:</label>
                                                <input id="LeaveEndDate" name="leaveEndDate" type="date" class="form-control student-leave-input date" required>
                                            </div>
                                            <span id="endDateError" class="ValidationErrorMessage" style="display:none;color:red;"></span>

                                            <textarea id="reason" name="reason" class="reason-input p-3 mt-3" id="" cols="30" rows="5" required placeholder="Reason for leave"></textarea>
                                            <span id="reasonError" class="ValidationErrorMessage" style="display:none;color:red;"></span>

                                            <button type="submit" class="btn mt-4 student-leave-button-submit"><span>Submit</span></button>
                                        </form>
                                        <script>
                                            document.addEventListener("DOMContentLoaded", function () {
                                                // Get the form element
                                                var form = document.querySelector("#leaveform");
                                        
                                                // Add a submit event listener to the form
                                                form.addEventListener("submit", function (event) {
                                                    // Prevent the default form submission
                                                    event.preventDefault();
                                        
                                                    // Reset previous error messages
                                                    document.getElementById("startDateError").style.display = "none";
                                                    document.getElementById("endDateError").style.display = "none";
                                                    document.getElementById("reasonError").style.display = "none";
                                        
                                                    // Get form values
                                                    var startDate = form.querySelector("[name='leaveStartDate']").value;
                                                    var endDate = form.querySelector("[name='leaveEndDate']").value;
                                                    var reason = form.querySelector("[name='reason']").value;
                                        
                            
                                                    // Validate that start date is less than current date
                                                    var currentDate = new Date();
                                                    if (new Date(startDate) <= currentDate) {
                                                        document.getElementById("startDateError").innerHTML = "Start date must be greater than or equal the current date.";
                                                        document.getElementById("startDateError").style.display = "block";
                                                        return;
                                                    }
                                                    // Validate that end date is greater than or equal to start date
                                                    if (new Date(endDate) <= new Date(startDate)) {
                                                        document.getElementById("endDateError").innerHTML = "End date must be greater than to start date";
                                                        document.getElementById("endDateError").style.display = "block";
                                                        return;
                                                    }
                                                    

                                                    
                                                    // Validate reason
                                                    var reasonRegex = /^[a-zA-Z\s]+$/;

                                                    if (!reasonRegex.test(reason)) {
                                                        document.getElementById("reasonError").innerHTML = "Reason can only contain alphabets";
                                                        document.getElementById("reasonError").style.display = "block";
                                                        return;
                                                    }

                                                    var wordCount = reason.split(/\s+/).filter(function (word) {
                                                        return word.length > 0;
                                                    }).length;

                                                    if (wordCount < 5 || wordCount > 250) {
                                                        document.getElementById("reasonError").innerHTML = "Reason must be between 5 and 250 words.";
                                                        document.getElementById("reasonError").style.display = "block";
                                                        return;
                                                    }

                                    
                                                    // If all validations pass, submit the form
                                                    form.submit();
                                                });
                                            });
                                        </script>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </section>      
@endsection