@extends('layouts.HRLayout')

@section('hrcss')
    <link rel="stylesheet" href="{{ asset('css/hr-css/dashboard.css') }}">
@endsection

@section('title-addition')
    Visitors Record
@endsection

@section('dashboard-heading')
    Visitors Record
@endsection

@section('dashboard-content')
    <div class="col-md-12 mt-4">
            <h1 class="visitors-record-heading">Visitor Records: Individuals Currently On Campus</h1>
    </div>
    <div class="col-md-12 mt-3 mb-0 section-container d-flex flex-wrap">
        <button class="sortStudentButtonBlock buttonAll buttonActive px-4" data-visitor="All">All</button>
       
        <button data-visitor="shortTerm" class="sortStudentButtonBlock px-4">Short Term</button>
        <button data-visitor="longTerm" class="sortStudentButtonBlock px-4">Long Term</button>
     
    </div>
    {{-- Visitors Record --}}
    <div class=" col-md-12 section-container table-responsive mt-3">
        <table id="visitorRecordTable" class="table table-bordered custom-table">
            <thead>
                <tr class="table-title py-3">
                    <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                    <th scope="col">CID NUMBER</th>
                    <th scope="col">VISITOR TYPE</th>
                    <th scope="col">NO. OF VISITOR</th>
                    <th scope="col">PHONE NUMBER</th>
                    <th scope="col">DEPENDENT</th>
                    <th scope="col">CHECK-IN-DATE-TIME</th>
                    <th scope="col" class="rounded-top-right">REASON</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $count = 1;
                @endphp
                @foreach ($visitorCheckInRecord as $visitor)
                <tr>
                    <td class="ps-3">{{$count}}</td>
                    <td>{{$visitor->visitorCID}}</td>
                    <td class="text-capitalize">{{$visitor->visitor_type}}</td>
                    <td class="text-center">
                        <!-- Button trigger modal -->
                        <button class="noOfVisitorButton" data-bs-toggle="modal" data-bs-target="#No_ofVisitor{{$visitor->visitorEntryExitID}}">
                            <div class="VisitorNumber">
                                {{$visitor->no_visitor}}
                            </div>
                        </button>
                       </td>
                    <!-- No of visitor Modal -->
                    <div class="modal fade" id="No_ofVisitor{{$visitor->visitorEntryExitID}}" tabindex="-1" aria-labelledby="No_ofVisitorLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                                <div class="modal-header border-0">
                                    <h5 class="modal-title scanHeading">No of Visitors</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p class="scan-sub-heading" style="font-size: 14px">Total number of visitor:  <span style="color: rgb(4, 146, 35);font-weight:800;"> {{$visitor->no_visitor}}</span></p>
                                    <h6 class="vistornamesHeading">Visitors Name:</h6>
                                    <ol>
                                        @foreach (json_decode($visitor->visitor_names) as $visitorName)
                                            <li class="visitorsName">{{ $visitorName }}</li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td>{{$visitor->v_phoneNumber}}</td>
                    <td>{{$visitor->dependentName}}</td>
                    <td>{{$visitor->checkInDateTime}}</td>
                    <td> 
                        <button style="font-size: 12px" type="button" class="btn btn-success py-0" data-bs-toggle="modal" data-bs-target="#visitorReason{{$visitor->visitorEntryExitID}}">
                            View
                        </button>
                    </td>

                    <!-- View Reason Modal -->
                    <div class="modal fade" id="visitorReason{{$visitor->visitorEntryExitID}}" tabindex="-1" aria-labelledby="visitorReasonLabel" aria-hidden="true">
                        <div class="modal-dialog  modal-dialog-scrollable modal-dialog-centered">
                        <div class="modal-content p-3">
                            <div class="modal-header">
                            <h1 class="modal-title fs-5" id="visitorReasonLabel">Visitor CID Number: <span style="color: rgb(255, 98, 0)">{{$visitor->visitorCID}} </span> campus entry reason.</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <p>{{$visitor->reason}}</p>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </tr>
                @php
                    $count ++;
                @endphp
                @endforeach
                <tr id="noDataMatchedMessage" class=" alert-danger text-center" style="display: none;">
                    <td colspan="9">Oops, sorry! No data for the selected visitor</td>
                </tr>
            </tbody>
        </table>
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script>
          $(document).ready(function () {
            $(".sortStudentButtonBlock").on("click", function () {
              var selectedBlock = $(this).data("visitor");
        
              // Update button states
              $(".sortStudentButtonBlock").removeClass("buttonActive");
              $(this).addClass("buttonActive");
        
              // Show/hide rows based on the selected block
              if (selectedBlock === "All") {
                $("#visitorRecordTable tbody tr").show();
                $("#noDataMatchedMessage").hide();
              } else {
                var matchingRows = $("#visitorRecordTable tbody tr").filter(function () {
                  return $(this).find("td:eq(2)").text() === selectedBlock;
                });
        
                if (matchingRows.length > 0) {
                  $("#visitorRecordTable tbody tr").hide();
                  matchingRows.show();
                  $("#noDataMatchedMessage").hide();
                } else {
                  // Display a message if no data matched
                  $("#visitorRecordTable tbody tr").hide();
                  $("#noDataMatchedMessage").show();
                }
              }
            });
          });
        </script>
</div>


<div class="col-md-12 mt-4">
    <h1 class="visitors-record-heading">Visitor History Records: checkin & checkout</h1>
</div>
<div class="col-md-12 mt-3 mb-0 section-container d-flex flex-wrap">
<button class="sortVisitors buttonAll buttonActive px-4" data-visitorchekinout="All">All</button>

<button data-visitorchekinout="shortTerm" class="sortVisitors px-4">Short Term</button>
<button data-visitorchekinout="longTerm" class="sortVisitors px-4">Long Term</button>

</div>
{{-- Visitors Record --}}
<div class=" col-md-12 section-container table-responsive mt-3">
<table id="visitorRecordCheckinOutTable" class="table table-bordered custom-table">
    <thead>
        <tr class="table-title py-3">
            <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
            <th scope="col">CID NUMBER</th>
            <th scope="col">VISITOR TYPE</th>
            <th scope="col">NO. OF VISITOR</th>
            <th scope="col">PHONE NUMBER</th>
            <th scope="col">DEPENDENT</th>
            <th scope="col">CHECK-IN-DATE-TIME</th>
            <th scope="col" class="rounded-top-right">CHECK-OUT-DATE-TIME</th>
        </tr>
    </thead>
    <tbody>
        @php
            $count = 1;
        @endphp
        @foreach ($visitorCheckInOutRecords as $visitor)
        <tr>
            <td class="ps-3">{{$count}}</td>
            <td>{{$visitor->visitorCID}}</td>
            <td class="text-capitalize">{{$visitor->visitor_type}}</td>
            <td class="text-center">
                <!-- Button trigger modal -->
                <button class="noOfVisitorButton" data-bs-toggle="modal" data-bs-target="#No_ofVisitor{{$visitor->visitorEntryExitID}}">
                    <div class="VisitorNumber">
                        {{$visitor->no_visitor}}
                    </div>
                </button>
               </td>
            <!-- No of visitor Modal -->
            <div class="modal fade" id="No_ofVisitor{{$visitor->visitorEntryExitID}}" tabindex="-1" aria-labelledby="No_ofVisitorLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                        <div class="modal-header border-0">
                            <h5 class="modal-title scanHeading">No of Visitors</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p class="scan-sub-heading" style="font-size: 14px">Total number of visitor:  <span style="color: rgb(4, 146, 35);font-weight:800;"> {{$visitor->no_visitor}}</span></p>
                            <h6 class="vistornamesHeading">Visitors Name:</h6>
                            <ol>
                                @foreach (json_decode($visitor->visitor_names) as $visitorName)
                                    <li class="visitorsName">{{ $visitorName }}</li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <td>{{$visitor->v_phoneNumber}}</td>
            <td>{{$visitor->dependentName}}</td>
            <td>{{$visitor->checkInDateTime}}</td>
            <td>{{$visitor->checkOutDateTime}}</td>
        </tr>
        @php
            $count ++;
        @endphp
        @endforeach
        <tr id="noDataMatchedMessageV" class=" alert-danger text-center" style="display: none;">
            <td colspan="9">Oops, sorry! No data for the selected visitor</td>
        </tr>
    </tbody>
</table>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
  $(document).ready(function () {
    $(".sortVisitors").on("click", function () {
      var selectedBlock = $(this).data("visitorchekinout");

      // Update button states
      $(".sortVisitors").removeClass("buttonActive");
      $(this).addClass("buttonActive");

      // Show/hide rows based on the selected block
      if (selectedBlock === "All") {
        $("#visitorRecordCheckinOutTable tbody tr").show();
        $("#noDataMatchedMessageV").hide();
      } else {
        var matchingRows = $("#visitorRecordCheckinOutTable tbody tr").filter(function () {
          return $(this).find("td:eq(2)").text() === selectedBlock;
        });

        if (matchingRows.length > 0) {
          $("#visitorRecordCheckinOutTable tbody tr").hide();
          matchingRows.show();
          $("#noDataMatchedMessageV").hide();
        } else {
          // Display a message if no data matched
          $("#visitorRecordCheckinOutTable tbody tr").hide();
          $("#noDataMatchedMessageV").show();
        }
      }
    });
  });
</script>
</div>

{{ $visitorCheckInOutRecords->links('pagination::bootstrap-5') }}
@endsection
