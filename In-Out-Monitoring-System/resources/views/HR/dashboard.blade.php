@extends('layouts.HRLayout')

@section('hrcss')
    <link rel="stylesheet" href="{{ asset('css/hr-css/dashboard.css') }}">
@endsection

@section('title-addition')
    Dashboard
@endsection

@section('dashboard-heading')
    Dashboard
@endsection

@section('dashboard-content')
<div class="mt-3" id="currentVisitorInTheCampusGraph"></div>
<script>
    var options = {
       series: [{
       name: 'Long Term',
       data: [{{$longTermVisitorCheckIn}}],
       color: '#F28C28' 

     }, {
       name: 'Short Term',
       data: [{{$ShortTermVisitorCheckOut}}],
       color: '#239F49' 
     }],
       chart: {
       type: 'bar',
       height: 200,
       stacked: true,
     },
     plotOptions: {
       bar: {
         horizontal: true,
         dataLabels: {
           total: {
             enabled: true,
             offsetX: 0,
             style: {
               fontSize: '13px',
               fontWeight: 900
             }
           }
         }
       },
     },
     stroke: {
       width: 1,
       colors: ['#fff']
     },
     title: {
       text: 'Current Long-Term and Short-Term Campus Visitors in the campus'
     },
     xaxis: {
       categories: ['Current Visitors'],
       labels: {
         formatter: function (val) {
           return val
         }
       }
     },
     yaxis: {
       title: {
         text: undefined
       },
     },
     tooltip: {
       y: {
         formatter: function (val) {
           return val
         }
       }
     },
     fill: {
       opacity: 1
     },
     legend: {
       position: 'top',
       horizontalAlign: 'left',
       offsetX: 40
     }
     };

     var chart = new ApexCharts(document.querySelector("#currentVisitorInTheCampusGraph"), options);
     chart.render();
 </script>
<div id="visitorVisitInCampusMonthly"></div>
<script>
    var options = {
        series: [
            {
                name: "Long Term",
                data: {!! json_encode($currentYearVisitorData->pluck('long_term_count')->toArray()) !!},
                color:'#F56E2C',
            },
            {
                name: "Short Term",
                data: {!! json_encode($currentYearVisitorData->pluck('short_term_count')->toArray()) !!},
                color:'#239F49',
            }
        ],
        chart: {
            height: 350,
            type: 'line',
            dropShadow: {
                enabled: true,
                color: '#000',
                top: 18,
                left: 7,
                blur: 10,
                opacity: 0.2
            },
            toolbar: {
                show: false
            }
        },
        dataLabels: {
            enabled: true,
        },
        stroke: {
            curve: 'smooth'
        },
        title: {
            text: 'Monthly Comparison of Long-Term and Short-Term Campus Visitors (' + {!! json_encode(date('Y')) !!} + ')',
            align: 'left'
        },
        grid: {
            borderColor: '#e7e7e7',
            row: {
                colors: ['#f3f3f3', 'transparent'],
                opacity: 0.5
            },
        },
        markers: {
            size: 1
        },
        xaxis: {
            categories: {!! json_encode($currentYearVisitorData->pluck('month')->map(function($month) { return date('M', mktime(0, 0, 0, $month, 1)); })->toArray()) !!},
            title: {
                text: 'Month'
            }
        },
        yaxis: {
            title: {
                text: 'No. of visitor'
            },
        },
        legend: {
            position: 'top',
            horizontalAlign: 'right',
            floating: true,
            offsetY: -25,
            offsetX: -5
        }
    };

    var chart = new ApexCharts(document.querySelector("#visitorVisitInCampusMonthly"), options);
    chart.render();
</script>



    <div id="visitorYearlyGraph"></div>
    <script>
       var options = {
          series: [{
          name: 'Long Term',
          data: @json($YearlyVisitorData->pluck('long_term_count')),
          color: '#F28C28' 

        }, {
          name: 'Short Term',
          data: @json($YearlyVisitorData->pluck('short_term_count')),
          color: '#239F49' 
        }],
          chart: {
          type: 'bar',
          height: 350,
          stacked: true,
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              total: {
                enabled: true,
                offsetX: 0,
                style: {
                  fontSize: '13px',
                  fontWeight: 900
                }
              }
            }
          },
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        title: {
          text: 'Yearly Comparison of Long-Term and Short-Term Campus Visitors'
        },
        xaxis: {
          categories: @json($YearlyVisitorData->pluck('year')),
          labels: {
            formatter: function (val) {
              return val
            }
          }
        },
        yaxis: {
          title: {
            text: undefined
          },
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val
            }
          }
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: 'top',
          horizontalAlign: 'left',
          offsetX: 40
        }
        };

        var chart = new ApexCharts(document.querySelector("#visitorYearlyGraph"), options);
        chart.render();
    </script>
    
@endsection
