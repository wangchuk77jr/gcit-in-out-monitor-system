@extends('layouts.HRLayout')
@section('title-addition')
    Setting
@endsection

@section('hrcss')
    <link rel="stylesheet" href="{{asset('css/profile-setting.css')}}">
@endsection
@section('dashboard-heading')
    Profile Setting
@endsection
@section('dashboard-content')
    @include('partials.Profile-setting')
@endsection