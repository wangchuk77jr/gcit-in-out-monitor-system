@extends('layouts.HRHeadlayout')
@section('title-addition')
    Setting
@endsection

@section('hrheadcss')
    <link rel="stylesheet" href="{{asset('css/profile-setting.css')}}">
@endsection
@section('dashboard-heading')
    Profile Setting
@endsection
@section('dashboard-content')
    @include('partials.Profile-setting')
@endsection