@extends('layouts.HRHeadlayout')

@section('hrheadcss')
    <link rel="stylesheet" href="{{ asset('css/hrhead-css/dashboard.css') }}">
@endsection

@section('title-addition')
    Dashboard
@endsection

@section('dashboard-heading')
    Dashboard
@endsection

@section('dashboard-content')

<div class="mt-2" id="chart"></div>
<script>
    // Use Blade syntax to inject PHP variables into JavaScript
    var pendingLeave = {{ $pendingVisitor }};
    var totalLeave = {{ $approvedVisitor }};

    var options = {
        series: [{
            name: 'Pending Visitors',
            data: [pendingLeave],
            color: '#239F49', // Color for the first bar
        }, {
            name: 'Approved Visitors',
            data: [totalLeave],
            color: '#F56E2C', // Color for the second bar
        }],
        chart: {
            type: 'bar',
            height: 150,
            stacked: true,
        },
        plotOptions: {
            bar: {
                horizontal: true,
                dataLabels: {
                    total: {
                        enabled: true,
                        offsetX: 50,
                        style: {
                            fontSize: '13px',
                            fontWeight: 700,
                        },
                        formatter: function (w) {
                            return pendingLeave.toString() +' Pending visitor'; // Display the difference as the data label
                        }
                    }
                }
            },
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title: {
            text: 'Tracking Pending and Approved Visitors',
        },
        xaxis: {
            categories: ['Visitor Status'],
            labels: {
                formatter: function (val) {
                    return val
                }
            }
        },
        yaxis: {
            title: {
                text: undefined
            },
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val
                }
            }
        },
        fill: {
            opacity: 1
        },
        legend: {
            position: 'top',
            horizontalAlign: 'left',
            offsetX: 40
        },
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
</script>

    {{-- Visitors Record --}}
        <div class=" col-md-12 section-container table-responsive">
            <table class="table table-bordered custom-table">
                <thead>
                    <tr class="table-title py-3">
                        <th scope="col" class="rounded-top-left ps-3">SL.NO</th>
                        <th scope="col">CID NUMBER</th>
                        <th scope="col">Dependent</th>
                        <th scope="col">NO. OF VISITOR</th>
                        <th scope="col">PHONE NUMBER</th>
                        <th scope="col">START DATE</th>
                        <th scope="col">END DATE</th>
                        <th scope="col">NO. OF DAYS</th>
                        <th scope="col" class="rounded-top-right">STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $count = 1;
                    @endphp
                    @foreach ($visitorEntryForm as $v)
                    @php
                        $start_date = new DateTime($v->start_date);
                        $end_date = new DateTime($v->end_date);
                        $interval = $start_date->diff($end_date);
                        $number_of_days = $interval->days;
                    @endphp
                    <tr>
                        <td class="ps-3">{{$count}}</td>
                        <td>{{ $v->visitorCID}}</td>
                        <td>{{ $v->dependentName}}</td>
                        <td class="text-center">
                            <!-- Button trigger modal -->
                            <button class="noOfVisitorButton" data-bs-toggle="modal" data-bs-target="#No_ofVisitor{{$v->visitorID}}">
                                <div class="VisitorNumber">
                                    {{ $v->no_visitor}}
                                </div>
                            </button>
                           </td>
                        <!-- No of visitor Modal -->
                        <div class="modal fade" id="No_ofVisitor{{$v->visitorID}}" tabindex="-1" aria-labelledby="No_ofVisitorLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                                    <div class="modal-header border-0">
                                        <h5 class="modal-title scanHeading">No of Visitors</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="scan-sub-heading" style="font-size: 14px">Total number of visitor:  <span style="color: rgb(4, 146, 35);font-weight:800;">{{ $v->no_visitor}}</span></p>
                                        <h6 class="vistornamesHeading">Visitors Name:</h6>
                                        <ol>
                                            @foreach (json_decode($v->visitor_names) as $visitorName)
                                                <li class="visitorsName">{{ $visitorName }}</li>
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <td>{{ $v->v_phoneNumber}}</td>
                        <td>{{ $v->start_date}}</td>
                        <td>{{ $v->end_date}}</td>
                        <td class="text-center">{{$number_of_days}} days</td>
                        <td>
                            @if ($v->Visitor_Approval === null)
                                <button class="approve-button"  data-bs-toggle="modal" data-bs-target="#approveModal{{$v->visitorID}}">Approve</button>
                                <button class="decline-button" data-bs-toggle="modal" data-bs-target="#declineModal{{$v->visitorID}}">Decline</button>
                            @elseif ($v->Visitor_Approval === 'accepted')
                              <span style="color:green;font-weight:700;">Approved</span>
                            @elseif ($v->Visitor_Approval === 'expired')
                               <span style="color:orange;font-weight:700;">Expired</span>
                            @elseif ($v->Visitor_Approval === 'Decline')
                              <span style="color:red;font-weight:700;" >Declined</span>  
                            @else
                                <button class="approve-button"  data-bs-toggle="modal" data-bs-target="#approveModal{{$v->visitorID}}">Approve</button>
                                <button class="decline-button" data-bs-toggle="modal" data-bs-target="#declineModal{{$v->visitorID}}">Decline</button>
                            @endif
                        </td>
                    </tr>
                    @php
                       $count++;
                    @endphp
                    <!-- Modal Approve-->
                    <div class="modal fade" id="approveModal{{$v->visitorID}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="approveModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content px-3 py-2">
                            <div class="modal-header border-0">
                            <h5 class="modal-title" id="approveModalLabel">Visitor Approval</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-wrap justify-content-between w-100 border-bottom">
                                    <p class="visitor-detail w-50"><strong>Visitor CID:</strong> {{ $v->visitorCID}}</p>
                                    <p class="visitor-detail w-50"><strong>Visitor Phone Number:</strong> {{ $v->v_phoneNumber}}</p>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between w-100 border-bottom mt-3">
                                    <p class="visitor-detail w-50"><strong>Dependent:</strong> {{ $v->dependentName}}</p>
                                    <p class="visitor-detail w-50"><strong>Dependent’s EID:</strong> {{ $v->employeId}} </p>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between w-100 border-bottom mt-3">
                                    <p class="visitor-detail w-50"><strong>Start Date:</strong> {{ $v->start_date}}</p>
                                    <p class="visitor-detail w-50"><strong>End Date:</strong> {{ $v->end_date}}</p>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between w-100 border-bottom mt-3">
                                <p class="visitor-detail w-50"><strong>Number days of stay:</strong> {{$number_of_days}} days</p>
                                </div>
                                <div class="w-100 mt-3  border-bottom">
                                    <p class="visitor-detail">
                                        <strong>Visitor Names:</strong> 
                                        @foreach (json_decode($v->visitor_names) as $visitorName)
                                            <span class="visitorsName">{{ $visitorName }}, </span>
                                        @endforeach
                                    </p>
                                </div>
                                <div class="w-100 mt-3">
                                    <p class="visitor-detail">
                                        <strong>Reason for visiting:</strong> 
                                        {{ $v->reason}}
                                    </p>
                                </div>
                            </div>
                            <div class="border-top d-flex gap-4 ps-3 py-4">
                            <form action="{{url('hrhead/visitorApproval')}}" method="post">
                                @csrf
                                <input hidden type="text" value="{{$v->visitorID}}" name="visitorID">
                                <input hidden type="text" name="visitorApproval" value="accepted">
                                <input hidden type="text" name="staffEmail" value ="{{ $v->staffEmail}} "/>
                                <input hidden  type="text" name="message" value="Your visitor has been approved to visit the campus from ({{$v->start_date}} to {{$v->end_date}})">
                                <button type="submit" class="approve-button px-5">Approve</button>
                            </form>
                            <button type="button" class="decline-button px-5" data-bs-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!-- Modal Decline -->
                    <div class="modal fade" id="declineModal{{$v->visitorID}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="declineModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content px-3 py-2">
                            <div class="modal-header">
                            <h5 class="modal-title" id="declineModalLabel">Visitor Decline Confirmation</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                            <p>
                                Are you sure you want to deny approval for visitor
                                @foreach (json_decode($v->visitor_names) as $visitorName)
                                                <span style="color: red" class="visitorsName">{{ $visitorName }}, </span>
                                @endforeach
                                to enter the campus?
                            </p>
                            </div>
                            <div class="modal-footer justify-content-start">
                            <form action="{{url('hrhead/visitorApproval')}}" method="POST">
                                @csrf
                                <input hidden type="text" value="{{$v->visitorID}}" name="visitorID">
                                <input hidden type="text" name="visitorApproval" value="Decline">
                                <input hidden type="text" name="staffEmail" value ="{{ $v->staffEmail}} "/>
                                <textarea required name="message" id="" class="w-100 p-4" cols="100" rows="3" placeholder="Please enter the reason for declining the visitor approval"></textarea>
                                <div class="mt-2">
                                    <button type="submit" class="px-5 decline-button-form">Decline</button>
                                    <button type="button" class="px-5 ms-md-3 decline-button" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                   
                    @endforeach
         
                </tbody>
            </table>
            <!-- Display pagination links -->
            {{ $visitorEntryForm->links('pagination::bootstrap-5') }}
    </div>
    <div class="col-md-12" id="studentLeaveChart"></div>
<script>
    var currentYear = new Date().getFullYear(); // Get the current year

    var options = {
        series: [{
            name: 'Total Applied',
            data: {!! json_encode($visitorCountStatusCurrentYear->pluck('total_logs')) !!},
            color: '#F28C28' 

        }, {
            name: 'Total Approved',
            data: {!! json_encode($visitorCountStatusCurrentYear->pluck('total_approved')) !!},
            color: '#239F49' 
        }, {
            name: 'Total Declined',
            data: {!! json_encode($visitorCountStatusCurrentYear->pluck('total_declined')) !!},
            color: '#FF5733' 
        }],
        chart: {
            type: 'bar',
            height: 300,
            title: 'Visitor Statistics by Month' // Add your desired title here
        },
        title: {
            text: 'Visitor Statistics by Month for ' + currentYear,
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: {!! json_encode($visitorCountStatusCurrentYear->pluck('month')->map(function($month) {
                return DateTime::createFromFormat('!m', $month)->format('M');
            })) !!},
            labels: {
                rotate: 0,
                style: {
                    fontSize: '12px'
                }
            }
        },
        yaxis: {
            title: {
                text: 'No of visitors'
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val;
                }
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#studentLeaveChart"), options);
    chart.render();
</script>

@endsection
