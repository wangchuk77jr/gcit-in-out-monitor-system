@extends('layouts.HRHeadlayout')

@section('hrheadcss')
    <link rel="stylesheet" href="{{asset('css/hrhead-css/visitorApproval.css')}}">
@endsection

@section('title-addition')
    Visitors Record
@endsection

@section('dashboard-heading')
    Visitors Record
@endsection

@section('dashboard-content')
@if (count($visitorEntryForm) > 0)
    {{-- Visitor Approval --}}
    @foreach ($visitorEntryForm as $v)
    @php
        $start_date = new DateTime($v->start_date);
        $end_date = new DateTime($v->end_date);
        $interval = $start_date->diff($end_date);
        $number_of_days = $interval->days;
    @endphp
    <div class="col-md-12 p-4 w-100 mt-4 visitorApprovalContainer">
       <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <p class="message">Mrs./Mr. {{ $v->dependentName}} as dependent of visitor CID {{ $v->visitorCID}} sent you a visitor approval</p>
                </div>
                <div class="col-md-12">
                    <p class="timeDate">
                        {{ $v->created_at->timezone('Asia/Thimphu')->format('j F Y') }}
                        <span>{{ $v->created_at->timezone('Asia/Thimphu')->format('g:ia') }}</span>
                        <span>{{ $v->created_at->timezone('Asia/Thimphu')->diffForHumans() }}</span>                                               
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-center p-0">
            <div class="row w-100">
                <div class="col-md-6">
                    <button class="w-100 approve-button"  data-bs-toggle="modal" data-bs-target="#approveModal{{$v->visitorID}}">Approve</button>
                </div>
                <div class="col-md-6 mt-md-0 mt-2">
                    <button class="w-100 decline-button" data-bs-toggle="modal" data-bs-target="#declineModal{{$v->visitorID}}">Decline</button>
                </div>
            </div>
        </div>
       </div>
    </div>
  <!-- Modal Approve-->
  <div class="modal fade" id="approveModal{{$v->visitorID}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="approveModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content px-3 py-2">
        <div class="modal-header border-0">
          <h5 class="modal-title" id="approveModalLabel">Visitor Approval</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="d-flex flex-wrap justify-content-between w-100 border-bottom">
                <p class="visitor-detail w-50"><strong>Visitor CID:</strong> {{ $v->visitorCID}}</p>
                <p class="visitor-detail w-50"><strong>Visitor Phone Number:</strong> {{ $v->v_phoneNumber}}</p>
            </div>
            <div class="d-flex flex-wrap justify-content-between w-100 border-bottom mt-3">
                <p class="visitor-detail w-50"><strong>Dependent:</strong> {{ $v->dependentName}}</p>
                <p class="visitor-detail w-50"><strong>Dependent’s EID:</strong> {{ $v->employeId}} </p>
            </div>
            <div class="d-flex flex-wrap justify-content-between w-100 border-bottom mt-3">
                <p class="visitor-detail w-50"><strong>Start Date:</strong> {{ $v->start_date}}</p>
                <p class="visitor-detail w-50"><strong>End Date:</strong> {{ $v->end_date}}</p>
            </div>
            <div class="d-flex flex-wrap justify-content-between w-100 border-bottom mt-3">
              <p class="visitor-detail w-50"><strong>Number days of stay:</strong> {{$number_of_days}} days</p>
            </div>
            <div class="w-100 mt-3  border-bottom">
                <p class="visitor-detail">
                    <strong>Visitor Names:</strong> 
                    @foreach (json_decode($v->visitor_names) as $visitorName)
                        <span class="visitorsName">{{ $visitorName }}, </span>
                    @endforeach
                </p>
            </div>
            <div class="w-100 mt-3">
                <p class="visitor-detail">
                    <strong>Reason for visiting:</strong> 
                    {{ $v->reason}}
                </p>
            </div>
        </div>
        <div class="border-top d-flex gap-4 ps-3 py-4">
          <form action="{{url('hrhead/visitorApproval')}}" method="post">
            @csrf
            <input hidden type="text" value="{{$v->visitorID}}" name="visitorID">
            <input hidden type="text" name="visitorApproval" value="accepted">
            <input hidden type="text" name="staffEmail" value ="{{ $v->staffEmail}} "/>
            <input hidden  type="text" name="message" value="Your visitor has been approved to visit the campus from ({{$v->start_date}} to {{$v->end_date}})">
            <button type="submit" class="approve-button px-5">Approve</button>
          </form>
          <button type="button" class="decline-button px-5" data-bs-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Decline -->
  <div class="modal fade" id="declineModal{{$v->visitorID}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="declineModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content px-3 py-2">
        <div class="modal-header">
          <h5 class="modal-title" id="declineModalLabel">Visitor Decline Confirmation</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>
            Are you sure you want to deny approval for visitor
            @foreach (json_decode($v->visitor_names) as $visitorName)
                            <span style="color: red" class="visitorsName">{{ $visitorName }}, </span>
            @endforeach
            to enter the campus?
        </p>
        </div>
        <div class="modal-footer justify-content-start">
           <form action="{{url('hrhead/visitorApproval')}}" method="POST">
            @csrf
            <input hidden type="text" value="{{$v->visitorID}}" name="visitorID">
            <input hidden type="text" name="visitorApproval" value="Decline">
            <input hidden type="text" name="staffEmail" value ="{{ $v->staffEmail}} "/>
             <textarea required name="message" id="" class="w-100 p-4" cols="100" rows="3" placeholder="Please enter the reason for declining the visitor approval"></textarea>
             <div class="mt-2">
                <button type="submit" class="px-5 decline-button-form">Decline</button>
                <button type="button" class="px-5 ms-md-3 decline-button" data-bs-dismiss="modal">Cancel</button>
             </div>
           </form>
        </div>
      </div>
    </div>
  </div>
  @endforeach

  @elseif(count($visitorEntryForm) === 0)
  <div class="col-md-12 d-flex flex-column justify-content-center align-items-center mt-3">
    <h6 style="font-weight: bold;font-size:20px;">There are no visitors for approval!</h6>
     <img style="height: 550px;" src="{{asset('images/hrhead/No data-pana.png')}}" alt="no data" class="img-fluid">
  </div>
  @endif

  
@endsection
