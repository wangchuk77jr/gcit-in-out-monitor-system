<!DOCTYPE html>
<html>
<head>
    <title>Daily Student Report</title>
</head>
<body>
        <ol>
            <h2>Names of Students Who Are Off-Campus</h2>
            @foreach ($outOfCampusStudents as $student)
            <li style="padding-top:10px">{{ $student->studentName }} from ({{$student->studentYear}} year <b>{{$student->studentCourse}}</b>)</li>
            @endforeach
        </ol>
        <br> <br>
        <a  style="text-decoration: none;background: orange;color: #fff;padding:20px;" href="http://127.0.0.1:8000/HR/visitors-record">View In Detail</a>

</body>
</html>
