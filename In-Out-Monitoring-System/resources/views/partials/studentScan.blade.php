    <!-- Button to trigger the scan modal -->
    <button type="button" class="button-check-out-in d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#scanModal" onclick="openCamera()">
        <p class="me-auto button-check-out-in-text text-uppercase pt-3">Student Check-Out</p>
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="20" viewBox="0 0 46 29" fill="none">
                <rect width="46" height="29" fill="white"/>
                <rect x="0.5" y="0.5" width="45" height="28" stroke="black" stroke-opacity="0.37"/>
                <path d="M23.7071 7.29273C23.3166 6.90221 22.6834 6.90221 22.2929 7.29273L15.9289 13.6567C15.5384 14.0472 15.5384 14.6804 15.9289 15.0709C16.3195 15.4614 16.9526 15.4614 17.3431 15.0709L23 9.41405L28.6569 15.0709C29.0474 15.4614 29.6805 15.4614 30.0711 15.0709C30.4616 14.6804 30.4616 14.0472 30.0711 13.6567L23.7071 7.29273ZM24 24.167L24 7.99984H22L22 24.167H24Z" fill="#239F49"/>
            </svg>
        </div>
    </button>

    <!-- Scan Modal -->
    <div class="modal fade" id="scanModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="scanHeading">Student Check-Out</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p id="scan-sub-heading">Scan student card</p>
                    <!-- Video element for the camera stream -->
                    <video id="video" autoplay></video>
                    <!-- Canvas element for capturing frames -->
                    <canvas id="canvas" style="display:none;width:100%;"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for QR Code Result -->
    <div class="modal fade" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                <div class="modal-header border-0  p-0 mt-3">
                    <h5 class="modal-title" id="resultHeading">Student Check-Out Result</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body result-qr-code-container mt-4 px-3">
                    <h6 class="check-out-time mt-2" id="result"></h6>

                    <!-- Element to display the QR code result -->
                    <div class="mt-2">
                        <h6 class="check-out-time">Check-in Time----- <span id="current-time"></span></h6>
                        <h6 id="student-details" class="student-details"><span id="studentName"></span> <span id="studentCourse"></span> <span id="studentYear"></span> Year check-out successfully!</h6>
                        <img class="my-2 img-fluid" id="student-profile" src="{{ asset('studentProfile/no_image.png') }}" alt="student-profile">

                    </div>
                    {{-- error --}}
                    <div>
                        <h4 class="mt-4 mb-4" id="resultERR"></h4>
                    </div>
                </div>
                <form action="">
                    <input hidden type="text" name="srudentID" id="studentID">
                    <input hidden type="text" name="checkOutTime" id="checkOutTime">
                    <input hidden type="text" name="checkOutDate" id="checkOutDate">
                    <input hidden type="text" name="entryType" value="studentCheckOut">
                    <button type="submit" class="w-100 mt-4 mb-4 checkinout-confrim-button ">Confirm</button>
                </form>
            </div>
        </div>
    </div>
<audio id="notificationSound"  src="{{ asset('images/notification2.mp3') }}"></audio>
<!-- JavaScript in your HTML -->
<script>
    let scanning = false;
    let stopScanning = false; // Flag to stop scanning
    let cameraStream = null; // Store the camera stream for closing

    function openCamera() {
        if (!scanning) {
            scanning = true;
            const videoElement = document.getElementById("video");
            const canvasElement = document.getElementById("canvas");
            const context = canvasElement.getContext("2d");

            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({ video: true })
                    .then(function (stream) {
                        videoElement.srcObject = stream;
                        cameraStream = stream; // Store the camera stream

                        function scanQRCode() {
                            if (stopScanning) {
                                scanning = false;
                                stopScanning = false; // Reset the stopScanning flag
                                stream.getTracks().forEach((track) => {
                                    track.stop();
                                });
                                return;
                            }

                            context.drawImage(videoElement, 0, 0, canvasElement.width, canvasElement.height);
                            const imageData = context.getImageData(0, 0, canvasElement.width, canvasElement.height);

                            const code = jsQR(imageData.data, imageData.width, imageData.height);
                            if (code) {
                                // Capture the detected QR code data
                                const qrData = code.data;

                                // Play the notification sound
                                const notificationSound = document.getElementById("notificationSound");
                                notificationSound.play();

                                // Update the time immediately when QR code is detected
                                const currentTime = getCurrentTime();
                                const currentTimeElement = document.getElementById('current-time');
                                currentTimeElement.textContent = currentTime;

                                // Close the scanning modal
                                $('#scanModal').modal('hide');

                                // Reset the camera stream
                                if (cameraStream) {
                                    cameraStream.getTracks().forEach((track) => {
                                        track.stop();
                                    });
                                }

                                // Fetch and display the result
                                fetchResultFromServer(qrData);
                            }

                            requestAnimationFrame(scanQRCode);
                        }

                        // Add a timeout to stop scanning after a certain time (e.g., 3 seconds)
                        setTimeout(function () {
                            stopScanning = true;
                        }, 3000);

                        scanQRCode();
                    })
                    .catch(function (error) {
                        console.error("Error accessing the camera: " + error);
                        scanning = false;
                    });
            } else {
                console.error("getUserMedia not supported in this browser");
                scanning = false;
            }
        }
    }

    function fetchResultFromServer(qrData) {
        // Get the CSRF token from a meta tag
        const csrfToken = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            type: "POST",
            url: "/process-qr-code", // Update the URL to match your route
            data: {
                _token: csrfToken, // Include the CSRF token
                qrData: qrData
            },
            success: function (response) {
                if (response.error) {
                    $("#resultERR").text(response.error);
                    $(".student-details, #student-profile, .checkinout-confrim-button, .check-out-time").css({
                        display: "none"
                    });
                } else {
                    // Clear the error message and show student details
                    $("#resultERR").text("");
                    $("#studentID").val(response.studentID);
                    $("#studentName").text(response.studentName);
                    $("#studentCourse").text(response.studentCourse);
                    $("#studentYear").text(response.studentYear);
                    if (response.profileImage) {
                        $("#student-profile").attr("src", response.profileImage);
                    } else {
                        $("#student-profile").attr("src", "No Image");
                    }
                    $(".student-details, #student-profile, .checkinout-confrim-button, .check-out-time").css({
                        display: "block"
                    });
                }
                $('#resultModal').modal('show');
            },
            error: function (err) {
                console.error("Error sending QR code data: " + err);
            }
        });
    }

    function getCurrentTime() {
        const now = new Date();
        let hours = now.getHours();
        let minutes = now.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12;

        minutes = minutes < 10 ? '0' + minutes : minutes;

        const currentTime = hours + ':' + minutes + ampm;

        return currentTime;
    }

    const currentTimeElement = document.getElementById('current-time');
    const checkOuttime = document.getElementById('checkOutTime');
    const currentTime = getCurrentTime();
    currentTimeElement.textContent = currentTime;
    checkOuttime.value = currentTime;

    // Get the current date
    const today = new Date();

    // Get the day, month, and year
    const day = String(today.getDate()).padStart(2, '0');
    const month = String(today.getMonth() + 1).padStart(2, '0'); // Months are 0-based, so we add 1
    const year = today.getFullYear();

    // Create the formatted date in "dd mm yyyy" format
    const formattedDate = `${day} ${month} ${year}`;

    // Set the default value of the input
    document.getElementById('checkOutDate').value = formattedDate;

</script>

