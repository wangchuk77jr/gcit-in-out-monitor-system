    <!-- Button to trigger the scan modal -->
    <button type="button" class="button-check-out-in d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#scanModal" onclick="openCamera()">
        <p class="me-auto button-check-out-in-text text-uppercase pt-3">Student Check-Out</p>
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="20" viewBox="0 0 46 29" fill="none">
                <rect width="46" height="29" fill="white"/>
                <rect x="0.5" y="0.5" width="45" height="28" stroke="black" stroke-opacity="0.37"/>
                <path d="M23.7071 7.29273C23.3166 6.90221 22.6834 6.90221 22.2929 7.29273L15.9289 13.6567C15.5384 14.0472 15.5384 14.6804 15.9289 15.0709C16.3195 15.4614 16.9526 15.4614 17.3431 15.0709L23 9.41405L28.6569 15.0709C29.0474 15.4614 29.6805 15.4614 30.0711 15.0709C30.4616 14.6804 30.4616 14.0472 30.0711 13.6567L23.7071 7.29273ZM24 24.167L24 7.99984H22L22 24.167H24Z" fill="#239F49"/>
            </svg>
        </div>
    </button>

    <!-- Scan Modal -->
    <div class="modal fade" id="scanModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="scanHeading">Student Check-Out</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p id="scan-sub-heading">Scan student card</p>
                    <!-- Video element for the camera stream -->
                    <video id="video" autoplay></video>
                    <!-- Canvas element for capturing frames -->
                    <canvas id="canvas" style="display:none;width:100%;"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for QR Code Result -->
    <div class="modal fade" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content py-3 px-5" style="border-radius: 20px;">
                <div class="modal-header border-0  p-0 mt-3">
                    <h5 class="modal-title" id="resultHeading">Check-Out Confirmation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body result-qr-code-container mt-4 mb-3 px-3">
                    <h6 class="check-out-time mt-2" id="result"></h6>
                    <p id="resultCheckOut" class="d-none"></p>
                    <!-- Element to display the QR code result -->
                    <div class="mt-2">
                        <h6 class="check-out-time">Check-Out Time----- <span id="current-time"></span></h6>
                        <h6 id="student-details" class="student-details">
                            <span class="text-capitalize" id="studentName"></span>,
                            <span id="studentYear"></span>
                            <span id="studentCourse"></span>,
                            is ready to checkout.
                        </h6>
                        <img class="mt-3 img-fluid" id="student-profile" src="{{ asset('studentProfile/no_image.png') }}" alt="student-profile">

                    </div>
                    {{-- error --}}
                    <div>
                        <h4 class="mt-4 mb-4" id="resultERR"></h4>
                    </div>
                </div>
                <form action="{{ url('securityGuard/student-exit-entry-record') }}" method="post">
                    @csrf
                    <input hidden required type="text" name="studentID" id="studentID">
                    <input hidden required type="text" name="checkOutDateTime" id="checkOutDateTime">
                    <button type="submit" class="w-100 mb-4 checkinout-confrim-button ">Confirm</button>
                </form>
            </div>
        </div>
    </div>
<audio id="notificationSound"  src="{{ asset('images/notification2.mp3') }}"></audio>
<script>
    $(document).ready(function () {
      const videoElement = document.getElementById('video');
      const canvasElement = document.getElementById('canvas');
      const context = canvasElement.getContext('2d');
      const resultElement = document.getElementById('resultCheckOut');
      const notificationSound = document.querySelector('.notificationSound');
      let isNotificationPlayed = false;
      let stream = null;
      let qrCodeScannerInterval = null;
  
      function startCamera() {
        navigator.mediaDevices.getUserMedia({ video: true })
          .then(function (cameraStream) {
            if (stream) {
              stream.getTracks().forEach(track => track.stop());
              videoElement.srcObject = null;
            }
            stream = cameraStream;
            videoElement.srcObject = stream;
            videoElement.play();
  
            clearInterval(qrCodeScannerInterval);
            qrCodeScannerInterval = setInterval(qrCodeScanner, 1000);
          })
          .catch(function (error) {
            console.error('Error accessing the webcam: ', error);
          });
      }
  
      function stopCamera() {
        clearInterval(qrCodeScannerInterval);
  
        if (stream) {
          const tracks = stream.getTracks();
          tracks.forEach(track => track.stop());
          videoElement.srcObject = null;
          stream = null;
        }
      }
  
      function qrCodeScanner() {
        context.drawImage(videoElement, 0, 0, canvasElement.width, canvasElement.height);
        const imageData = context.getImageData(0, 0, canvasElement.width, canvasElement.height);
  
        const code = jsQR(imageData.data, imageData.width, imageData.height);
        if (code) {
          resultElement.textContent = 'QR Code detected: ' + code.data;
  
          if (!isNotificationPlayed) {
            notificationSound.play();
            isNotificationPlayed = true;
          }
  
          const currentTime = getCurrentTime();
          const currentTimeElement = document.getElementById('current-time');
          currentTimeElement.textContent = currentTime;

          const checkOutDateTime= document.getElementById('checkOutDateTime');
          checkOutDateTime.value = getCurrentDateTime();
          // Call the function to fetch data from the server

          fetchResultFromServer(code.data);
  
          // Close the camera and modal
          stopCamera();
          $('#scanModal').modal('hide');
        }
      }
  
      function getCurrentTime() {
        const now = new Date();
        let hours = now.getHours();
        let minutes = now.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12;

        minutes = minutes < 10 ? '0' + minutes : minutes;

        const currentTime = hours + ':' + minutes + ampm;

        return currentTime;
    }

  
      $('#scanModal').on('shown.bs.modal', function () {
        startCamera();
      });
  
      $('#scanModal').on('hidden.bs.modal', function () {
        stopCamera();
        resultElement.textContent = '';
        isNotificationPlayed = false;
      });
  
      // Function to fetch and display the result from the server
      function fetchResultFromServer(qrData) {
        // Get the CSRF token from a meta tag
        const csrfToken = $('meta[name="csrf-token"]').attr('content');
  
        $.ajax({
          type: "POST",
          url: "/process-qr-code", // Update the URL to match your server route
          data: {
            _token: csrfToken, // Include the CSRF token
            qrData: qrData
          },
          success: function (response) {
            if (response.error) {
              // Handle the error response
              $("#resultERR").text(response.error);
              $(".student-details, #student-profile, .checkinout-confrim-button, .check-out-time").css({
                display: "none"
              });
            } else {
              // Display the student details
              $("#resultERR").text("");
              $("#studentID").val(response.studentID);
              $("#studentName").text(response.studentName);
              $("#studentCourse").text(response.studentCourse);
              $("#studentYear").text(response.studentYear);
              if (response.profileImage) {
                $("#student-profile").attr("src", response.profileImage);
              } else {
                $("#student-profile").attr("src", "No Image");
              }
              $(".student-details, #student-profile, .checkinout-confrim-button, .check-out-time").css({
                display: "block"
              });
            }
            $('#resultModal').modal('show');
          },
          error: function (err) {
            console.error("Error sending QR code data: " + err);
          }
        });
      }
    });


    function getCurrentDateTime() {
                const now = new Date();
                const year = now.getFullYear();
                const month = String(now.getMonth() + 1).padStart(2, '0');
                const day = String(now.getDate()).padStart(2, '0');
                const hours = now.getHours();
                const minutes = String(now.getMinutes()).padStart(2, '0');
                const ampm = hours >= 12 ? 'pm' : 'am';

                let formattedHours = hours % 12;
                formattedHours = formattedHours ? formattedHours : 12;

                const currentDate = `${year}-${month}-${day}`;
                const currentTime = `${formattedHours}:${minutes} ${ampm}`;
                const currentDateTime = `${currentDate} ${currentTime}`;

                return currentDateTime;
            }
            
  </script>
  
  


