<!-- Button to trigger the modal -->
<button type="button" class="button-check-out-in d-flex align-items-center px-5" data-bs-toggle="modal" data-bs-target="#checkInModal">
    <p class="me-auto button-check-out-in-text text-uppercase pt-3">Student Check-In</p>
    <div>
        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="20" viewBox="0 0 46 29" fill="none">
            <rect width="46" height="29" fill="white"/>
            <rect x="0.5" y="0.5" width="45" height="28" stroke="black" stroke-opacity="0.37"/>
            <path d="M21.5429 22.8391C21.9334 23.2296 22.5666 23.2296 22.9571 22.8391L29.3211 16.4751C29.7116 16.0846 29.7116 15.4515 29.3211 15.0609C28.9305 14.6704 28.2974 14.6704 27.9069 15.0609L22.25 20.7178L16.5931 15.0609C16.2026 14.6704 15.5695 14.6704 15.1789 15.0609C14.7884 15.4515 14.7884 16.0846 15.1789 16.4751L21.5429 22.8391ZM21.25 5.96484L21.25 22.132H23.25L23.25 5.96484H21.25Z" fill="#239F49"/>
        </svg>
    </div>
</button>

<!-- Modal for QR Code -->
<div class="modal fade" id="checkInModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content py-3 px-5" style="border-radius: 20px;">
            <div class="modal-header border-0">
                <h5 class="modal-title scanHeading">Student Check-In</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p class="scan-sub-heading">Scan student card</p>
                <!-- Video element for the camera stream -->
                <video id="videoCheckIn" autoplay playsinline></video>
                <!-- Canvas element for capturing frames -->
                <canvas id="canvasCheckIn" style="display:none;width:100%;"></canvas>
            </div>
        </div>
    </div>
</div>

<!-- Modal for QR Code Result -->
<div class="modal fade" id="checkInResult" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content py-3 px-5" style="border-radius: 20px;">
            <div class="modal-header border-0 p-0 mt-3">
                <h5 class="modal-title" id="resultHeading">Student Check-In Result</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body result-qr-code-container mt-4 mb-4 px-3">
                <!-- Element to display the QR code result -->
                <div class="mt-2">
                    <p id="resultCheckIn" class="d-none"></p>
                   <!-- Element to display the QR code result -->
                    <div class="mt-2">
                        <h6 class="check-out-time check-in-time">Check-in Time----- <span id="current-time-checkin"></span></h6>
                        <h6 id="student-details" class="student-details">
                          <span id="studentNameCheckIn"></span>,
                          <span id="studentYearCheckIn"></span> 
                          <span id="studentCourseCheckIn"></span>,
                          is ready to checkin.
                        </h6>
                        <img class="my-2 img-fluid student-profile" id="student-profileCheckIn" src="{{ asset('studentProfile/no_image.png') }}" alt="student-profile">
                    </div>
                    {{-- error --}}
                     <div>
                        <h4 class="mt-4 mb-4" id="resulterror"></h4>
                     </div>
            </div>
            <form method="post" action="{{ url('securityGuard/updateCheckInTimeDate') }}">
              @csrf
              <input hidden type="text" name="studentID" id="studentIDCheckIn" required>
              <input hidden type="text" name="CheckInDateTime" id="newCheckInDateTime" required>
              <button type="submit" class="w-100 mt-4 checkinout-confrim-button">Confirm</button>
            </form>
        </div>
    </div>
</div>

<!-- Audio element for notification sound -->
<audio class="notificationSound" src="{{ asset('images/notification2.mp3') }}"></audio>
<script>
    $(document).ready(function () {
      const videoElement = document.getElementById('videoCheckIn');
      const canvasElement = document.getElementById('canvasCheckIn');
      const context = canvasElement.getContext('2d');
      const resultElement = document.getElementById('resultCheckIn');
      const notificationSound = document.querySelector('.notificationSound');
      let isNotificationPlayed = false;
      let stream = null;
      let qrCodeScannerInterval = null;
  
      function startCamera() {
        navigator.mediaDevices.getUserMedia({ video: true })
          .then(function (cameraStream) {
            if (stream) {
              stream.getTracks().forEach(track => track.stop());
              videoElement.srcObject = null;
            }
            stream = cameraStream;
            videoElement.srcObject = stream;
            videoElement.play();
  
            clearInterval(qrCodeScannerInterval);
            qrCodeScannerInterval = setInterval(qrCodeScanner, 1000);
          })
          .catch(function (error) {
            console.error('Error accessing the webcam: ', error);
          });
      }
  
      function stopCamera() {
        clearInterval(qrCodeScannerInterval);
  
        if (stream) {
          const tracks = stream.getTracks();
          tracks.forEach(track => track.stop());
          videoElement.srcObject = null;
          stream = null;
        }
      }
  
      function qrCodeScanner() {
        context.drawImage(videoElement, 0, 0, canvasElement.width, canvasElement.height);
        const imageData = context.getImageData(0, 0, canvasElement.width, canvasElement.height);
  
        const code = jsQR(imageData.data, imageData.width, imageData.height);
        if (code) {
          resultElement.textContent = 'QR Code detected: ' + code.data;
  
          if (!isNotificationPlayed) {
            notificationSound.play();
            isNotificationPlayed = true;
          }
  
          const currentTime = getCurrentTime();
          const currentTimeElement = document.getElementById('current-time-checkin');
          currentTimeElement.textContent = currentTime;
          const newcheckInDateTime = document.getElementById('newCheckInDateTime');
          newcheckInDateTime.value = getCurrentDateTime();

  
          // Call the function to fetch data from the server
          fetchResultFromServer(code.data);
  
          // Close the camera and modal
          stopCamera();
          $('#checkInModal').modal('hide');
        }
      }
  
      function getCurrentTime() {
        const now = new Date();
        let hours = now.getHours();
        let minutes = now.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12;

        minutes = minutes < 10 ? '0' + minutes : minutes;

        const currentTime = hours + ':' + minutes + ampm;

        return currentTime;
    }

    const currentTimeElement = document.getElementById('current-time-checkin');
    const currentTime = getCurrentTime();
    currentTimeElement.textContent = currentTime;
  
      $('#checkInModal').on('shown.bs.modal', function () {
        startCamera();
      });
  
      $('#checkInModal').on('hidden.bs.modal', function () {
        stopCamera();
        resultElement.textContent = '';
        isNotificationPlayed = false;
      });
  
      // Function to fetch and display the result from the server
      function fetchResultFromServer(qrData) {
        // Get the CSRF token from a meta tag
        const csrfToken = $('meta[name="csrf-token"]').attr('content');
  
        $.ajax({
          type: "POST",
          url: "/process-qr-code", // Update the URL to match your server route
          data: {
            _token: csrfToken, // Include the CSRF token
            qrData: qrData
          },
          success: function (response) {
            if (response.error) {
              // Handle the error response
              $("#resulterror").text(response.error);
              $(".student-details, .student-profile, .checkinout-confrim-button, .check-in-time").css({
                display: "none"
              });
            } else {
              // Display the student details
              $("#resulterror").text("");
              $("#studentIDCheckIn").val(response.studentID);
              $("#studentNameCheckIn").text(response.studentName);
              $("#studentCourseCheckIn").text(response.studentCourse);
              $("#studentYearCheckIn").text(response.studentYear);
              if (response.profileImage) {
                $("#student-profileCheckIn").attr("src", response.profileImage);
              } else {
                $("#student-profileCheckIn").attr("src", "No Image");
              }
              $(".student-details, .student-profile, .checkinout-confrim-button, .check-in-time").css({
                display: "block"
              });
            }
            $('#checkInResult').modal('show');
          },
          error: function (err) {
            console.error("Error sending QR code data: " + err);
          }
        });
      }
    });

    function getCurrentDateTime() {
                const now = new Date();
                const year = now.getFullYear();
                const month = String(now.getMonth() + 1).padStart(2, '0');
                const day = String(now.getDate()).padStart(2, '0');
                const hours = now.getHours();
                const minutes = String(now.getMinutes()).padStart(2, '0');
                const ampm = hours >= 12 ? 'pm' : 'am';

                let formattedHours = hours % 12;
                formattedHours = formattedHours ? formattedHours : 12;

                const currentDate = `${year}-${month}-${day}`;
                const currentTime = `${formattedHours}:${minutes} ${ampm}`;
                const currentDateTime = `${currentDate} ${currentTime}`;

                return currentDateTime;
            }
            
    
  </script>
  
  
