  {{-- proflie setting section --}}
  <div class="col-md-12 section-container mt-3">
    <div class="row">
        <div class="col-md-7">
            <div class="profile-update-content  d-flex flex-column align-items-center w-100 px-5">
                <h4 class="text-uppercase profile-heading mt-4">Profile Setting</h4>
                <div class="profile-image-container mt-2">  
                    @if (Auth::user()->profile_image)
                        <img src="{{ asset('profile_images/' . Auth::user()->profile_image) }}" alt="User Profile Image">
                    @else
                        <img src="{{ asset('images/defualtProfileImage.png') }}" alt="Default Profile Image">
                    @endif
                </div>
                <form class="d-flex flex-column gap-3 w-100 px-3 mt-3" action="{{ route('profile.update.submit') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <label for="profile-image" class="custom-file-upload">
                        <input name="profileImage" id="profile-image" class="w-100 profile-update-input px-md-3" type="file" onchange="displaySelectedImage(this)">
                        <span id="selected-image-path">Choose Profile Image</span> 
                        @if (Auth::user()->profile_image)
                            <img  class="choose-profile-image" src="{{ asset('profile_images/' . Auth::user()->profile_image) }}" alt="User Profile Image">
                        @else
                        <img  class="choose-profile-image" src="{{ asset('images/defualtProfileImage.png') }}" alt="Default Profile Image">
                        @endif
                    </label>
                    
                    <script>
                        function displaySelectedImage(input) {
                            var selectedImageName = "";
                            if (input.files && input.files[0]) {
                                // Get the file name without the path
                                selectedImageName = input.files[0].name;
                    
                                var reader = new FileReader();
                    
                                reader.onload = function (e) {
                                    // Display the selected image
                                    $('.choose-profile-image').attr('src', e.target.result);
                                };
                    
                                reader.readAsDataURL(input.files[0]);
                    
                                // Set the selected image name in the span element
                                document.getElementById('selected-image-path').innerText = selectedImageName;
                            } else {
                                // Handle the case where no file is selected
                                document.getElementById('selected-image-path').innerText = "No image selected";
                            }
                        }
                    </script>
                    
                    <input  name="userName" required class="w-100 profile-upadte-input px-3" type="text" value="{{ old('userName', auth()->user()->name) }}">
                    <input name="email" class="w-100 profile-upadte-input px-3" type="text" value="{{ old('email', auth()->user()->email) }}">
                    <button class="w-100 mt-3 text-uppercase saveButton">Save</button>
                </form>
            </div>
        </div>
        <div class="col-md-5 mt-md-0 mt-3">
            <div class="reset-password d-flex flex-column align-items-center w-100 px-5">
                <h4 class="text-uppercase  profile-heading mt-4">Password setting</h4>
                <form autocomplete="off" method="post" action="{{ route('password.update') }}"  class="d-flex flex-column gap-3 w-100 px-1 mt-5">
                    @csrf
                    @method('put')
                    @if (session('status') === 'password-updated')
                    <div id="success-message" class="mt-1 alert-success p-2">
                        {{ __('Password has been updated successfully') }}
                    </div>
                    <script>
                        setTimeout(function () {
                            var successMessage = document.getElementById('success-message');
                            if (successMessage) {
                                successMessage.style.display = 'none';
                            }
                        }, 3000); // 3000 milliseconds = 3 seconds
                    </script>
                    @endif
                    <input autocomplete="off" required class="w-100 resetPassword-input px-3" type="password" placeholder="Current password" name="current_password">
                    @if($errors->updatePassword->has('current_password'))
                    <div class="alert-danger p-2">
                        {{ $errors->updatePassword->first('current_password') }}
                    </div>
                    @endif
                
                    <input autocomplete="off" required class="w-100 resetPassword-input px-3" type="password" placeholder="New password" name="password">
                    @if($errors->updatePassword->has('password'))
                    <div class="alert-danger p-2">
                        {{ $errors->updatePassword->first('password') }}
                    </div>
                    @endif
                
                    <input required class="w-100 resetPassword-input px-3" type= "password" autocomplete="off" placeholder="Confirmed password" name="password_confirmation">
                    @if($errors->updatePassword->has('password_confirmation'))
                    <div class="alert-danger p-2">
                        {{ $errors->updatePassword->first('password_confirmation') }}
                    </div>
                    @endif
                
                    <button type="submit" class="w-100 mt-5 text-uppercase resetpasswordButton">{{ __('Update') }}</button>
                </form>
                
            </div>
        </div>
    </div>
</div>