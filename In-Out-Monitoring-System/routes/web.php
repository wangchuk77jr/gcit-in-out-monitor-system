<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backendPanel\adminController;
use App\Http\Controllers\backendPanel\ssoController;
use App\Http\Controllers\backendPanel\securityguardController;
use App\Http\Controllers\backendPanel\counsilorController;
use App\Http\Controllers\backendPanel\hrController;
use App\Http\Controllers\backendPanel\hrheadController;
use App\Http\Controllers\homeController;


Route::get('/', [homeController::class, 'Home'])->name('home');
Route::get('/home', [homeController::class, 'HomePage'])->name('homepgae');

Route::post('/', [homeController::class, 'VisitorForm']);
Route::post('/studentLeaveForm',[homeController::class,'studentLeaveForm'],'studentLeaveForm');


// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'ProfileSetting'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::post('/profile/update',[ProfileController::class, 'updateProfile'])->name('profile.update.submit');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

// admin routes
Route::middleware(['auth','role:admin'])->group(function () {
    Route::get('admin/dashboard',[adminController::class,'dashboard'])->name('Admindashboard');
    Route::post('admin/addUsers',[adminController::class,'AddUsers'])->name('AddUsers');
    Route::put('/update-user/{id}',[adminController::class,'updateUser'])->name('user.update');
    Route::post('/delete-user',[adminController::class,'deleteUser'])->name('userdelete');//deleteuser
    Route::get('admin/users',[adminController::class,'user'])->name('adminUsers');
    Route::post('admin/importStudentExcel',[adminController::class,'importStudentExcel'])->name('importStudentExcel'); // import student from excel
    Route::post('admin/editStudent',[adminController::class,'editStudent'])->name('editStudent'); // editStudent
    Route::post('admin/deleteStudent',[adminController::class,'deleteStudent'])->name('deleteStudent'); // deleteStudent
    Route::post('/update-student-profile-images', [adminController::class,'updateStudentProfileImages'])->name('updateStudentProfileImages');// update profile batch
    Route::post('/UpdateStudentYear', [adminController::class,'updateStudentYear'])->name('UpdateStudentYear');// UpdateStudentYear
    Route::post('/deleteStudentsByYear', [adminController::class,'deleteStudentsByYear'])->name('deleteStudentsByYear');//deleteStudentsByYear
    Route::get('admin/students',[adminController::class,'student'])->name('adminStudents');
    Route::post('admin/studentRecord',[adminController::class,'studentRecord']);
    Route::get('admin/settings',[adminController::class,'setting'])->name('adminSetting');

    
});

// sso routes
Route::middleware(['auth','role:sso'])->group(function () { 
    Route::get('sso/dashboard',[ssoController::class,'dashboard'])->name('SSOdashboard');
    Route::post('sso/leaveApproval',[ssoController::class,'studentLeaveApproval'])->name('studentLeaveApproval');
    Route::get('sso/studentLeave',[ssoController::class,'studentLeave'])->name('ssoStudentLeave');
    Route::get('sso/studentRecord',[ssoController::class,'studentRecord'])->name('ssoStudentRecord');
    Route::get('sso/settings',[ssoController::class,'setting'])->name('ssoSetting');

});

// Security guard routes
Route::post('/process-qr-code',[securityguardController::class,'processQRCode'])->middleware('web');

Route::middleware(['auth','role:security-guard'])->group(function () {
    Route::get('securityGuard/dashboard',[securityguardController::class,'dashboard'])->name('securityGuardDashboard');
    Route::post('securityGuard/student-exit-entry-record', [securityguardController::class, 'StudentCheckOut'])->name('studentexitentrylogs.store');
    Route::post('securityGuard/updateCheckInTimeDate', [securityguardController::class ,'StudentCheckIn'])->name('updateCheckInTimeDate');
    Route::get('securityGuard/student-exit-entry-record',[securityguardController::class,'studentRecord'])->name('security-students-record');
    Route::post('securityGuard/visitorCheckIn',[securityguardController::class,'visitorCheckInLog'])->name('visitorCheckInLog');
    Route::post('securityGuard/visitorCheckOutLog',[securityguardController::class,'visitorCheckOutLog'])->name('visitorCheckOutLog');
    Route::get('securityGuard/visitorRecord',[securityguardController::class,'visitorRecord'])->name('security-visitors-record');
    Route::get('securityGuard/setting',[securityguardController::class,'ProfileSetting'])->name('security-profile-setting');
});


// HR routes
Route::middleware(['auth','role:HR'])->group(function () {
    Route::get('HR/dashboard',[hrController::class,'dashboard'])->name('HRdashboard');
    Route::get('/visitor-chart-data', [hrController::class, 'getVisitorChartData']);
    Route::get('HR/visitors-record',[hrController::class,'visitorRecord'])->name('hr-visitor-record');
    Route::get('HR/profile-setting',[hrController::class,'profileSetting'])->name('hr-setting');

});

// HR Head routes
Route::middleware(['auth','role:HR_Head'])->group(function () {
    Route::get('hrhead/dashboard',[hrheadController::class,'dashboard'])->name('HRHeaddashboard');
    Route::post('hrhead/visitorApproval',[hrheadController::class,'visitorApproval'])->name('visitorApproval');
    Route::post('hrhead/visitorDecline',[hrheadController::class,'visitorDecline'])->name('visitorDecline');
    Route::get('hrhead/visitors-record',[hrheadController::class,'visitorRecord'])->name('hrhead-visitor-record');
    Route::get('hrhead/profile-setting',[hrheadController::class,'profileSetting'])->name('hrhead-setting');
});

// counsilor routes
Route::middleware(['auth','role:councilor'])->group(function () {
    Route::get('counsilor/dashboard',[counsilorController::class,'dashboard'])->name('Counsilordashboard');
    Route::get('counsilor/studentLeave',[counsilorController::class,'studentLeave'])->name('CounsilorStudentLeave');
    Route::get('counsilor/studentRecord',[counsilorController::class,'studentRecord'])->name('CounsilorStudentRecord');
    Route::get('counsilor/settings',[counsilorController::class,'setting'])->name('CounsilorSetting');
});

