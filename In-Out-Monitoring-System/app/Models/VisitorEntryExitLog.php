<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisitorEntryExitLog extends Model
{
    protected $table = 'visitorentryexitlogs';
    protected $primaryKey = 'visitorEntryExitID';
    public $timestamps = true;

    protected $fillable = [
        'checkInDateTime',
        'checkOutDateTime',
        'visitorCID',
        'visitor_type',
        'v_phoneNumber',
        'visitorPassID',
        'dependentName',
        'no_visitor',
        'visitor_names',
        'reason',
    ];
    use HasFactory;
}
