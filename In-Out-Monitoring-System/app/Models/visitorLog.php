<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class visitorLog extends Model
{
    protected $table = 'visitor_log';
    protected $primaryKey = 'visitorID';
    protected $fillable = [
        'visitorCID','Visitor_type','staffEmail','dependentName','employeId','v_phoneNumber','no_visitor',
        'visitor_names','start_date','end_date','reason','Visitor_Approval'
    ];
    use HasFactory;
}
