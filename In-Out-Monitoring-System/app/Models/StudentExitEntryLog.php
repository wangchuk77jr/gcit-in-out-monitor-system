<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentExitEntryLog extends Model
{
    use HasFactory;

    protected $table = 'studentexitentrylogs'; // Set the table name explicitly

    protected $primaryKey = 'exitEntrylogID'; // Set the primary key field

    protected $fillable = [
        'checkInDateTime',
        'checkOutDateTime',
        'studentID',
    ];

    // Define the relationship to the 'student' table
    public function student()
    {
        return $this->belongsTo(Student::class, 'studentID', 'studentID');
    }
    
 }
