<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $primaryKey = 'studentID';
    protected $fillable = ['studentID', 'studentName', 'studentYear', 'studentCourse', 'hotelBlock', 'gender', 'phoneNumber', 'profileImage'];
    use HasFactory;

    public function exitEntryLogs()
    {
        return $this->hasMany(StudentExitEntryLog::class, 'studentID');
    }

    public function leaves()
    {
        return $this->hasMany(StudentLeave::class, 'studentID');
    }
}
