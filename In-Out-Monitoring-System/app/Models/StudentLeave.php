<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentLeave extends Model
{
    use HasFactory;
    protected $table = 'studentleave'; // Set the table name

    protected $primaryKey = 'leaveID'; // Set the primary key field name

    protected $fillable = [
        'StartDate',
        'EndDate',
        'Reason',
        'studentID',
        'LeaveApproval'
    ];

    // Define the relationship with the Student model
    public function student()
    {
        return $this->belongsTo(Student::class, 'studentID', 'studentID');
    }
    
}
