<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class visitorApproval extends Notification
{
    use Queueable;
    private $message;

    /**
     * Create a new notification instance.
     */
    public function __construct($message)
    {
        $this->message= $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Visitors Request to visit the Campus')
            ->greeting('Dear HR Head,')
            ->line('---------------------------------------------------------------------------------------------')
            ->line('Reason for visit: '.$this->message['reason'])
            ->line('---------------------------------------------------------------------------------------------')
            ->line('Visitors dependent name: '.$this->message['dependentName'])
            ->line('Visitor entry date start from: '.$this->message['start_date'].' to '.$this->message['end_date'])
            ->action('Aprove Vitor entry','http://127.0.0.1:8000/hrhead/visitors-record');

            
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
