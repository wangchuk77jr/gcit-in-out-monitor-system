<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class gcitInOutNewUserPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    private $details;
    public function __construct($details)
    {
        $this->details= $details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                ->greeting('New User Password')
                ->line('---------------------------------------------------------------------------------------------')
                ->line('Email Address: '.$this->details['email'])
                ->line('Password: '.$this->details['password'])
                ->line('---------------------------------------------------------------------------------------------')
                ->line("Please refrain from sharing this password with anyone. Use the password provided above to log in and update it as necessary.")
                ->action('Login','http://127.0.0.1:8000/login');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
