<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendEmailNotification extends Notification
{
    use Queueable;

    private $details;

    /**
     * Create a new notification instance.
     */
    public function __construct($details)
    {
        $this->details= $details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Student Leave Request')
            ->greeting('Student Leave Application')
            ->line('---------------------------------------------------------------------------------------------')
            ->line('Dear SSO,')
            ->line($this->details['reason'])
            ->line('---------------------------------------------------------------------------------------------')
            ->line('Student Leave request from: '.$this->details['studentName'].'('.$this->details['year'].' year)')
            ->line('Date of Leave Request: '.$this->details['startDate'].' to '.$this->details['endDate'])
            ->action('Accept Leave','http://127.0.0.1:8000/sso/studentLeave');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
