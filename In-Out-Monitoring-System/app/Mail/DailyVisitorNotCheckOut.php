<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyVisitorNotCheckOut extends Mailable
{
    use Queueable, SerializesModels;

    public $visitorInTheCampus;
    public $user;

    public function __construct($visitorInTheCampus, $user)
    {
        $this->visitorInTheCampus = $visitorInTheCampus;
        $this->user = $user;
    }

    public function build()
    {
        return $this->view('visitorNotCheckOut')
        ->subject('Visitor Not checkout on time');
    }
}
