<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyStudentReport extends Mailable
{
    use Queueable, SerializesModels;

    public $outOfCampusStudents;
    public $user;

    public function __construct($outOfCampusStudents, $user)
    {
        $this->outOfCampusStudents = $outOfCampusStudents;
        $this->user = $user;
    }

    public function build()
    {
        return $this->view('daily_student_report')
        ->subject('Student Out-of-Campus List');
    }
}
