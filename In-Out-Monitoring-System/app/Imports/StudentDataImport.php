<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Student;

class StudentDataImport implements ToModel, WithHeadingRow
{
    private $updatedCount = 0;
    private $newCount = 0;
    private $skippedCount = 0;
    // Define an array of required column names.
    private $requiredColumns = [
        'student_id',
        'student_name',
        'year',
        'course_name',
        'hostel_block',
        'gender',
        'phone_number',
    ];
    public function model(array $row)
    {
        // Check for null values in the row.
        if (
            $row['student_id'] === null ||
            $row['student_name'] === null ||
            $row['year'] === null ||
            $row['course_name'] === null ||
            $row['hostel_block'] === null ||
            $row['gender'] === null ||
            $row['phone_number'] === null
        ) {
            // If any of the required columns have null values, skip this row.
            $this->skippedCount++; // Increment the skipped count.
            return null;
        }

        $existingStudent = Student::where('studentID', $row['student_id'])->first(); // Use Eloquent or your database query method to find the student.

        $gender = strtolower($row['gender']); // Convert gender to lowercase for case-insensitivity.

        if ($gender === 'f') {
            $gender = 'Female';
        } elseif ($gender === 'm') {
            $gender = 'Male';
        } else {
            // Handle other cases if needed.
            // For example, you can set a default gender or handle invalid gender values.
            // $gender = 'Unknown';
        }

        if ($existingStudent) {
            // Update the existing student.
            $existingStudent->studentName = $row['student_name'];
            $existingStudent->studentYear = (int)$row['year'];
            $existingStudent->studentCourse = $row['course_name'];
            $existingStudent->hotelBlock = $row['hostel_block'];
            $existingStudent->gender = $gender; // Use the modified gender value.
            $existingStudent->phoneNumber = (int)$row['phone_number'];
            $this->updatedCount++;
            $existingStudent->save();
            

            return $existingStudent; // Return the updated student object.
        } else {
            // If the student doesn't exist, create a new student.
            $this->newCount++;
            return new Student([
                'studentID' => (int)$row['student_id'],
                'studentName' => $row['student_name'],
                'studentYear' => (int)$row['year'],
                'studentCourse' => $row['course_name'],
                'hotelBlock' => $row['hostel_block'],
                'gender' => $gender, // Use the modified gender value.
                'phoneNumber' => (int)$row['phone_number'],
            ]);
        }
    }
    public function getUpdatedCount()
    {
        return $this->updatedCount;
    }

    public function getNewCount()
    {
        return $this->newCount;
    }

    public function getSkippedCount()
    {
        return $this->skippedCount;
    }
}