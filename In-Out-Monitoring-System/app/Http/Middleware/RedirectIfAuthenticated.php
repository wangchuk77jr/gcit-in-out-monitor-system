<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                // Determine the authenticated user's role
                $user = Auth::guard($guard)->user();
                $role = $user->role; // Adjust this to match how your roles are stored

                // Redirect based on the user's role
                switch ($role) {
                    case 'admin':
                        return redirect(RouteServiceProvider::Admin);
                    case 'security-guard':
                        return redirect(RouteServiceProvider::securityGuard);
                    case 'sso':
                        return redirect(RouteServiceProvider::SSO);
                    case 'councilor':
                        return redirect(RouteServiceProvider::Counsilor);
                    case 'HR':
                        return redirect(RouteServiceProvider::HR);
                    case 'HR_Head':
                        return redirect(RouteServiceProvider::HRHead);
                }
            }
        }

        return $next($request);
    }
}
