<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();

        if($request->user()->role === 'admin'){
            return redirect()->intended(RouteServiceProvider::Admin);
        }
        if($request->user()->role === 'security-guard'){
            return redirect()->intended(RouteServiceProvider::securityGuard);
        }
        if($request->user()->role === 'councilor'){
            return redirect()->intended(RouteServiceProvider::Counsilor);
        }
        if($request->user()->role === 'sso'){
            return redirect()->intended(RouteServiceProvider::SSO);
        }
        if($request->user()->role === 'HR'){
            return redirect()->intended(RouteServiceProvider::HR);
        }
        if($request->user()->role === 'HR_Head'){
            return redirect()->intended(RouteServiceProvider::HRHead);
        }

    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
