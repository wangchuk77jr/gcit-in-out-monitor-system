<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\visitorLog;
use App\Models\User;
use App\Models\StudentLeave;
use App\Models\Student;
use Notification;
use App\Notifications\SendEmailNotification;
use App\Notifications\visitorApproval;


class homeController extends Controller
{
    public function Home(){
        return view('home');
    }

    public function HomePage(){
        return view('homepage');
    }

    public function VisitorForm(Request $request)
    {
        $users = User::where('role', 'HR_Head')
        ->get();
        try {
            $visitor_log = new visitorLog([
                'visitorCID' => $request->visitorCID,
                'Visitor_type' => $request->Visitor_type,
                'staffEmail' => $request->staffEmail,
                'dependentName' => $request->dependentName,
                'employeId' => $request->employeId,
                'v_phoneNumber' => $request->v_phoneNumber,
                'visitor_names' => json_encode($request->input('vistor_Name')),
                'no_visitor' => $request->no_visitor,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'reason' => $request->reason,
            ]);
            $visitorMessage = [
                'dependentName' => $request->dependentName,
                'reason' => $request->reason,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
            ];

            $visitor_log->save();
            // Success message
            if ($users->isNotEmpty()) {
                // Send emails to matching SSO users.
                Notification::send($users, new visitorApproval($visitorMessage));
            }
            return redirect('/')->with('success', 'Visitor entry form has been submitted successfully. Wait for response from HR Head...');
        } catch (\Exception $e) {
            // Error message
            return redirect('/')->with('error', 'An error occurred while saving the visitor entry');
        }
    }

    public function studentLeaveForm(Request $request)
    {
        $studentID = $request->studentID;
        $leaveStart = $request->leaveStartDate;
        $leaveEnd = $request->leaveEndDate;
        $reason = $request->reason;
    
        // Check if there is an existing leave request for the student with an end date after the current date.
        $existingLeaveRequest = StudentLeave::where('studentID', $studentID)
            ->where('EndDate', '>', now()) // Check if the end date is in the future (now() is the current date and time).
            ->where(function ($query) {
                $query->where('LeaveApproval', 'accepted')
                      ->orWhereNull('LeaveApproval');
            })
            ->first();

        $studentName = Student::where('studentID', $studentID)->get()->first();
        $studentYear = Student::where('studentYear', $studentID)->get()->first();
        
    
        if ($existingLeaveRequest) {
            return redirect()->back()->with('error', 'You cannot submit a leave request before the end date of your previous leave request!');
        }
    
        // Retrieve the student's gender (assuming 'gender' is the attribute in the Student model).
        $studentGender = Student::where('studentID', $studentID)->value('gender');
    
        if ($studentGender) {
            // Find SSO users with the same gender as the student's leave-to gender.
            $users = User::where('role', 'sso')
                ->where('gender', $studentGender)
                ->get();
    
            $studentLeaveForm = [
                'year' => $studentName->studentYear,
                'studentName' => $studentName->studentName,
                'startDate' => $leaveStart,
                'endDate' => $leaveEnd,
                'reason' => $reason,
            ];
            if ($users->isNotEmpty()) {
            $studentLeaveRecord = new StudentLeave();
            $studentLeaveRecord->studentID = $studentID;
            $studentLeaveRecord->StartDate = $leaveStart;
            $studentLeaveRecord->EndDate = $leaveEnd;
            $studentLeaveRecord->reason = $reason;
            $studentLeaveRecord->save();
            // Send emails to matching SSO users.
             Notification::send($users, new SendEmailNotification($studentLeaveForm));
            }
    
            return redirect()->back()->with('success', 'Your leave request form has been successfully submitted! Please wait for a response from SSO...');
        } else {
            return redirect()->back()->with('error', "Sorry, it seems you're not a GCIT student, so you can't submit a leave request through this system");
        }
    }
    
    
}
