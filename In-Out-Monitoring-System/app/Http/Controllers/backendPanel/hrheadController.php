<?php

namespace App\Http\Controllers\backendPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\visitorLog;
use Notification;
use App\Notifications\visitorApprovalConfirmation;
use Illuminate\Support\Facades\DB;


class hrheadController extends Controller
{
    public function VisitorRequestCount() {
  
        $totalVisitorRequest = 0; 
            $totalVisitorRequest = DB::table('visitor_log')
                ->whereNull('visitor_log.Visitor_Approval')
                ->count();

        return $totalVisitorRequest;
    }

    public function dashboard()
    {
     $visitorEntryForm = visitorLog::orderByRaw('Visitor_Approval IS NULL DESC, created_at DESC')->paginate(8);
     $approvedVisitor = visitorLog::where('Visitor_Approval', 'accepted')->count();
     $pendingVisitor = visitorLog::whereNull('Visitor_Approval',)->count();
     $year = now()->year;

    $visitorCountStatusCurrentYear = VisitorLog::select(
            DB::raw('YEAR(created_at) AS year'),
            DB::raw('MONTH(created_at) AS month'),
            DB::raw('COUNT(*) AS total_logs'),
            DB::raw('SUM(CASE WHEN Visitor_Approval = "accepted" OR Visitor_Approval = "expired" THEN 1 ELSE 0 END) AS total_approved'),
            DB::raw('SUM(CASE WHEN Visitor_Approval = "Decline" THEN 1 ELSE 0 END) AS total_declined')
        )
        ->whereYear('created_at', $year)
        ->groupBy(DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'))
        ->get();
    
    $totalVisitorRequestCount = $this->VisitorRequestCount();


     
     return view('HR-Head.dashboard',[
        'visitorEntryForm'=>$visitorEntryForm,
        'approvedVisitor'=>$approvedVisitor,
        'pendingVisitor'=> $pendingVisitor,
        'visitorCountStatusCurrentYear'=>$visitorCountStatusCurrentYear,
        'totalVisitorRequestCount'=>$totalVisitorRequestCount,

    ]);
    }

    public function visitorRecord()
    {
        $visitorEntryForm = visitorLog::whereNull('Visitor_Approval')
        ->orWhere('Visitor_Approval', '')
        ->get();
    $totalVisitorRequestCount = $this->VisitorRequestCount();
       
     return view('HR-Head.visitor-record',[
        'visitorEntryForm'=>$visitorEntryForm,
        'totalVisitorRequestCount'=>$totalVisitorRequestCount,
    ]);
    }

    public function visitorApproval(Request $request)
    {
        $visitorID = $request->input('visitorID');
        $visitorApproval = $request->input('visitorApproval');
        $approvalMessage = $request->input('message');
        $staffEmailAddress = $request->input('staffEmail');
        $emailto = $staffEmailAddress;
        if($visitorApproval === 'accepted'){
            $statusmessage = 'Your Visitor Entry Request Accepted';
        }else{
            $statusmessage = 'Reason for Declining Your Visitor Entry Request:';

        }
        $approvalmessage = [
            'statusmessage' =>$statusmessage,
            'approvemessage'=>$approvalMessage,
        ];
    
        $visitorEntryForm = visitorLog::where('visitorID', $visitorID)->first();
    
        if ($visitorEntryForm) {
            // Check if the Visitor_Approval is null
            if ($visitorEntryForm->Visitor_Approval === null) {
                // Check if end_date has passed
                $endDate = date('Y-m-d', strtotime($visitorEntryForm->end_date));
    
                if ($endDate < date('Y-m-d')) {
                    $visitorEntryForm->Visitor_Approval = 'expired';
                    $message = 'Visitor entry form has expired.';
                } else {
                    $visitorEntryForm->Visitor_Approval = $visitorApproval;
                    if($visitorApproval === 'accepted'){
                        $message = 'Visitor entry form has been approved successfully.';
                        Notification::route('mail', $emailto)->notify(new visitorApprovalConfirmation($approvalmessage));
                    }else if($visitorApproval === 'Decline'){
                        $message = 'Visitor entry form has been declined successfully.';
                        Notification::route('mail', $emailto)->notify(new visitorApprovalConfirmation($approvalmessage));

                    }
                }
                $visitorEntryForm->save();
                return redirect()->back()->with('success', $message);
            } else {
                return redirect()->back()->with('error', 'Visitor entry form has already been approved/declined or expired.');
            }
        } else {
            return redirect()->back()->with('error', 'Visitor entry form not found.');
        }
    }

    public function profileSetting()
    {
    $totalVisitorRequestCount = $this->VisitorRequestCount();
     return view('HR-Head.Setting',compact('totalVisitorRequestCount'));
    }
}
