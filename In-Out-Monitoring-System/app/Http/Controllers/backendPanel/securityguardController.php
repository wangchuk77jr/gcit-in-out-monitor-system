<?php

namespace App\Http\Controllers\backendPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\StudentExitEntryLog;
use App\Models\VisitorEntryExitLog;
use App\Models\visitorLog;
use App\Models\StudentLeave;
use Carbon\Carbon;

class securityguardController extends Controller
{
    
    public function dashboard()
    {
     $totalNumberOfUnchecked = StudentExitEntryLog::whereNull('checkInDateTime')->count();
     $totalNumberOfCheckOutVisitor = VisitorEntryExitLog::whereNull('checkOutDateTime')->count();

     return view('security-guard.dashboard',compact('totalNumberOfUnchecked','totalNumberOfCheckOutVisitor'));
    }


    public function processQRCode(Request $request) {
        $qrData = $request->input('qrData');

        // Query the database to retrieve student information based on the QR code data
        $student = Student::where('studentID', $qrData)->first();

        if ($student) {
            // Matching student found
            $result = [
                'studentID' => $student->studentID,
                'studentName' => $student->studentName,
                'studentYear' => $student->studentYear,
                'studentCourse' => $student->studentCourse,
                'profileImage' => asset('studentProfile/' . $student->profileImage),
            ];

            return response()->json($result);
        } else {
            // No matching student found
            return response()->json(['error' => 'Oops, sorry! You are not a GCIT student.']);
        }
    }

    public function StudentCheckOut(Request $request)
    {
        $studentID = $request->input('studentID');
        // $LeaveApproval = $request->input('LeaveApproval');
        $checkOutDateTime = $request->input('checkOutDateTime');
    
        // Check if there are any existing records for the student with a null checkInDateTime
        $existingLogs = StudentExitEntryLog::where('studentID', $studentID)
            ->whereNull('checkInDateTime')
            ->get();
    
        if ($existingLogs->isEmpty()) {
            // No existing entries with null checkInDateTime for the student, so you can insert the new entry
            $log = new StudentExitEntryLog();
            $log->studentID = $studentID;
            $log->checkOutDateTime = $checkOutDateTime;
    
            if ($log->save()) {
                // Insertion was successful
                return redirect('securityGuard/student-exit-entry-record')->with('success', 'Student with ID ' . $studentID . ' has successfully checked out.');
            } else {
                // Insertion failed, show an error message
                return redirect()->back()->with('error', 'An error occurred while saving the checkout record.');
            }
        } else {
            // Existing entries with null checkInDateTime found, throw an error
            return redirect()->back()->with('error', 'Student with ID ' . $studentID . ' has already checked out. Please check in before checking out again.');
        }
    }
    public function StudentCheckIn(Request $request)
    {
        $studentID = $request->input('studentID');
        
        // Get the current date and time in Bhutan's timezone
        $CheckInDateTime = Carbon::now('Asia/Thimphu');        
        // If you want to format the date and time, you can use the format method
        $formattedDateTime = $CheckInDateTime->format('Y-m-d h:i a');

        $newCheckInDateTime = $formattedDateTime;

        // Check if there is an existing record for the student with a null checkInDateTime
        $existingLog = StudentExitEntryLog::where('studentID', $studentID)
            ->whereNull('checkInDateTime')
            ->first();

        if ($existingLog) {
            // An existing entry with a null checkInDateTime was found, so you can update it
            $existingLog->checkInDateTime = $newCheckInDateTime;

            if ($existingLog->save()) {
                // Update was successful
                return redirect('securityGuard/student-exit-entry-record')->with('success', 'Student with ID ' . $studentID . ' has successfully checked in.');
            } else {
                // Update failed, show an error message
                return redirect()->back()->with('error', 'An error occurred while updating the check-in date and time.');
            }
        } else {
            // No existing entry with a null checkInDateTime found, so you cannot update it
            return redirect()->back()->with('error', 'Student with ID ' . $studentID . ' must check out before checking in.');
        }
    }

    public function studentRecord()
    {
        $entryExitLogs = StudentExitEntryLog::with('student')->get();
        $totalNumberOfUnchecked = StudentExitEntryLog::whereNull('checkInDateTime')->count();
        $currentDate = Carbon::today();
        // Retrieve records for today's check-ins and check-outs
        $entryExitLogs = StudentExitEntryLog::with('student')
        ->whereDate('checkInDateTime', $currentDate)
        ->orWhereDate('checkOutDateTime', $currentDate)
        ->orWhere(function ($query) {
            $query->whereNull('checkInDateTime')
                ->orWhereNull('checkOutDateTime');
        })
        ->orderByRaw('IF(checkInDateTime IS NULL OR checkOutDateTime IS NULL, 0, 1)') // Order null check-in/out first
        ->orderBy('checkOutDateTime', 'desc') // Order by check-out date-time descending
        ->orderBy('checkInDateTime', 'desc') // Order by check-in date-time descending
        ->get();

        // Get the total number of check-ins for the current date
        $totalNumberOfCheckedIn = StudentExitEntryLog::whereDate('checkInDateTime', '=', $currentDate)->count();
        $totalNumberOfCheckOut = StudentExitEntryLog::whereDate('checkOutDateTime', '=', $currentDate)->count();


        return view('security-guard.studentRecord', [
            'entryExitLogs' => $entryExitLogs,
            'totalNumberOfUnchecked' => $totalNumberOfUnchecked,
            'totalNumberOfCheckedIn' => $totalNumberOfCheckedIn,
            'totalNumberOfCheckOut' => $totalNumberOfCheckOut,
        ]);    
    }
    public function visitorCheckInLog(Request $request)
    {
        $visitorCID = $request->input('visitorCID');
        $visitorType = $request->input('visitorType');
        $visitorPassID = $request->input('visitorPassID');
        $checkInDateTime = $request->input('checkInDateTime');
        
        // Check if the visitor with the same CID is already checked in
        $existingCheckIn = VisitorEntryExitLog::where('visitorCID', $visitorCID)
        ->whereNull('checkOutDateTime')
        ->first();

        if ($existingCheckIn) {
            // Visitor is already checked in; they need to check out first
            return redirect()->back()->with('error', 'Visitor with the same CID is already checked in and must check out before checking in again.');
        }
        // Create a new VisitorEntryExitLog instance
        $visitorEntryRecord = new VisitorEntryExitLog();
    
        if ($visitorType === 'shortTerm') {
            $visitorEntryRecord->visitor_type = $visitorType;
            $visitorEntryRecord->visitorCID = $visitorCID;
            $visitorEntryRecord->visitorPassID = $visitorPassID;
            $visitorEntryRecord->checkInDateTime = $checkInDateTime;
    
            // Retrieve other short-term visitor data from the request
            $visitorEntryRecord->v_phoneNumber = $request->input('phoneNumber');
            $visitorEntryRecord->no_visitor = $request->input('numberOfVisitor');
            $visitorEntryRecord->visitor_names = json_encode($request->input('vistor_Name'));
            $visitorEntryRecord->dependentName = $request->input('dependentName');
            $visitorEntryRecord->reason = $request->input('reason');
    
            if ($visitorEntryRecord->save()) {
                // Insertion was successful
                return redirect('securityGuard/visitorRecord')->with('success', 'Visitor checked in successfully.');
            } else {
                // Insertion failed, show an error message
                return redirect()->back()->with('error', 'An error occurred while checking in the visitor.');
            }
        } elseif ($visitorType === 'longTerm') {
            $longTermVisitorData = visitorLog::where('visitorCID', $visitorCID)
                ->where('Visitor_Approval', 'accepted')
                ->first();
    
            if ($longTermVisitorData) {
                // Handle long-term visitor check-in if data is found
                $visitorEntryRecord->visitor_type = $visitorType;
                $visitorEntryRecord->visitorCID = $visitorCID;
                $visitorEntryRecord->visitorPassID = $visitorPassID;
                $visitorEntryRecord->checkInDateTime = $checkInDateTime;
    
                $visitorEntryRecord->v_phoneNumber = $longTermVisitorData->v_phoneNumber;
                $visitorEntryRecord->no_visitor = $longTermVisitorData->no_visitor;
                $visitorEntryRecord->visitor_names = $longTermVisitorData->visitor_names;
                $visitorEntryRecord->dependentName = $longTermVisitorData->dependentName;
                $visitorEntryRecord->reason = $longTermVisitorData->reason;
    
                if ($visitorEntryRecord->save()) {
                    // Insertion was successful
                    return redirect('securityGuard/visitorRecord')->with('success', 'Visitor checked in successfully.');
                } else {
                    // Insertion failed, show an error message
                    return redirect()->back()->with('error', 'An error occurred while checking in the visitor.');
                }
            } else {
                // Handle errors for long-term visitors
                $matchingVisitor = visitorLog::where('visitorCID', $visitorCID)->first();
    
                if (!$matchingVisitor) {
                    return redirect()->back()->with('error', 'No longterm visitor found with the provided CID.');
                } elseif ($matchingVisitor->Visitor_Approval !== 'accepted') {
                    return redirect()->back()->with('error', 'The visitor is not approved for check-in.');
                }
            }
        }
    }


    public function visitorCheckOutLog(Request $request)
    {
        $visitorPassID = $request->input('visitorPassID');
        // Get the current date and time in Bhutan's timezone
        $DateTime = Carbon::now('Asia/Thimphu');        
        // If you want to format the date and time, you can use the format method
        $formattedDateTime = $DateTime->format('Y-m-d h:i a');

        $checkOutDateTime = $formattedDateTime;
    
        // Check if the visitor with the given visitorPassID is checked in
        $checkedInVisitor = VisitorEntryExitLog::where('visitorPassID', $visitorPassID)
            ->whereNull('checkOutDateTime')
            ->first();
    
        if ($checkedInVisitor) {
            // Update the check-out date and time
            $checkedInVisitor->checkOutDateTime = $checkOutDateTime;
    
            if ($checkedInVisitor->save()) {
                // Check-out was successful
                return redirect('securityGuard/visitorRecord')->with('success', 'Visitor checked out successfully.');
            } else {
                // Update failed, show an error message
                return redirect()->back()->with('error', 'An error occurred while updating the check-out time.');
            }
        } else {
            // Check if the visitor exists
            $visitorExists = VisitorEntryExitLog::where('visitorPassID', $visitorPassID)->first();
    
            if ($visitorExists) {
                // Visitor is not checked in
                return redirect()->back()->with('error', 'Visitor with the given pass ID is not checked in.');
            } else {
                // Visitor does not exist
                return redirect()->back()->with('error', 'Visitor with the given pass ID does not exist.');
            }
        }
    }
    
    

    public function visitorRecord()
    {
        $VisitorEntryExitLog = VisitorEntryExitLog::all();
        // The current date
        $currentDate = Carbon::today();
        $totalNumberOfUnchecked = VisitorEntryExitLog::whereNull('checkOutDateTime')->count();
        $totalNumberOfCheckedIn = VisitorEntryExitLog::whereDate('checkInDateTime', '=', $currentDate)->count();
        $totalNumberOfCheckOut = VisitorEntryExitLog::whereDate('checkOutDateTime', '=', $currentDate)->count();

        $VisitorEntryExitLog = VisitorEntryExitLog::
        whereDate('checkInDateTime', $currentDate)
        ->orWhereDate('checkOutDateTime', $currentDate)
        ->orWhere(function ($query) {
            $query->whereNull('checkInDateTime')
                ->orWhereNull('checkOutDateTime');
        })
        ->orderByRaw('IF(checkInDateTime IS NULL OR checkOutDateTime IS NULL, 0, 1)') // Order null check-in/out first
        ->orderBy('checkOutDateTime', 'desc') // Order by check-out date-time descending
        ->orderBy('checkInDateTime', 'desc') // Order by check-in date-time descending
        ->get();
    
    
        return view('security-guard.visitorRecord', [
            'VisitorEntryExitLog' => $VisitorEntryExitLog,
            'totalNumberOfUnchecked' => $totalNumberOfUnchecked,
            'totalNumberOfCheckedIn' => $totalNumberOfCheckedIn,
            'totalNumberOfCheckOut' => $totalNumberOfCheckOut,
        ]);
    }
    
    public function ProfileSetting()
    {
     return view('security-guard.setting');
    }
}
