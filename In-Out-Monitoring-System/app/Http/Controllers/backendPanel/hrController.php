<?php

namespace App\Http\Controllers\backendPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VisitorEntryExitLog;
use Illuminate\Support\Facades\DB;


class hrController extends Controller
{
    public function dashboard()
    {
    $longTermVisitorCheckIn = VisitorEntryExitLog::whereNull('checkOutDateTime')
        ->where('visitor_type', 'longTerm')
        ->count();
    
     $ShortTermVisitorCheckOut =VisitorEntryExitLog::wherenull('checkOutDateTime')
        ->where('visitor_type', 'shortTerm')
        ->count();

    $monthlyData = VisitorEntryExitLog::select(
            DB::raw('MONTH(created_at) as month'),
            DB::raw('SUM(CASE WHEN checkInDateTime IS NULL AND visitor_type = "longTerm" THEN 1 ELSE 0 END) as longTerm'),
            DB::raw('SUM(CASE WHEN checkInDateTime IS NULL AND visitor_type = "shortTerm" THEN 1 ELSE 0 END) as shortTerm')
        )
        ->groupBy('month')
        ->get();

    $months = [];
    $longTermData = [];
    $shortTermData = [];

    foreach ($monthlyData as $data) {
        $months[] = date('M', mktime(0, 0, 0, $data->month, 1));
        $longTermData[] = $data->longTerm;
        $shortTermData[] = $data->shortTerm;
    }

    $currentYear = now()->year;

    $currentYearVisitorData = DB::table('visitorentryexitlogs')
        ->select(
            DB::raw('MONTH(checkinDateTime) AS month'),
            DB::raw('YEAR(checkinDateTime) AS year'),
            DB::raw('SUM(CASE WHEN visitor_type = "shortTerm" THEN 1 ELSE 0 END) AS short_term_count'),
            DB::raw('SUM(CASE WHEN visitor_type = "longTerm" THEN 1 ELSE 0 END) AS long_term_count')
        )
        ->whereYear('checkinDateTime', $currentYear)
        ->groupBy(DB::raw('YEAR(checkinDateTime), MONTH(checkinDateTime)'))
        ->orderBy('year')
        ->orderBy('month')
        ->get();

    $YearlyVisitorData = DB::table('visitorentryexitlogs')
        ->select(
            DB::raw('YEAR(checkinDateTime) AS year'),
            DB::raw('SUM(CASE WHEN visitor_type = "shortTerm" THEN 1 ELSE 0 END) AS short_term_count'),
            DB::raw('SUM(CASE WHEN visitor_type = "longTerm" THEN 1 ELSE 0 END) AS long_term_count')
        )
        ->whereRaw('YEAR(checkinDateTime) IS NOT NULL') 
        ->groupBy(DB::raw('YEAR(checkinDateTime)'))
        ->orderBy('year','desc')
        ->get();
    
     $visitorCheckInRecord = VisitorEntryExitLog::wherenull('checkOutDateTime')->paginate(10);
     return view('HR.dashboard',compact('longTermVisitorCheckIn','ShortTermVisitorCheckOut','visitorCheckInRecord','months', 'longTermData', 'shortTermData','currentYearVisitorData','YearlyVisitorData'));
    }





    public function visitorRecord()
    {
     $visitorCheckInRecord = VisitorEntryExitLog::wherenull('checkOutDateTime')->paginate(10);
     $visitorCheckInOutRecords = VisitorEntryExitLog::whereNotNull('checkInDateTime')
        ->whereNotNull('checkOutDateTime')
        ->orderByDesc('checkOutDateTime')
        ->paginate(10);
     return view('HR.visitor-record',compact('visitorCheckInRecord','visitorCheckInOutRecords'));
    }
    public function profileSetting()
    {
     return view('HR.Setting');
    }
}
