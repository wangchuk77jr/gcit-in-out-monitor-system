<?php

namespace App\Http\Controllers\backendPanel;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\StudentLeave;
use App\Models\StudentExitEntryLog;
use Illuminate\Support\Facades\DB;



class counsilorController extends Controller
{
    public function dashboard()
    {
            $totalstudent = Student::whereIn('studentYear', [1, 2, 3, 4])->count();
            $totalgirlnumber = Student::where('gender', 'female')->whereIn('studentYear', [1, 2, 3, 4])->count();
            $totalboynumber = Student::where('gender', 'male')->whereIn('studentYear', [1, 2, 3, 4])->count();
            $studentLeavebyBlock = DB::select("
                    SELECT
                    blocks.hotelBlock AS student_block,
                    COUNT(sl.leaveID) AS total_leaves
                    FROM
                        (
                        SELECT DISTINCT
                            hotelBlock
                        FROM
                            student
                        ) blocks
                    LEFT JOIN student s ON
                        blocks.hotelBlock = s.hotelBlock
                    LEFT JOIN studentleave sl ON
                        s.studentID = sl.studentID AND sl.LeaveApproval = 'accepted' AND sl.EndDate > CURRENT_DATE
                    GROUP BY
                        blocks.hotelBlock
                    HAVING
                        COUNT(sl.leaveID) > 0;
                                ");
            $studentOutofCampusbyBlock = DB::select("
                    SELECT
                    student.hotelBlock,
                    COUNT(DISTINCT student.studentID) as studentCount
                    FROM
                        student
                    JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
                    LEFT JOIN studentleave ON student.studentID = studentleave.studentID
                    WHERE
                        studentexitentrylogs.checkInDateTime IS NULL AND (
                            studentleave.LeaveApproval IS NULL OR (
                                studentleave.LeaveApproval = 'accepted' AND studentleave.EndDate <= CURDATE()
                            ) OR (
                                studentleave.LeaveApproval <> 'accepted' AND NOT EXISTS (
                                    SELECT 1
                                    FROM studentleave sl
                                    WHERE sl.studentID = student.studentID AND sl.LeaveApproval = 'accepted'
                                )
                            )
                        )
                    GROUP BY student.hotelBlock
                    ");
            $totalLeaveCount = DB::select("
                SELECT
                    COUNT(*) AS total_accepted_leaves,
                    SUM(CASE WHEN gender = 'male' THEN 1 ELSE 0 END) AS total_male_leaves,
                    SUM(CASE WHEN gender = 'female' THEN 1 ELSE 0 END) AS total_female_leaves
                FROM
                    studentleave
                LEFT JOIN
                    student ON studentleave.studentID = student.studentID
                WHERE
                    LeaveApproval = 'accepted'
                    AND EndDate >= CURDATE()
            ");
            
        
            $totalStudentOutofcampusByGender = Student::select(
                    DB::raw("'TotalStudentOutCampus' AS category"),
                    DB::raw("COUNT(DISTINCT student.studentID) AS totalCount"),
                    DB::raw("COUNT(DISTINCT CASE WHEN student.gender = 'male' THEN student.studentID END) AS maleCount"),
                    DB::raw("COUNT(DISTINCT CASE WHEN student.gender = 'female' THEN student.studentID END) AS femaleCount")
                )
                ->join('studentexitentrylogs', 'student.studentID', '=', 'studentexitentrylogs.studentID')
                ->leftJoin('studentleave', 'student.studentID', '=', 'studentleave.studentID')
                ->where(function ($query) {
                    $query->whereNull('studentexitentrylogs.checkInDateTime')
                        ->where(function ($query) {
                            $query->whereNull('studentleave.LeaveApproval')
                                ->orWhere(function ($query) {
                                    $query->where('studentleave.LeaveApproval', 'accepted')
                                        ->whereDate('studentleave.EndDate', '<=', now());
                                })
                                ->orWhere(function ($query) {
                                    $query->where('studentleave.LeaveApproval', '<>', 'accepted')
                                        ->whereNotExists(function ($query) {
                                            $query->select(DB::raw(1))
                                                ->from('studentleave as sl')
                                                ->whereColumn('sl.studentID', 'student.studentID')
                                                ->where('sl.LeaveApproval', 'accepted');
                                        });
                                });
                        });
                })
                ->groupBy('category')
                ->get();
            
            
        
            
        
        // Access the results using the ternary operator
        $totalCount = isset($totalStudentOutofcampusByGender[0]->totalCount) ? $totalStudentOutofcampusByGender[0]->totalCount : 0;
        $maleCount = isset($totalStudentOutofcampusByGender[0]->maleCount) ? $totalStudentOutofcampusByGender[0]->maleCount : 0;
        $femaleCount = isset($totalStudentOutofcampusByGender[0]->femaleCount) ? $totalStudentOutofcampusByGender[0]->femaleCount : 0;
        $totalAcceptedLeaves = isset($totalLeaveCount[0]->total_accepted_leaves) ? $totalLeaveCount[0]->total_accepted_leaves : 0;
        $totalMaleLeaves = isset($totalLeaveCount[0]->total_male_leaves) ? $totalLeaveCount[0]->total_male_leaves : 0;
        $totalFemaleLeaves = isset($totalLeaveCount[0]->total_female_leaves) ? $totalLeaveCount[0]->total_female_leaves : 0;



        // Create an associative array
        $dataCount = [
            'total' => $totalCount,
            'totalMale' => $maleCount,
            'totalFemale' =>  $femaleCount,
            'totalAcceptedLeaves' => $totalAcceptedLeaves,
            'totalMaleLeaves' => $totalMaleLeaves,
            'totalFemaleLeaves' => $totalFemaleLeaves,
        ];

        $studentByBlock = Student::select('hotelBlock', \DB::raw('COUNT(*) as total_students'))
        ->groupBy('hotelBlock')
        ->whereIn('studentYear', [1, 2, 3, 4])
        ->get();

     return view('counsilor.dashboard',compact('totalstudent','totalgirlnumber','totalboynumber',
     'studentLeavebyBlock','studentOutofCampusbyBlock','dataCount','studentByBlock',
     'totalStudentOutofcampusByGender',
    ));
    }

    public function studentLeave()
    {
        $loggedInUser = Auth::user();

        if ($loggedInUser->role == 'councilor') {
            $gender = $loggedInUser->gender;
        
            $studentLeaveRecord = studentLeave::with('student')
                ->whereHas('student', function ($query) use ($gender) {
                    $query->where('gender', $gender);
                })
                ->where('LeaveApproval', 'accepted')
                ->where(function ($query) {
                    $query->orWhereRaw('CURDATE() < studentleave.EndDate');
                })
                ->get();
        
            $totalNumberLeave = studentLeave::whereHas('student', function ($query) use ($gender) {
                    $query->where('gender', $gender);
                })
                ->where('LeaveApproval', 'accepted')
                ->whereDate('EndDate', '>=', now())
                ->count();

            $studentLeaveCountByBlock = DB::table(DB::raw('(SELECT DISTINCT hotelBlock FROM student WHERE gender = ?) as blocks'))
                ->select([
                    'blocks.hotelBlock AS student_block',
                    DB::raw('COALESCE(COUNT(sl.leaveID), 0) AS total_leaves'),
                ])
                ->addBinding($gender, 'select')
                ->leftJoin('student as s', 'blocks.hotelBlock', '=', 's.hotelBlock')
                ->leftJoin('studentleave as sl', function ($join) {
                    $join->on('s.studentID', '=', 'sl.studentID')
                        ->where('sl.LeaveApproval', '=', 'accepted')
                        ->where('sl.EndDate', '>', now());
                })
                ->groupBy('blocks.hotelBlock')
                ->get();

            $distinctHotelBlocks = Student::select('hotelBlock')->groupBy('hotelBlock')
            ->where('gender',$gender)
            ->get();

            return view('counsilor.studentLeave',
             [
                'studentLeaveRecord' => $studentLeaveRecord,
                'totalNumberLeave'=>$totalNumberLeave,
                'studentLeaveCountByBlock'=>$studentLeaveCountByBlock,
                'distinctHotelBlocks' =>$distinctHotelBlocks,
            ]);
         }
    }
    public function studentRecord()
    {
        $loggedInUser = Auth::user();
        if ($loggedInUser->role == 'councilor') {
            $gender = $loggedInUser->gender;
 
            $studentCheckoutRecord = DB::select("
            SELECT DISTINCT
                student.*, studentexitentrylogs.checkOutDateTime
            FROM
                student
            JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
            LEFT JOIN studentleave ON student.studentID = studentleave.studentID
            WHERE
                studentexitentrylogs.checkInDateTime IS NULL AND (
                    studentleave.LeaveApproval IS NULL OR (
                        studentleave.LeaveApproval = 'accepted' AND studentleave.EndDate <= CURDATE()
                    ) OR (
                        studentleave.LeaveApproval <> 'accepted' AND NOT EXISTS (
                            SELECT 1
                            FROM studentleave sl
                            WHERE sl.studentID = student.studentID AND sl.LeaveApproval = 'accepted'
                        )
                    )
                )
                AND student.gender = :gender;
        ", ['gender' => $gender]);
        

        $blockCounts = DB::select("
            SELECT
            student.hotelBlock,
            COUNT(DISTINCT student.studentID) as studentCount
            FROM
                student
            JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
            LEFT JOIN studentleave ON student.studentID = studentleave.studentID
            WHERE
                studentexitentrylogs.checkInDateTime IS NULL AND (
                    studentleave.LeaveApproval IS NULL OR (
                        studentleave.LeaveApproval = 'accepted' AND studentleave.EndDate <= CURDATE()
                    ) OR (
                        studentleave.LeaveApproval <> 'accepted' AND NOT EXISTS (
                            SELECT 1
                            FROM studentleave sl
                            WHERE sl.studentID = student.studentID AND sl.LeaveApproval = 'accepted'
                        )
                    )
                )
                AND student.gender = :gender
            GROUP BY student.hotelBlock;
            ", ['gender' => $gender]);
    
                
            $distinctHotelBlocks = Student::select('hotelBlock')->groupBy('hotelBlock')
                ->where('gender',$gender)
                ->get();
            
            return view('counsilor.studentRecord',[
                'studentCheckoutRecord'=>$studentCheckoutRecord,
                'blockCounts'=>$blockCounts,
                'distinctHotelBlocks' =>$distinctHotelBlocks,
        ]);
        }
    }
    public function setting()
    {
     return view('counsilor.setting');
    }
}
