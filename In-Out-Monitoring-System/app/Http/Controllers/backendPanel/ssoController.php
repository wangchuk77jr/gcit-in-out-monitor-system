<?php

namespace App\Http\Controllers\backendPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StudentLeave;
use Illuminate\Support\Facades\Auth;
use App\Models\StudentExitEntryLog;
use App\Models\Student;
use Notification;
use App\Notifications\studentLeaveNotification;
use Illuminate\Support\Facades\DB;



class ssoController extends Controller
{
    public function studentLeaveCount() {
        $loggedInUser = Auth::user();
        $nullApprovalCount = 0; // Initialize the variable outside the if block
    
        if ($loggedInUser->role == 'sso') {
            $gender = $loggedInUser->gender;
    
            $nullApprovalCount = DB::table('studentleave')
                ->join('student', 'studentleave.studentID', '=', 'student.studentID')
                ->where('student.gender', '=', $gender)
                ->whereNull('studentleave.leaveApproval')
                ->count();
        }
    
        return $nullApprovalCount;
    }
    
    public function dashboard()
    {
            $totalstudent = Student::whereIn('studentYear', [1, 2, 3, 4])->count();
            $totalgirlnumber = Student::where('gender', 'female')->whereIn('studentYear', [1, 2, 3, 4])->count();
            $totalboynumber = Student::where('gender', 'male')->whereIn('studentYear', [1, 2, 3, 4])->count();
            $studentLeavebyBlock = DB::select("
                        SELECT
                        blocks.hotelBlock AS student_block,
                        COUNT(sl.leaveID) AS total_leaves
                        FROM
                            (
                            SELECT DISTINCT
                                hotelBlock
                            FROM
                                student
                            ) blocks
                        LEFT JOIN student s ON
                            blocks.hotelBlock = s.hotelBlock
                        LEFT JOIN studentleave sl ON
                            s.studentID = sl.studentID AND sl.LeaveApproval = 'accepted' AND sl.EndDate > CURRENT_DATE
                        GROUP BY
                            blocks.hotelBlock
                        HAVING
                            COUNT(sl.leaveID) > 0;
                        ");
            $studentOutofCampusbyBlock = DB::select("
                    SELECT
                    student.hotelBlock,
                    COUNT(DISTINCT student.studentID) as studentCount
                    FROM
                        student
                    JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
                    LEFT JOIN studentleave ON student.studentID = studentleave.studentID
                    WHERE
                        studentexitentrylogs.checkInDateTime IS NULL AND (
                            studentleave.LeaveApproval IS NULL OR (
                                studentleave.LeaveApproval = 'accepted' AND studentleave.EndDate <= CURDATE()
                            ) OR (
                                studentleave.LeaveApproval <> 'accepted' AND NOT EXISTS (
                                    SELECT 1
                                    FROM studentleave sl
                                    WHERE sl.studentID = student.studentID AND sl.LeaveApproval = 'accepted'
                                )
                            )
                        )
                    GROUP BY student.hotelBlock
        
                    ");
            $totalLeaveCount = DB::select("
                SELECT
                    COUNT(*) AS total_accepted_leaves,
                    SUM(CASE WHEN gender = 'male' THEN 1 ELSE 0 END) AS total_male_leaves,
                    SUM(CASE WHEN gender = 'female' THEN 1 ELSE 0 END) AS total_female_leaves
                FROM
                    studentleave
                LEFT JOIN
                    student ON studentleave.studentID = student.studentID
                WHERE
                    LeaveApproval = 'accepted'
                    AND EndDate >= CURDATE()
            ");
            
            
            $totalStudentOutofcampusByGender = Student::select(
                DB::raw("'TotalStudentOutCampus' AS category"),
                DB::raw("COUNT(DISTINCT student.studentID) AS totalCount"),
                DB::raw("COUNT(DISTINCT CASE WHEN student.gender = 'Male' THEN student.studentID END) AS maleCount"),
                DB::raw("COUNT(DISTINCT CASE WHEN student.gender = 'Female' THEN student.studentID END) AS femaleCount")
            )
            ->join('studentexitentrylogs', 'student.studentID', '=', 'studentexitentrylogs.studentID')
            ->leftJoin('studentleave', 'student.studentID', '=', 'studentleave.studentID')
            ->where(function ($query) {
                $query->whereNull('studentexitentrylogs.checkInDateTime')
                    ->where(function ($query) {
                        $query->whereNull('studentleave.LeaveApproval')
                            ->orWhere(function ($query) {
                                $query->where('studentleave.LeaveApproval', 'accepted')
                                    ->whereDate('studentleave.EndDate', '<=', now());
                            })
                            ->orWhere(function ($query) {
                                $query->where('studentleave.LeaveApproval', '<>', 'accepted')
                                    ->whereNotExists(function ($query) {
                                        $query->select(DB::raw(1))
                                            ->from('studentleave as sl')
                                            ->whereColumn('sl.studentID', 'student.studentID')
                                            ->where('sl.LeaveApproval', 'accepted');
                                    });
                            });
                    });
            })
            ->groupBy('category')
            ->get();
        
        // Access the results with null coalescing operator
        $total = isset($totalStudentOutofcampusByGender[0]->totalCount) ? $totalStudentOutofcampusByGender[0]->totalCount : 0;
        $totalMale = isset($totalStudentOutofcampusByGender[0]->maleCount) ? $totalStudentOutofcampusByGender[0]->maleCount : 0;
        $totalFemale = isset($totalStudentOutofcampusByGender[0]->femaleCount) ? $totalStudentOutofcampusByGender[0]->femaleCount : 0;
        $totalAcceptedLeaves = isset($totalLeaveCount[0]->total_accepted_leaves) ? $totalLeaveCount[0]->total_accepted_leaves : 0;
        $totalMaleLeaves = isset($totalLeaveCount[0]->total_male_leaves) ? $totalLeaveCount[0]->total_male_leaves : 0;
        $totalFemaleLeaves = isset($totalLeaveCount[0]->total_female_leaves) ? $totalLeaveCount[0]->total_female_leaves : 0;


        // Create an associative array
        $dataCount = [
            'total' => $total,
            'totalMale' => $totalMale,
            'totalFemale' => $totalFemale,
            'totalAcceptedLeaves' => $totalAcceptedLeaves,
            'totalMaleLeaves' => $totalMaleLeaves,
            'totalFemaleLeaves' => $totalFemaleLeaves,
        ];

        $nullApprovalCount = $this->studentLeaveCount();
        
        $studentByBlock = Student::select('hotelBlock', \DB::raw('COUNT(*) as total_students'))
        ->groupBy('hotelBlock')
        ->whereIn('studentYear', [1, 2, 3, 4])
        ->get();
            
            
      return view('sso.dashboard',compact('totalstudent','totalgirlnumber','totalboynumber','studentLeavebyBlock','studentOutofCampusbyBlock','dataCount','nullApprovalCount','studentByBlock'));
    }

    

    // student record
    public function studentLeave()
    {
        $loggedInUser = Auth::user();

        if ($loggedInUser->role == 'sso') {
            $gender = $loggedInUser->gender;
        
            $studentLeaveRecord = studentLeave::with('student')
                ->whereHas('student', function ($query) use ($gender) {
                    $query->where('gender', $gender);
                })
                ->orderByRaw('ISNULL(LeaveApproval) DESC, created_at DESC')
                ->get();
            
            $totalNumberLeave = studentLeave::whereHas('student', function ($query) use ($gender) {
                    $query->where('gender', $gender);
                })
                ->where('LeaveApproval', 'accepted')
                ->whereDate('EndDate', '>=', now())
                ->count();

            $year = now()->year;

            $leaveCounts = StudentLeave::select(
                DB::raw("MONTH(studentleave.created_at) AS month"),
                DB::raw("DATE_FORMAT(studentleave.created_at, '%b') AS month_name"),
                DB::raw("COUNT(*) AS total_leaves"),
                DB::raw("SUM(CASE WHEN studentleave.leaveApproval = 'Accepted' or studentleave.leaveApproval = 'LeaveEnded' THEN 1 ELSE 0 END) AS accepted_leaves"),
                DB::raw("SUM(CASE WHEN studentleave.leaveApproval = 'Declined' THEN 1 ELSE 0 END) AS declined_leaves")
            )
            ->join('student', 'studentleave.studentID', '=', 'student.studentID')
            ->where('student.gender', $gender)
            ->whereYear('studentleave.created_at', $year)
            ->groupBy(DB::raw("MONTH(studentleave.created_at)"), DB::raw("month_name"))
            ->orderBy(DB::raw("MONTH(studentleave.created_at)"))
            ->get();
        

            // Prepare the data for the chart
            $appliedLeaveData = $leaveCounts->pluck('total_leaves')->toArray();
            $acceptedLeaveData = $leaveCounts->pluck('accepted_leaves')->toArray();
            $declinedLeaveData = $leaveCounts->pluck('declined_leaves')->toArray();
            $months = $leaveCounts->pluck('month')->toArray();


            // Get the start and end dates of the current week
            $startWeek = now()->startOfWeek();
            $endWeek = now()->endOfWeek();

            // Query to get the desired information with gender-based filtering
            $weekData = StudentLeave::join('student', 'studentleave.studentID', '=', 'student.studentID')
                ->where('student.gender', $gender)
                ->whereBetween('studentleave.created_at', [$startWeek, $endWeek])
                ->selectRaw('CURDATE() - INTERVAL WEEKDAY(CURDATE()) DAY AS startWeek')
                ->selectRaw('CURDATE() + INTERVAL 6 - WEEKDAY(CURDATE()) DAY AS endWeek')
                ->selectRaw('COUNT(*) as totalLeave')
                ->selectRaw('SUM(CASE WHEN LeaveApproval IS NULL THEN 1 ELSE 0 END) as pendingLeave')
                ->first();

                // Initialize variables with default values
                $startWeek = $weekData->startWeek ?? '';
                $endWeek = $weekData->endWeek ?? '';
                $totalLeave = $weekData->totalLeave ?? 0;
                $pendingLeave = $weekData->pendingLeave ?? 0;

            // get unique values of student block
            $distinctHotelBlocks = Student::select('hotelBlock')->groupBy('hotelBlock')->get();
            $distinctYear = Student::select('studentYear')->groupBy('studentYear')->get();
            $distinctCourse = Student::select('studentCourse')->groupBy('studentCourse')->get();

            $nullApprovalCount = $this->studentLeaveCount();


            return view('sso.studentLeave', [
                'studentLeaveRecord' => $studentLeaveRecord,
                'totalNumberLeave' => $totalNumberLeave,
            ])->with(compact('appliedLeaveData', 'acceptedLeaveData', 'declinedLeaveData', 'months','startWeek', 'endWeek', 'totalLeave', 'pendingLeave','distinctHotelBlocks','distinctYear','distinctCourse','nullApprovalCount'));

        } else {
            return view('sso.dashboard');
        }
    }
    // student leave approval
    public function studentLeaveApproval(Request $request)
    {
        $leaveId = $request->leaveID;
        $leaveApproval = $request->leaveApproval;
        $studentID = $request->studentID;
        $mailMessage = $request->message;
        $emailto = $studentID.'.gcit@rub.edu.bt';
        if($leaveApproval === 'accepted'){
            $statusmessage = 'Your Leave Request has been Approved!';
        }else{
            $statusmessage = 'Your leave request has been declined. The reason is:';
        }
        $message = [
            'statusmessage'=>$statusmessage,
            'message'=>$mailMessage,
        ];

        // Check if the leaveId exists in the database
        $studentLeave = StudentLeave::where('leaveID', $leaveId)->first();

        if (!$studentLeave) {
            return redirect()->back()->with('error', 'student not found.');
        }

        if ($studentLeave->LeaveApproval === null) {
            // Update Approval if it is null
            $studentLeave->LeaveApproval = $leaveApproval;
            $studentLeave->save();
            if ($leaveApproval === 'accepted'){
                Notification::route('mail', $emailto)->notify(new studentLeaveNotification($message));
                return redirect()->back()->with('success', "Student ID ".$studentID. "'s leave request has been successfully approved.");
            }else{
                Notification::route('mail', $emailto)->notify(new studentLeaveNotification($message));
                return redirect()->back()->with('success', "Student ID ".$studentID. "'s leave request has been successfully declined.");
            }

        } else {
            return redirect()->back()->with('error', "error occurred while accepting or declining the leave request");
        }
    }

    // student checkin and checkout record
    public function studentRecord()
    {
        $loggedInUser = Auth::user();

        if ($loggedInUser->role == 'sso') {
            $gender = $loggedInUser->gender;
            $studentCheckoutRecord = DB::select("
            SELECT DISTINCT
                student.*, studentexitentrylogs.checkOutDateTime
            FROM
                student
            JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
            LEFT JOIN studentleave ON student.studentID = studentleave.studentID
            WHERE
                studentexitentrylogs.checkInDateTime IS NULL AND (
                    studentleave.LeaveApproval IS NULL OR (
                        studentleave.LeaveApproval = 'accepted' AND studentleave.EndDate <= CURDATE()
                    ) OR (
                        studentleave.LeaveApproval <> 'accepted' AND NOT EXISTS (
                            SELECT 1
                            FROM studentleave sl
                            WHERE sl.studentID = student.studentID AND sl.LeaveApproval = 'accepted'
                        )
                    )
                )
                AND student.gender = :gender;
        ", ['gender' => $gender]);

        $studentCheckoutRecordOnleave = DB::select("
                SELECT DISTINCT
                    student.*,
                    studentexitentrylogs.checkOutDateTime,studentleave.EndDate,studentleave.Reason
                FROM
                    student
                JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
                JOIN studentleave ON student.studentID = studentleave.studentID
                WHERE
                    studentexitentrylogs.checkInDateTime IS NULL AND
                    studentleave.LeaveApproval = 'accepted' AND
                    studentleave.EndDate > CURDATE()
                    AND student.gender = :gender;
            ", ['gender' => $gender]);
            
        

        $studentExitEntryRecord = StudentExitEntryLog::with('student')
                    ->whereHas('student', function ($query) use ($gender) {
                        $query->where('gender', $gender);
                    })
                    ->whereNotNull('checkInDateTime')
                    ->orderByDesc('checkInDateTime')  
                    ->orderBy('checkOutDateTime', 'desc')
                    ->paginate(5);
                
            $checkInOutCounts = DB::table(DB::raw('(SELECT STR_TO_DATE(checkInDateTime, "%Y-%m-%d %h:%i %p") AS checkDateTime, "Check In" AS checkType, studentID FROM studentexitentrylogs WHERE checkInDateTime IS NOT NULL UNION ALL SELECT STR_TO_DATE(checkOutDateTime, "%Y-%m-%d %h:%i %p") AS checkDateTime, "Check Out" AS checkType, studentID FROM studentexitentrylogs WHERE checkOutDateTime IS NOT NULL) AS combined_check_data'))
                ->join('student', 'combined_check_data.studentID', '=', 'student.studentID')
                ->where('student.gender', '=', $gender) // Filter by gender 'male'
                ->select(
                    'student.gender',
                    DB::raw('DATE(combined_check_data.checkDateTime) AS checkDate'),
                    DB::raw('COUNT(CASE WHEN combined_check_data.checkType = "Check In" THEN 1 END) AS checkin_count'),
                    DB::raw('COUNT(CASE WHEN combined_check_data.checkType = "Check Out" THEN 1 END) AS checkout_count')
                )
                ->groupBy('student.gender', 'checkDate')
                ->orderBy('checkDate', 'desc')
                ->get();

            $nullApprovalCount = $this->studentLeaveCount();
            $distinctHotelBlocks = Student::select('hotelBlock')->groupBy('hotelBlock')
            ->where('gender',$gender)
            ->get();

            return view('sso.studentRecord',[
                'studentCheckoutRecord'=>$studentCheckoutRecord,
                'studentExitEntryRecord'=>$studentExitEntryRecord,
                'checkInOutCounts' => $checkInOutCounts,
                'nullApprovalCount' => $nullApprovalCount,
                'distinctHotelBlocks' =>$distinctHotelBlocks,
                'studentCheckoutRecordOnleave' =>$studentCheckoutRecordOnleave,
            ]);
        }
    }
    public function setting()
    {
    $nullApprovalCount = $this->studentLeaveCount();
     return view('sso.setting',compact('nullApprovalCount'));
    } 
    
}
