<?php

namespace App\Http\Controllers\backendPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Student;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Notification;
use App\Notifications\gcitInOutNewUserPassword;
use App\Imports\StudentDataImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class adminController extends Controller
{
   public function dashboard()
   {
    $roleTotals = User::select(
        'role',
        DB::raw('SUM(CASE WHEN gender = "male" THEN 1 ELSE 0 END) AS male_total'),
        DB::raw('SUM(CASE WHEN gender = "female" THEN 1 ELSE 0 END) AS female_total'),
        DB::raw('COUNT(*) AS role_total')
    )
    ->where('role', '<>', 'admin')
    ->groupBy('role')
    ->get();
    // Convert the result to an array
    $chartData = [];
    foreach ($roleTotals as $roleTotal) {
        $chartData[$roleTotal->role] = [
            'male_total' => $roleTotal->male_total,
            'female_total' => $roleTotal->female_total,
            'role_total' => $roleTotal->role_total,
            'role' => $roleTotal->role,
        ];
    }
    $totalstudent = Student::whereIn('studentYear', [1, 2, 3, 4])->count();
    $totalgirlnumber = Student::where('gender', 'female')->whereIn('studentYear', [1, 2, 3, 4])->count();
    $totalboynumber = Student::where('gender', 'male')->whereIn('studentYear', [1, 2, 3, 4])->count();

    return view('admin.dashboard',compact('totalstudent','totalgirlnumber','totalboynumber','chartData'));
   }
   public function user(Request $request)
   {
    $searchTerm = $request->input('search');

    // Query the users table and apply the search filter if a search term is provided
    $query = User::where('role', '!=', 'admin');
    
    if ($searchTerm) {
        $query->where(function ($query) use ($searchTerm) {
            $query->where('name', 'like', '%' . $searchTerm . '%')
                ->orWhere('role', 'like', '%' . $searchTerm . '%');
        });
    }

    $users = $query->get();

    $roleCounts = User::where('role', '<>', 'admin')
        ->groupBy('role')
        ->select('role', DB::raw('count(*) as count'))
        ->get();
    return view('admin.users',compact('users','roleCounts'));
   }
   
   public function student(Request $request)
   {
    $year1Students = Student::where('studentYear', 1)
                    ->orderBy('studentID', 'asc')
                    ->get();

    $year2Students = Student::where('studentYear', 2)
                    ->orderBy('studentID', 'asc')
                    ->get();
    $year3Students = Student::where('studentYear', 3)
                    ->orderBy('studentID', 'asc')
                    ->get();
    $year4Students = Student::where('studentYear', 4)
                    ->orderBy('studentID', 'asc')
                    ->get();
                    
    $yearCounts = [
                    'year1' => Student::where('studentYear', 1)->count(),
                    'year2' => Student::where('studentYear', 2)->count(),
                    'year3' => Student::where('studentYear', 3)->count(),
                    'year4' => Student::where('studentYear', 4)->count(),
                    ];
                    

      return view('admin.students', compact('year1Students', 'year2Students', 'year3Students', 'year4Students','yearCounts'));
   }

   public function AddUsers(Request $request){
        // Define validation rules
        $rules = [
            'userRole' => 'required',
            'userGender' => 'required',
            'email' => 'required|email',
            'userName' => 'required',
        ];

        // Define custom error messages
        $messages = [
            'userRole.required' => 'User Role is required.',
            'userGender.required' => 'User Gender is required.',
            'email.required' => 'Email is required.',
            'email.email' => 'Please provide a valid email address.',
            'userName.required' => 'User Name is required.',
        ];

        // Validate the request
        $validator = Validator::make($request->all(), $rules, $messages);

        // Check if validation fails
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
      }

    $specialChars = '!@#$%^&*()_-+=<>?';
    $length = 20; //length of the random string
    
    // Generate a random alphanumeric string
    $randomAlphaNumeric = Str::random($length - 2);
    
    // Select one or two random special characters
    $selectedSpecialChars = substr(str_shuffle($specialChars), 0, rand(1, 4));
    
    // Combine the random alphanumeric string and selected special characters
    $password = $randomAlphaNumeric . $selectedSpecialChars;

    $hashedPassword = bcrypt($password);

    $UserType = $request->input('userRole');
    $UserGender = $request->input('userGender');
    $UserEmail = $request->input('email');
    $UserName = $request->input('userName');

    $studentPaswordEmail = [
        'email' => $UserEmail,
        'password' => $password,
    ];

    $existingUser = User::where('email', $UserEmail)->first();

    if (!$existingUser) {
        $addUser = new User();
        $addUser->name = $UserName;
        $addUser->password = $hashedPassword;
        $addUser->role = $UserType;
        $addUser->email = $UserEmail;
        $addUser->gender = $UserGender;
    
        $addUser->save();
        Notification::route('mail', $UserEmail)->notify(new gcitInOutNewUserPassword($studentPaswordEmail));
        return redirect('admin/users')->with('success', "User has been created successfully");
    } else {
        // Email already registered, so show an error message.
        return redirect()->back()->with('error', "This email (".$UserEmail.") is already registered");
    }
   }

   public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        $UserType = $request->input('userRole');
        $UserEmail = $request->input('email');
        $UserName = $request->input('userName');
        
        if ($user) {
            $changes = false; // Initialize a flag to track changes
        
            // Check if there are changes in user data
            if ($user->name !== $UserName) {
                $user->name = $UserName;
                $changes = true;
            }
            if ($user->role !== $UserType) {
                $user->role = $UserType;
                $changes = true;
            }
        
            if ($user->email !== $UserEmail) {
                // Check if the new email already exists in the database
                $existingUser = User::where('email', $UserEmail)->first();
                if ($existingUser && $existingUser->id !== $user->id) {
                    return redirect()->back()->with('error', "Email already exists for another user");
                }
        
                $user->email = $UserEmail;
                $changes = true;
            }
        
            if ($changes) {
                $user->save();
                return redirect()->back()->with('success', "User has been updated successfully");
            } else {
                return redirect()->back()->with('success', "Nothing to update. User data remains the same.");
            }
        } else {
            return redirect()->back()->with('error', "User not found");
        }
        
    }
   
   public function deleteUser(Request $request)
    {
        $UserEmail = $request->input('email');

        // Find the user with the given email
        $existingUser = User::where('email', $UserEmail)->first();

        if ($existingUser) {
            // If the user exists, you can delete them
            $existingUser->delete();

            return redirect()->back()->with('success', "User has been deleted successfully");
        } else {
            return redirect()->back()->with('error', "User with email " . $UserEmail . " not found");
        }
    }


   public function studentRecord(Request $request)
   {
       try {
           $profileImage = time().'.'.$request->profileImage->extension();
           $request->profileImage->move(public_path('studentProfile'), $profileImage);
   
           $student = new Student;
           $student->studentID = $request->studentID;
           $student->studentName = $request->studentName;
           $student->studentYear = $request->studentYear;
           $student->studentCourse = $request->studentCourse;
           $student->hotelBlock = $request->hotelBlock;
           $student->gender = $request->gender;
           $student->phoneNumber = $request->phoneNumber;
           $student->profileImage = $profileImage;
           $student->save();
   
           // Success message
           return redirect('admin/students')->with('success', 'Student record has been added successfully.');
       } catch (\Exception $e) {
           // Error message
           return redirect('admin/dashboard')->with('error', 'An error occurred while adding the student record.');
       }
   }

    public function importStudentExcel(Request $request)
        {
            if ($request->hasFile('excel_file')) {
                $file = $request->file('excel_file');
                $import = new StudentDataImport();

                try {
                    Excel::import($import, $file);

                    $updatedCount = $import->getUpdatedCount();
                    $newCount = $import->getNewCount();
                    $skippedCount = $import->getSkippedCount();

                    $message = "Updated $updatedCount student(s), added $newCount new student(s), and skipped $skippedCount invalid record(s).";

                    return redirect('admin/students')->with('success', $message);
                } catch (ValidationException $e) {
                    $errorMessages = $e->validator->errors()->get('error');
                    return redirect('admin/students')->with('error', $errorMessages[0]);
                } catch (\Exception $e) {
                    return redirect('admin/students')->with('error', 'An unknown error occurred during the import.');
                }
            } else {
                return "No file uploaded.";
            }
        }
        
   public function editStudent(Request $request)
   {
       try {
           $studentID = $request->input('studentID');
           $student = Student::find($studentID);
           if (!$student) {
               return redirect('admin/students')->with('error', 'Student not found.');
           }
        
        // Check if a new profile image was uploaded
        if ($request->hasFile('profileImage')) {
            // Delete the old profile image if it exists
            $oldProfileImage = public_path('studentProfile/' . $student->profileImage);

            if (file_exists($oldProfileImage) && is_file($oldProfileImage)) {
                unlink($oldProfileImage); // Delete the old profile image file
            }

            $profileImage = time() . '.' . $request->profileImage->extension();
            $request->profileImage->move(public_path('studentProfile'), $profileImage);
            $student->profileImage = $profileImage;
        }

   
           // Update other fields
           $student->studentName = $request->studentName;
           $student->studentYear = $request->studentYear;
           $student->studentCourse = $request->studentCourse;
           $student->hotelBlock = $request->hotelBlock;
           $student->gender = $request->gender;
           $student->phoneNumber = $request->phoneNumber;
           $student->save();
   
           // Success message
           return redirect('admin/students')->with('success', 'Student record has been updated successfully.');
       } catch (\Exception $e) {
           // Error message with a specific error description
           return redirect('admin/students')->with('error', 'An error occurred while updating the student record: ' . $e->getMessage());
       }
   }
   
   public function deleteStudent(Request $request)
   {
       $studentID = $request->input('studentID');
       // Find the user with the given email
       $existingStudentID = Student::where('studentID', $studentID)->first();

       if ($existingStudentID) {
           // If the user exists, you can delete them
           $existingStudentID->delete();

           return redirect()->back()->with('success', "Student ID ".$studentID . " has been deleted successfully");
       } else {
           return redirect()->back()->with('error', "Student with Student ID " . $studentID . " not found");
       }
   }


//    public function updateStudentProfileImages(Request $request)
//    {
//        try {
//            $images = $request->file('images');
//            $successfulUploads = 0;
//            $ignoredImages = 0;
   
//            foreach ($images as $image) {
//                // Check if the file extension is one of the allowed formats (jpg, png, jpeg)
//                $allowedExtensions = ['jpg', 'jpeg', 'png'];
//                $extension = strtolower($image->getClientOriginalExtension());
   
//                if (in_array($extension, $allowedExtensions)) {
//                    $filename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME); // Get the original filename without extension
//                    $filename = str_replace('-removebg-preview', '', $filename); // Remove '-removebg-preview'
   
//                    // Find the student by ID
//                    $student = Student::where('studentID', $filename)->first();
   
//                    if ($student) {
//                        // Update the student's profile image
//                        $oldProfileImage = public_path('studentProfile/' . $student->profileImage);
   
//                        if (file_exists($oldProfileImage) && is_file($oldProfileImage)) {
//                            unlink($oldProfileImage);
//                        }
   
//                        $newProfileImage = $filename . '.jpg'; // You can specify the desired extension
//                        $image->move(public_path('studentProfile'), $newProfileImage);
//                        $student->profileImage = $newProfileImage;
//                        $student->save();
//                        $successfulUploads++;
//                    }
//                } else {
//                    $ignoredImages++;
//                }
//            }
   
//            $successMessage = 'Students profile images updated successfully. Uploaded: ' . $successfulUploads . ' | Ignored: ' . $ignoredImages;
   
//            return redirect('admin/students')->with('success', $successMessage);
//        } catch (Exception $e) {
//            return redirect('admin/students')->with('error', 'An error occurred while updating student profile images.');
//        }
//    }
   
    public function updateStudentProfileImages(Request $request)
    {
        try {
            $images = $request->file('images');
            $successfulUploads = 0;
            $ignoredImages = 0;

            foreach ($images as $image) {
                // Check if the file extension is one of the allowed formats (jpg, jpeg, png)
                $allowedExtensions = ['jpg', 'jpeg', 'png'];
                $extension = strtolower($image->getClientOriginalExtension());

                if (in_array($extension, $allowedExtensions)) {
                    $filename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME); // Get the original filename without extension
                    $filename = str_replace('-removebg-preview', '', $filename); // Remove '-removebg-preview'

                    // Find the student by ID
                    $student = Student::where('studentID', $filename)->first();

                    if ($student) {
                        // Update the student's profile image
                        $oldProfileImage = public_path('studentProfile/' . $student->profileImage);

                        if (file_exists($oldProfileImage) && is_file($oldProfileImage)) {
                            unlink($oldProfileImage);
                        }

                        $newProfileImage = $filename . '.jpg'; // You can specify the desired extension
                        $image->move(public_path('studentProfile'), $newProfileImage);
                        $student->profileImage = $newProfileImage;
                        $student->save();
                        $successfulUploads++;
                    } else {
                        // If the student with that ID doesn't exist, count it as an ignored image
                        $ignoredImages++;
                    }
                } else {
                    $ignoredImages++; // If the extension is not allowed, count it as an ignored image
                }
            }

            $successMessage = 'Students profile images updated successfully. Uploaded: ' . $successfulUploads . ' | Ignored: ' . $ignoredImages;

            return response()->json(['success' => true, 'message' => $successMessage]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'message' => 'An error occurred while updating student profile images.']);
        }
    }

    public function updateStudentYear(Request $request) {
        // Get the input studentYear from the request.
        $studentYear = $request->input('studentYear');

        // Check if there are students with the selected studentYear.
        $studentYearUpdate = Student::where('studentYear', $studentYear)->get();

        if ($studentYearUpdate->isEmpty()) {
            return redirect()->back()->with('error', 'No students found for the selected year.');
        }

        // Define an array to map studentYear updates.
        $yearUpdates = [1 => 2, 2 => 3, 3 => 4, 4 => date('Y')];

        // Check if the provided studentYear is within the defined range.
        if (array_key_exists($studentYear, $yearUpdates)) {
            $newYear = $yearUpdates[$studentYear];

            // Update student years in the database.
            foreach ($studentYearUpdate as $student) {
                $student->studentYear = $newYear;
                $student->save();
            }

            if ($studentYear === strval(4)) {
                $message = "Students of year 4 have graduated.";
            } else {
                $message = "Students of year $studentYear have been updated successfully to $newYear year.";
            }

            return redirect()->back()->with('success', $message);
        } else {
            return redirect()->back()->with('error', 'Invalid studentYear provided.');
        }
    }

    public function deleteStudentsByYear(Request $request) {
        // Get the input studentYear from the request.
        $studentYear = $request->input('studentYear');
    
        // Check if there are students with the selected studentYear.
        $studentsToDelete = Student::where('studentYear', $studentYear)->get();
    
        if ($studentsToDelete->isEmpty()) {
            return redirect()->back()->with('error', 'No students found for the selected year.');
        }
    
        // Attempt to delete the students with proper error handling.
        try {    
            $studentsToDelete->each(function ($student) {
                $student->delete();
            });
    
            return redirect()->back()->with('success', "All students of year $studentYear have been deleted.");
        } catch (\Exception $e) {    
            return redirect()->back()->with('error', 'An error occurred while deleting students.' . $e->getMessage());
        }
    }
    

   
   public function setting()
   {
    return view('admin.setting');
   }
}
