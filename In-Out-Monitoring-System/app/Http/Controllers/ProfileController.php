<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use App\Models\User;
use Illuminate\Support\Facades\Validator;


class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }

    public function updateProfile(Request $request) {
        $rules = [
            'userName' => 'required',
            'email' => 'required|email|unique:users,email,' . auth()->id(),
            'profileImage' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    
        $messages = [
            'userName.required' => 'User Name is required.',
            'email.required' => 'Email is required.',
            'email.email' => 'Please provide a valid email address.',
            'email.unique' => 'Email address is already in use.',
            'profileImage.image' => 'Please upload a valid image (jpeg, png, jpg, gif).',
            'profileImage.mimes' => 'The image must be a jpeg, png, jpg, or gif file.',
            'profileImage.max' => 'The image size should not exceed 2MB.',
        ];
    
        $validator = Validator::make($request->all(), $rules, $messages);
    
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
    
        $existingUser = User::find(auth()->id());
    
        if (!$existingUser) {
            return redirect()->back()->with('error', "User not found");
        }
    
        $existingUser->name = $request->input('userName');
    
        $newEmail = $request->input('email');
        
        if ($existingUser->email !== $newEmail) {
            // Email is being updated, validate uniqueness
            $isUnique = User::where('email', $newEmail)->where('id', '<>', $existingUser->id)->count() === 0;
            if (!$isUnique) {
                return redirect()->back()->with('error', "This email (" . $newEmail . ") is already registered");
            }
            $existingUser->email = $newEmail;
        }
    
        if ($request->hasFile('profileImage')) {
            // Handle image upload
            $image = $request->file('profileImage');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('profile_images'), $imageName);
    
            // Update the user's profile image
            $existingUser->profile_image = $imageName;
        }
    
        $existingUser->save();
    
        return redirect()->back()->with('success', "Profile has been updated successfully");
    }
    public function ProfileSetting()
    {
     return view('security-guard.setting');
    }
}
