<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
 
     protected function schedule(Schedule $schedule)
     {
         $schedule->command('visitor:update-expired-approvals')->everyMinute();
         $schedule->command('app:send-daily-student-report')->everyMinute();
         $schedule->command('app:send-daily-visitor-not-check-out')->everyMinute();

     }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}


// php artisan schedule:run
// php artisan visitor:update-expired-approvals