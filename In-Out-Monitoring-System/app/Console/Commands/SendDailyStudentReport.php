<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Student;
use App\Models\StudentExitEntryLog;
use App\Models\StudentLeave;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\DailyStudentReport;
use App\Mail\VisitorNotCheckOut;
use Carbon\Carbon;


class SendDailyStudentReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-daily-student-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
            // Fetch names of students who are out of campus
            $outOfCampusStudentsMale = DB::select('
            SELECT DISTINCT
            student.*
            FROM
                student
            JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
            LEFT JOIN studentleave ON student.studentID = studentleave.studentID
            WHERE
                studentexitentrylogs.checkInDateTime IS NULL AND
                (
                    studentleave.LeaveApproval IS NULL OR
                    (
                        studentleave.LeaveApproval = "accepted" AND studentleave.EndDate <= CURDATE()
                    ) OR
                    (
                        studentleave.LeaveApproval <> "accepted" AND NOT EXISTS (
                            SELECT 1
                            FROM studentleave sl
                            WHERE sl.studentID = student.studentID AND sl.LeaveApproval = "accepted"
                        )
                    )
                ) AND student.gender = "Male";
            ');

            $outOfCampusStudentsFemale = DB::select('
            SELECT DISTINCT
            student.*
            FROM
                student
            JOIN studentexitentrylogs ON student.studentID = studentexitentrylogs.studentID
            LEFT JOIN studentleave ON student.studentID = studentleave.studentID
            WHERE
                studentexitentrylogs.checkInDateTime IS NULL AND
                (
                    studentleave.LeaveApproval IS NULL OR
                    (
                        studentleave.LeaveApproval = "accepted" AND studentleave.EndDate <= CURDATE()
                    ) OR
                    (
                        studentleave.LeaveApproval <> "accepted" AND NOT EXISTS (
                            SELECT 1
                            FROM studentleave sl
                            WHERE sl.studentID = student.studentID AND sl.LeaveApproval = "accepted"
                        )
                    )
                ) AND student.gender = "Female";
            ');
               // Send email to users
               $usersMale = User::where(function ($query) {
                    $query->where('role', 'councilor')
                        ->orWhere('role', 'sso');
                        })
                    ->where('gender', 'Male')
                    ->get();

                $usersFemale = User::where(function ($query) {
                    $query->where('role', 'councilor')
                        ->orWhere('role', 'sso');
                        })
                    ->where('gender', 'Female')
                    ->get();

                // For Male Users
                if (count($outOfCampusStudentsMale) > 0) {
                    foreach ($usersMale as $user) {
                        Mail::to($user->email)->send(new DailyStudentReport($outOfCampusStudentsMale, $user));
                    }
                }

                // For Female Users
                if (count($outOfCampusStudentsFemale) > 0) {
                    foreach ($usersFemale as $user) {
                        Mail::to($user->email)->send(new DailyStudentReport($outOfCampusStudentsFemale, $user));
                    }
                }

               $this->info('Daily student report emails sent successfully.');
           

    }
}
