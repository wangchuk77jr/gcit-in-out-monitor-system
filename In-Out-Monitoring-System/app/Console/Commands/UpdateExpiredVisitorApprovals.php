<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\visitorLog;
use Carbon\Carbon;
use App\Models\StudentLeave;

class UpdateExpiredVisitorApprovals extends Command
{
    protected $signature = 'visitor:update-expired-approvals';

    protected $description = 'Update approval status for visitor entry forms with expired end dates';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $visitorForms = visitorLog::where(function ($query) {
            $query->where('Visitor_Approval', 'accepted')
                ->orWhereNull('Visitor_Approval');
        })
        ->whereDate('end_date', '<', now())
        ->get();

        $studentLeave = StudentLeave::where(function ($query) {
            $query->where('LeaveApproval', 'accepted')
                ->orWhereNull('LeaveApproval');
        })
        ->whereDate('EndDate', '<', now())
        ->get();
        
        foreach ($studentLeave as $s) {
            $s->LeaveApproval = 'leaveEnded';
            $s->save();
        }
        
        $this->info('Visitor approvals updated successfully.');        
    }
}
