<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Student;
use App\Models\StudentExitEntryLog;
use App\Models\visitorLog;
use App\Models\StudentLeave;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\DailyVisitorNotCheckOut;
use Carbon\Carbon;

class SendDailyVisitorNotCheckOut extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-daily-visitor-not-check-out';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $visitorInTheCampus = DB::select("
            SELECT ve.*
            FROM visitorentryexitlogs ve
            LEFT JOIN visitor_log vl ON ve.visitorCID = vl.visitorCID
            WHERE ve.checkInDateTime IS NOT NULL
            AND ve.checkOutDateTime IS NULL
            AND (
                ve.visitor_type = 'shortTerm'
                OR (ve.visitor_type = 'longTerm' AND vl.Visitor_Approval = 'accepted' AND (vl.end_date IS NULL OR vl.end_date >= CURRENT_DATE))
            );
        ");
           // Send email to users
           $hrUsers = User::where('role', 'HR')->get();

            // For Male Users
            if (count($visitorInTheCampus) > 0) {
                foreach ($hrUsers as $user) {
                    Mail::to($user->email)->send(new DailyVisitorNotCheckOut($visitorInTheCampus, $user));
                }
            }
            $this->info('Visitor not checkout send notification successfully.');  
    }
}
