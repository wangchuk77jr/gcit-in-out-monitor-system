<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('visitorentryexitlogs', function (Blueprint $table) {
            $table->id('visitorEntryExitID');
            $table->string('checkInDateTime')->nullable();
            $table->string('checkOutDateTime')->nullable();
            $table->string('visitorCID');
            $table->string('visitor_type');
            $table->string('dependentName');
            $table->string('visitorPassID');
            $table->string('v_phoneNumber');
            $table->integer('no_visitor');
            $table->json('visitor_names');
            $table->text('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('visitorentryexitlogs');
    }
};
