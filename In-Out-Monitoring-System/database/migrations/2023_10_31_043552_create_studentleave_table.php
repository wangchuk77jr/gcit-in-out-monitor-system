<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('studentleave', function (Blueprint $table) {
            $table->id('leaveID');
            $table->string('StartDate');
            $table->string('EndDate');
            $table->text('Reason');
            $table->unsignedBigInteger('studentID');
            $table->foreign('studentID')->references('studentID')->on('student')->onDelete('cascade');
            $table->string('LeaveApproval')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('studentleave');
    }
};
