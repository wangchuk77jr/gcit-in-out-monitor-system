<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('visitor_log', function (Blueprint $table) {
            $table->id('visitorID');
            $table->string('visitorCID');
            $table->string('Visitor_type');
            $table->string('staffEmail');
            $table->string('dependentName');
            $table->string('employeId');
            $table->string('v_phoneNumber');
            $table->integer('no_visitor');
            $table->json('visitor_names');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('reason');
            $table->string('Visitor_Approval')->nullable();
            $table->timestamps(); // Created_at and Updated_at columns
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('visitor_log');
    }
};
