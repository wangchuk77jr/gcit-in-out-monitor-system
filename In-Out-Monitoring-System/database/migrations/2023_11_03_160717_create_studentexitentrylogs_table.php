<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('studentexitentrylogs', function (Blueprint $table) {
            $table->id('exitEntrylogID');
            $table->string('checkInDateTime')->nullable();
            $table->string('checkOutDateTime')->nullable();
            $table->unsignedBigInteger('studentID');
            $table->foreign('studentID')->references('studentID')->on('student')->onDelete('cascade');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('studentexitentrylogs');
    }
};
