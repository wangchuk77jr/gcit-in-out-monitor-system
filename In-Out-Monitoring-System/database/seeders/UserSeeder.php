<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'name' =>'Admin',
                'email' =>'admin@gmail.com',
                'role' =>'admin',
                'status' => 'active',
                'password'=> bcrypt('password')
            ],
            [
                'name' =>'security-guard',
                'email' =>'securityguard@gmail.com',
                'role' =>'security-guard',
                'status' => 'active',
                'password'=> bcrypt('password')
            ],
            [
                'name' =>'sso',
                'email' =>'sso@gmail.com',
                'role' =>'sso',
                'status' => 'active',
                'password'=> bcrypt('password')
            ],
            [
                'name' =>'councilor',
                'email' =>'councilor@gmail.com',
                'role' =>'councilor',
                'status' => 'active',
                'password'=> bcrypt('password')
            ],
            [
                'name' =>'HR',
                'email' =>'hr@gmail.com',
                'role' =>'HR',
                'status' => 'active',
                'password'=> bcrypt('password')
            ],
                    [
                'name' =>'HR_Head',
                'email' =>'hrhead@gmail.com',
                'role' =>'HR_Head',
                'status' => 'active',
                'password'=> bcrypt('password')
            ]

        ]);
    }
}
