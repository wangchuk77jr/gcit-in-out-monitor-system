# GCIT In and Out Monitoring System

The **GCIT In and Out Monitoring System** is a cutting-edge web application built using the Laravel framework. This system has been designed to efficiently log the entry and exit of both students and visitors, enhancing campus security and safety measures at your institution. This document will guide you through the setup and usage of this application.

## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Features

- Secure login system for administrators,Security-Guard,SSO,Counsilor,HR & HRHead.
- Real-time entry and exit logging for students and visitors.
- Detailed reporting and data visualization.
- User-friendly dashboard.
- Role-based access control for enhanced security.
- Alerts and notifications for unusual activity.
- Easy-to-use web interface.

## Requirements

Before you begin the installation process, ensure that your server meets the following requirements:

- PHP 7.4 or higher
- Composer
- MySQL 5.7 or higher
- Web server (e.g., Apache, Nginx)

## Installation

1. Clone the repository to your server:

   ```bash
   https://gitlab.com/wangchuk77jr/gcit-in-out-monitor-system.git
   ```

2. Navigate to the project directory:

   ```bash
   cd gcit-in-out-monitor-system
   cd In-Out-Monitoring-System
   ```

3. Install PHP dependencies using Composer:

   ```bash
   composer install
   ```

4. Create a copy of the `.env.example` file as `.env`:

   ```bash
   cp .env.example .env
   ```

5. Generate an application key:

   ```bash
   php artisan key:generate
   ```

6. Configure the `.env` file with your database and other settings:

   - Set the `DB_HOST`, `DB_PORT`, `DB_DATABASE`, `DB_USERNAME`, and `DB_PASSWORD` to match your database configuration.
   - You may also configure other settings, such as mail and app URL, in the `.env` file.

7. Run database migrations and seed the database:

   ```bash
   php artisan migrate --seed
   ```

8. Set proper permissions for storage and cache:

   ```bash
   chmod -R 777 storage
   chmod -R 777 bootstrap/cache
   ```

9. Start the Laravel development server:

   ```bash
   php artisan serve
   ```

10. Visit `http://localhost:8000` in your web browser to access the application.

## Configuration

To further configure and customize the system, you can do the following:

- Adjust roles and permissions in the `config/roles.php` file.
- Customize the views and frontend assets in the `resources/views` and `public` directories.
- Modify alert and notification settings in the `config/alerts.php` file.
- Update email settings in the `.env` file for notifications.

## Usage

1. **Administrator Login**: Use the provided administrator credentials to log in and manage the system.

2. **Add Users**: Add students, visitors, and staff members to the system.

3. **Monitor Entry and Exit**: The system will automatically log the entry and exit of users.

4. **Generate Reports**: View detailed reports and data visualizations on the dashboard.

5. **Set Up Alerts**: Configure alerts and notifications to be informed of unusual activity.

## Contributing

We welcome contributions to improve this system. Please follow these steps to contribute:

1. Fork the repository.

2. Create a new branch for your feature or bug fix.

3. Make your changes and ensure code quality.

4. Submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE). Feel free to use, modify, and distribute it according to the terms of the license.

For any questions, issues, or support, please contact us at support@gcit-monitoring.com.

Thank you for using the **GCIT In and Out Monitoring System**! We hope it enhances the security and safety measures on your campus.
